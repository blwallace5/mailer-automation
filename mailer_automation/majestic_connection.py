import time
import datetime
from majesticseo_external_rpc.APIService import *
from helpers import create_alternate_urls_from_item


def set_string_to_unicode(s):
    """
    Sets an object to unicode if it is a non-unicode string, otherwise ignores it.

    Args:
        s : a text object to be potentially converted to a unicode object
    """

    encodings_to_try = ['utf-8', 'ascii', 'latin_1', 'iso8859_2']
    for encoding in encodings_to_try:
        try:
            if isinstance(s, basestring) and not isinstance(s, unicode):
                s = unicode(s, encoding)
        except UnicodeDecodeError:
            continue
    return s


def set_unicode_to_string(u, encoding='utf-8'):
    """
    Converts a unicode text object to a string object for writing/printing

    Args:
        u : a text unicode object to be converted to a string
        encoding (string) : optionally specifies the encoding to use for unicode object to string, defaults to utf-8
    """

    if isinstance(u, basestring) and not isinstance(u, str):
        u = u.encode(encoding)
    return u


def print_message(message):
    """
    Custom print statement adds timestamp to beginning of printed line.

    Args:
        message (basestring): the message to print out to user

    Returns:
        None
    """

    raw_time = time.time()
    timestamp = datetime.datetime.fromtimestamp(raw_time).strftime('%H:%M:%S')
    message = set_unicode_to_string(message)
    print('{} : {}'.format(timestamp, message))
    return


class MajesticConnection(object):
    """
    A class that connects to the Majestic API

    Attributes:
        endpoint (basestring) : the URL of the specific API endpoint in Majestic to connect to
        api_key (basestring) : the API access key to connect with
        items_to_query (list) : the items to query in Majestic API
        item_to_query (basestring) : a single item to query in Majestic API
        item_count (int) : the number of items to query in current check
        query_parameters (dict) : a dict containing the parameters to pass to Majestic in query
        api_connection (majesticseo_external_rpc.APIService) : a connection to Majestic

    """

    def __init__(self, endpoint, api_key):
        """
        Initializes the MajesticConnection object

        Args:
            endpoint (basestring) : the URL of the specific API endpoint in Majestic to connect to
            api_key (basestring) : the API access key to connect with

        """

        self.endpoint = endpoint
        self.api_key = api_key
        self.items_to_query = None
        self.item_to_query = None
        self.item_count = 0
        self.query_parameters = None
        self.api_connection = None

    def item_to_query_from_raw_input(self):
        """
        Gets an item to query from raw input and sets it to item_to_query

        Returns:
            None
        """

        itq = raw_input("Please enter the URL to query in Majestic. To skip this check, enter nothing.\n")
        self.item_count = 1
        self.item_to_query = set_string_to_unicode(itq)
        return

    def items_to_query_from_list(self, items_list):
        """
        Adds all items in a list to items_to_query

        Returns:
            None
        """

        self.items_to_query = [set_string_to_unicode(item) for item in items_list]
        self.item_count = len(self.items_to_query)
        return

    def build_query_parameters_single_item(self):
        """
        Builds the query_parameters dictionary for a single item query

        Returns:
            None
        """

        if not self.query_parameters:
            self.query_parameters = {}
        self.query_parameters['items'] = unicode(self.item_count)
        self.query_parameters['item0'] = self.item_to_query
        return

    def query_api(self, command, datasource):
        """
        Sends query to Majestic API and if there is a valid result, returns it as a response table XML object

        Returns:
            An XML response table
        """

        self.query_parameters['datasource'] = datasource
        for key, value in self.query_parameters.iteritems():
            self.query_parameters[key] = set_unicode_to_string(value)
        response = self.api_connection.execute_command(set_unicode_to_string(command), self.query_parameters)
        if response.is_ok:
            results = response.get_table_for_name('Results')
            return results
        else:
            print_message("Majestic returned error: {}".format(str(response.get_error_message)))
        return None

    def build_api_connection(self):
        """
        Builds the API connector

        Returns:
            None
        """

        self.api_connection = APIService(set_unicode_to_string(self.api_key), set_unicode_to_string(self.endpoint))
        return

    def query_api_connection(self, command, type):
        """
        Queries the Majestic API for results from either fresh, historic, or both for a specific command

        Args:
            command (basestring) : the specific command to be given to the Majestic query
            type (unicode) : a unicode string of u'f', u'h', or u'fh' (u'hf') that determines which index to query

        Returns:
            The combined results of all queries in a list
        """

        results = []
        if u'f' in type:
            fresh_results = self.query_api(command, 'fresh')
            if fresh_results:
                for row in fresh_results.rows:
                    results.append(row)
        if u'h' in type:
            historic_results = self.query_api(command, 'historic')
            if historic_results:
                for row in historic_results.rows:
                    results.append(row)
        return results

    def get_referring_domains_single_item(self, item=None, q_type=u'fh'):
        """
        Calls the GetRefDomains query from Majestic API

        Args:
            item (unicode) : a unicode string containing the URL to check in Majestic
            q_type (unicode) : a unicode string of u'f', u'h', or u'fh' (u'hf') that determines which index to query

        Returns:
            A list of unique referring domains that link to each variant url of item
        """

        referring_domains = set()
        self.item_to_query = item
        self.item_count = 1
        if not self.item_to_query or self.item_to_query[:4] != u'http' or len(self.item_to_query) <= 8:
            self.item_to_query_from_raw_input()
        if self.item_to_query == u'':
            return list(referring_domains)
        alternate_url_list = create_alternate_urls_from_item(self.item_to_query)
        for alternate in alternate_url_list:
            self.item_to_query = alternate
            self.build_query_parameters_single_item()
            self.query_parameters['AnalysisDepth'] = 100000
            results = self.query_api_connection('GetRefDomains', q_type)
            if results:
                for row in results:
                    domain = row['Domain']
                    referring_domains.add(set_string_to_unicode(domain))
        checked_domains = sorted(alternate_url_list)
        print_message("Checked the following URL variants in Majestic: {}".format(', '.join(checked_domains)))
        print_message("Length of referring domains retrieved: {}".format(len(referring_domains)))
        return list(referring_domains)



