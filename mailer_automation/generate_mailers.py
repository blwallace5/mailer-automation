import cPickle as pickle
import os
from helpers import print_message, ZohoFailureError
from mailer_creation_classes import ContactProcessor, DataCleaner, BlacklistScreener, DomainChecker, EmailValidator, \
                                    OutputWriter, ResponseScreener, VerticalBlacklistScreener, \
                                    ReferringDomainsScreener, FollowUpPublisher, RecentSendsScreener, \
                                    DomainSendsLimiter, SendReadyPublisher, CompContactProcessor, \
                                    CompDomainSendsLimiter, CompPublisher, ManualSendScreener, ImpressionwiseScreener,\
                                    ContactDeduper

def customize_outfile_names():
    """
    Updates the outfile names based on user input.

    Returns:
        a string representing the file name to save to
    """

    custom_key = raw_input("Please enter the custom name for the outfiles for this mailer.\n")
    if custom_key[:-4] != '.csv':
        custom_key += '.csv'
    return custom_key


def delete_files(file_list):
    """
    Deletes files in a file list

    Args:
        file_list:

    Returns:
        None
    """

    for filename in file_list:
        os.remove(filename)
    return


def configure_mailer(mailer_type, output_file_key):
    """
    Performs a series of steps for a mailer of a specified type. These steps can all require manual input

    Args:
        mailer_type (str) : the name of the mailer type to use for internal reference
        output_file_key (str) : the name of the mailer type to be written into filenames

    Returns:
        Multiple filenames of files containing pickled contacts data
    """

    if mailer_type == 'Comp':
        C = CompContactProcessor()
        contacts = C.get_contacts_and_compare()
    else:
        C = ContactProcessor()
        contacts = C.get_contacts_from_sources()
    print_message("Pulled Contacts: {}".format(len(contacts)))
    D = DataCleaner(contacts)
    contacts, invalid_contacts = D.clean_contacts()
    invalids_filename = os.path.join(os.path.dirname(__file__),
                                     'run_configuration/temp_files/invalids_{}.p'.format(output_file_key))
    pickle.dump(invalid_contacts, open(invalids_filename, "wb"))
    print_message("Cleaned Contacts: {}".format(len(contacts)))
    if mailer_type in ['FollowUp', 'Comp']:
        try:
            R = ResponseScreener(contacts)
            contacts, response_cut_contacts = R.screen_responders()
            responders_filename = os.path.join(os.path.dirname(__file__),
                                               'run_configuration/temp_files/responders_{}.p'.format(output_file_key))
            pickle.dump(response_cut_contacts, open(responders_filename, "wb"))
            print_message("Screened Contacts: {}".format(len(contacts)))
        except ZohoFailureError:
            raise ZohoFailureError
    else:
        responders_filename = ''
    if mailer_type != 'Prelim':
        L = ReferringDomainsScreener(contacts)
        contacts, already_linking = L.screen_already_linking_domains()
        linkers_filename = os.path.join(os.path.dirname(__file__),
                                        'run_configuration/temp_files/linkers_{}.p'.format(output_file_key))
        pickle.dump(already_linking, open(linkers_filename, "wb"))
        print_message("Linking Domain Screened Contacts: {}".format(len(contacts)))
    else:
        linkers_filename = ''
    contact_filename = os.path.join(os.path.dirname(__file__),
                                    'run_configuration/temp_files/contacts_{}.p'.format(output_file_key))
    pickle.dump(contacts, open(contact_filename, "wb"))
    print_message("*****Pausing execution to add additional mailers.")
    if mailer_type == 'Prelim':
        return contact_filename, invalids_filename
    elif mailer_type in ['SendReady', 'PrelimSendReady']:
        return contact_filename, invalids_filename, linkers_filename
    elif mailer_type in ['FollowUp', 'Comp']:
        return contact_filename, invalids_filename, responders_filename, linkers_filename


def generate_prelim_mailer(contacts_filename, invalids_filename, output_file_key):
    """
    Calls the remaining, fully automated features of prelim mailer generation

    Args:
        contacts_filename (str) : the pickle file containing contacts data
        invalids_filename (str): the pickle file containing invalid contacts cut data
        output_file_key (str) : the name of the mailer type to be written into filenames

    Returns:
        An integer counting the number of valid contacts written to output
    """

    contacts = pickle.load(open(contacts_filename, "rb"))
    invalid_contacts = pickle.load(open(invalids_filename, "rb"))
    B = BlacklistScreener(contacts)
    contacts, blacklisted_contacts = B.screen_contacts()
    print_message("Screened Contacts: {}".format(len(contacts)))
    M = DomainChecker(contacts)
    contacts, geo_cut_contacts = M.check_contacts_list_ownership()
    print_message("Checked Contacts: {}".format(len(contacts)))
    E = EmailValidator(contacts)
    contacts, email_cut_contacts = E.validate_all_emails()
    print_message("Kickbox Validated Contacts: {}".format(len(contacts)))
    I = ImpressionwiseScreener(contacts)
    contacts, iw_cut_contacts = I.process_all_contacts()
    print_message("Impressionwise Validated Contacts: {}".format(len(contacts)))
    #H = ManualSendScreener(contacts)
    #contacts, high_value_contacts = H.screen_contacts()
    high_value_contacts = []
    #print_message("High Value Contacts: {}".format(len(high_value_contacts)))
    print_message("Standard Contacts: {}".format(len(contacts)))
    W = OutputWriter(contacts, blacklisted_contacts, geo_cut_contacts, email_cut_contacts, invalid_contacts,
                     high_value_contacts, iw_cut_contacts)
    W.export_contacts(outfile_name=output_file_key)
    print('\a')
    print_message("Prelim mailer {} complete.".format(output_file_key))
    return len(contacts)


def generate_send_ready_mailer(contacts_filename, invalids_filename, linkers_filename, output_file_key):
    """
    Calls the remaining, fully automated features of send-ready mailer generation

    Args:
        contacts_filename (str) : the pickle file containing contacts data
        invalids_filename (str): the pickle file containing invalid contacts cut data
        linkers_filename (str) : the pickle file containing already-linking contacts cut data
        output_file_key (str) : the name of the mailer type to be written into filename

    Returns:
        An integer counting the number of valid contacts written to output
    """

    contacts = pickle.load(open(contacts_filename, "rb"))
    invalid_contacts = pickle.load(open(invalids_filename, "rb"))
    already_linking = pickle.load(open(linkers_filename, "rb"))
    contacts = ContactDeduper(contacts).dedupe()
    I = ImpressionwiseScreener(contacts)
    contacts, iw_cut_contacts = I.process_all_contacts()
    print_message("Impressionwise Validated Contacts: {}".format(len(contacts)))
    V = VerticalBlacklistScreener(contacts)
    contacts, blacklisted_contacts = V.screen_contacts_verticals()
    print_message("Blacklist Screened Contacts: {}".format(len(contacts)))
    R = RecentSendsScreener(contacts)
    contacts, recently_sent = R.screen_recently_sent_contacts()
    print_message("Recent Send Screened Contacts: {}".format(len(contacts)))
    L = DomainSendsLimiter(contacts)
    contacts, extra_contacts = L.limit_contacts()
    print_message("Domain Limited Contacts: {}".format(len(contacts)))
    H = ManualSendScreener(contacts)
    contacts, high_value_contacts = H.screen_contacts()
    print_message("High Value Contacts: {}".format(len(high_value_contacts)))
    print_message("Standard Contacts: {}".format(len(contacts)))
    W = SendReadyPublisher(contacts, blacklisted_contacts, already_linking, recently_sent, invalid_contacts,
                           extra_contacts, high_value_contacts, iw_cut_contacts)
    W.export_contacts(outfile_name=output_file_key)
    print('\a')
    print_message("Send ready mailer {} complete.".format(output_file_key))
    return len(contacts)




def generate_follow_up_mailer(contacts_filename, invalids_filename, responders_filename, linkers_filename, output_file_key):
    """
    Calls the remaining, fully automated features of follow-up mailer generation

    Args:
        contacts_filename (str) : the pickle file containing contacts data
        invalids_filename (str): the pickle file containing invalid contacts cut data
        responders_filename (str) : the pickle file containing responder contacts cut data
        linkers_filename (str) : the pickle file containing already-linking contacts cut data
        output_file_key (str) : the name of the mailer type to be written into filenames

    Returns:
        An integer counting the number of valid contacts written to output
    """

    contacts = pickle.load(open(contacts_filename, "rb"))
    invalid_contacts = pickle.load(open(invalids_filename, "rb"))
    response_cut_contacts = pickle.load(open(responders_filename, "rb"))
    already_linking = pickle.load(open(linkers_filename, "rb"))
    B = VerticalBlacklistScreener(contacts)
    contacts, blacklisted_contacts = B.screen_contacts_verticals()
    print_message("Screened Contacts: {}".format(len(contacts)))
    M = DomainChecker(contacts)
    contacts, geo_cut_contacts = M.check_contacts_list_ownership()
    print_message("Checked Contacts: {}".format(len(contacts)))
    E = EmailValidator(contacts)
    contacts, email_cut_contacts = E.validate_all_emails()
    print_message("Kickbox Validated Contacts: {}".format(len(contacts)))
    I = ImpressionwiseScreener(contacts)
    contacts, iw_cut_contacts = I.process_all_contacts()
    print_message("Impressionwise Validated Contacts: {}".format(len(contacts)))
    H = ManualSendScreener(contacts)
    contacts, high_value_contacts = H.screen_contacts()
    print_message("High Value Contacts: {}".format(len(high_value_contacts)))
    print_message("Standard Contacts: {}".format(len(contacts)))
    W = FollowUpPublisher(contacts, response_cut_contacts, blacklisted_contacts, already_linking, geo_cut_contacts,
                          email_cut_contacts, invalid_contacts, high_value_contacts, iw_cut_contacts)
    W.export_contacts(outfile_name=output_file_key)
    print_message("Follow up mailer {} complete.".format(output_file_key))
    print('\a')
    return len(contacts)


def generate_comp_mailer(contacts_filename, invalids_filename, responders_filename, linkers_filename, output_file_key):
    """
    Calls the remaining, fully automated features of comp mailer generation

    Args:
        contacts_filename (str) : the pickle file containing contacts data
        invalids_filename (str): the pickle file containing invalid contacts cut data
        responders_filename (str) : the pickle file containing responder contacts cut data
        linkers_filename (str) : the pickle file containing already-linking contacts cut data
        output_file_key (str) : the name of the mailer type to be written into filenames

    Returns:
        An integer counting the number of valid contacts written to output
    """

    contacts = pickle.load(open(contacts_filename, "rb"))
    invalid_contacts = pickle.load(open(invalids_filename, "rb"))
    response_cut_contacts = pickle.load(open(responders_filename, "rb"))
    already_linking = pickle.load(open(linkers_filename, "rb"))
    B = BlacklistScreener(contacts)
    contacts, blacklisted_contacts = B.screen_contacts()
    print_message("Screened Contacts: {}".format(len(contacts)))
    M = DomainChecker(contacts)
    contacts, geo_cut_contacts = M.check_contacts_list_ownership()
    print_message("Checked Contacts: {}".format(len(contacts)))
    E = EmailValidator(contacts)
    contacts, email_cut_contacts = E.validate_all_emails()
    print_message("Kickbox Validated Contacts: {}".format(len(contacts)))
    I = ImpressionwiseScreener(contacts)
    contacts, iw_cut_contacts = I.process_all_contacts()
    print_message("Impressionwise Validated Contacts: {}".format(len(contacts)))
    V = VerticalBlacklistScreener(contacts)
    contacts, vertical_blacklisted_contacts = V.screen_contacts_verticals()
    print_message("Blacklist Screened Contacts: {}".format(len(contacts)))
    R = RecentSendsScreener(contacts)
    contacts, recently_sent = R.screen_recently_sent_contacts()
    print_message("Recent Send Screened Contacts: {}".format(len(contacts)))
    L = CompDomainSendsLimiter(contacts)
    contacts, extra_contacts = L.limit_contacts()
    print_message("Domain Limited Contacts: {}".format(len(contacts)))
    H = ManualSendScreener(contacts)
    contacts, high_value_contacts = H.screen_contacts()
    print_message("High Value Contacts: {}".format(len(high_value_contacts)))
    print_message("Standard Contacts: {}".format(len(contacts)))
    W = CompPublisher(contacts, blacklisted_contacts, geo_cut_contacts, email_cut_contacts,
                      vertical_blacklisted_contacts, response_cut_contacts, already_linking, recently_sent,
                      extra_contacts, invalid_contacts, high_value_contacts, iw_cut_contacts)
    W.export_contacts(outfile_name=output_file_key)
    print_message("Comp mailer {} complete.".format(output_file_key))
    print('\a')
    return len(contacts)


def generate_mailers():
    """
    Operates the main UI for mailer generation and controls the generation processes

    Returns:
        None
    """

    mailer_states = {'Prelim': [],
                     'SendReady': [],
                     'FollowUp': [],
                     'Comp': [],
                     'PrelimSendReady': []}
    while True:
        outfile_name = None
        mailer_category = ''
        try:
            print_message("Please select the mailer type you wish to create:")
            selection = raw_input("1: Prelim Mailer\n"
                                  "2: Send-Ready Mailer\n"
                                  "3: Combo Prelim + Send-Ready Mailer\n"
                                  "4: Follow-up Mailer\n"
                                  "5: Comp Mailer\n"
                                  "6: NONE (End Program)\n")
            int_selection = int(selection)
            if int_selection == 1:
                mailer_category = 'Prelim'
            elif int_selection == 2:
                mailer_category = 'SendReady'
            elif int_selection == 3:
                mailer_category = 'PrelimSendReady'
            elif int_selection == 4:
                mailer_category = 'FollowUp'
            elif int_selection == 5:
                mailer_category = 'Comp'
            elif int_selection == 6:
                break
            else:
                print_message("Invalid selection. Please enter a valid number.")
                continue
            outfile_name = customize_outfile_names()
            mailer_state = list(configure_mailer(mailer_category, outfile_name))
            mailer_state.append(outfile_name)
            mailer_states[mailer_category].append(tuple(mailer_state))
        except ValueError:
            print_message("Invalid selection. Please enter a valid number.")
        except ZohoFailureError:
            print_message("Unable to retrieve Zoho data. Please retry mailer.")
        except KeyboardInterrupt:
            if outfile_name and mailer_category:
                print_message("CANCELED MAILER: {} (type: {})".format(outfile_name, mailer_category))
            else:
                print_message("CANCELED MAILER")
            print_message("Begin new mailer or use CTRL+Z to exit script (you will lose all progress).")
            continue
        except Exception as e:
            try:
                print_message("Something bad happened. {} did not complete and will need to be re-attempted:\n{}".format(outfile_name, e))
            except NameError:
                print_message("Something bad happened. A mailer (unknown, please check output) did not complete and will need to be re-attempted:\n{}".format(e))
    try:
        for mailer_state in mailer_states['Prelim']:
            contacts_pickle, invalids_pickle, outfile_key = mailer_state
            generate_prelim_mailer(contacts_pickle, invalids_pickle, outfile_key)
            delete_files([contacts_pickle, invalids_pickle])
        for mailer_state in mailer_states['SendReady']:
            contacts_pickle, invalids_pickle, linkers_pickle, outfile_key = mailer_state
            generate_send_ready_mailer(contacts_pickle, invalids_pickle, linkers_pickle, outfile_key)
            delete_files([contacts_pickle, invalids_pickle, linkers_pickle])
        for mailer_state in mailer_states['FollowUp']:
            contacts_pickle, invalids_pickle, responders_pickle, linkers_pickle, outfile_key = mailer_state
            generate_follow_up_mailer(contacts_pickle, invalids_pickle, responders_pickle, linkers_pickle, outfile_key)
            delete_files([contacts_pickle, invalids_pickle, responders_pickle, linkers_pickle])
        for mailer_state in mailer_states['Comp']:
            contacts_pickle, invalids_pickle, responders_pickle, linkers_pickle, outfile_key = mailer_state
            generate_comp_mailer(contacts_pickle, invalids_pickle, responders_pickle, linkers_pickle, outfile_key)
            delete_files([contacts_pickle, invalids_pickle, responders_pickle, linkers_pickle])
        for mailer_state in mailer_states['PrelimSendReady']:
            contacts_pickle, invalids_pickle, linkers_pickle, outfile_key = mailer_state
            generate_prelim_mailer(contacts_pickle, invalids_pickle, outfile_key)
            generate_send_ready_mailer(contacts_pickle, invalids_pickle, linkers_pickle, outfile_key)
            delete_files([contacts_pickle, invalids_pickle, linkers_pickle])
    except KeyboardInterrupt:
        raise
    except Exception as e:
        try:
            print_message(
                "Something bad happened. {} did not complete and will need to be re-attempted:\n{}".format(outfile_key, e))
        except NameError:
            print_message("Something bad happened. A mailer (unknown, please check output) did not complete and will need to be re-attempted:\n{}".format(e))
    print('\a')
    print_message("All mailers created. Program complete.")

if __name__ == '__main__':
    generate_mailers()
