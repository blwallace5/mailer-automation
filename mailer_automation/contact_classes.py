import time
from helpers import set_string_to_unicode


class Contact(object):
    """
    A class to represent a contact and all of its related information.

    Attributes:
        email_entry (EmailEntry) : Optional. an object containing the contact email and related information
        source_entry (DomainEntry) : Optional. an object containing the source domain and related information
        target_entry (DomainEntry) : Optional. an object containing the target domain and related information
        asset_entry (AssetEntry) : Optional. an object containing the asset and related information
        contact_name (unicode) : the name of the contact
        contact_action (unicode) : the ultimate action to be taken for this contact
        date_collected (unicode) : date the VA collected this contact
        contact_type (unicode) : the type of contact collected -- almost always CLB
        previously_contacted (boolean) : used only in Comp Mailer process -- set to True if contact was included in
                                            initial campaign send
    """

    def __init__(self, contact_name, date_collected, contact_type, email_entry=None, source_entry=None,
                 target_entry=None, asset_entry=None, send_vars_entry=None):
        """
        Initializes the Contact object

        Args:
            email_entry (EmailEntry) : an object containing the contact email and related information
            source_entry (DomainEntry) : an object containing the source domain and related information
            target_entry (DomainEntry) : an object containing the target domain and related information
            asset_entry (AssetEntry) : an object containing the asset and related information
            contact_name (unicode) : the name of the contact
            date_collected (unicode) : date the VA collected this contact
            contact_type (unicode) : the type of contact collected -- almost always CLB
        """

        self.email_entry = email_entry
        self.source_entry = source_entry
        self.target_entry = target_entry
        self.asset_entry = asset_entry
        self.send_vars_entry = send_vars_entry
        self.contact_name = contact_name
        self.contact_action = None
        self.date_collected = date_collected  # format as datetime? can't rely on format for strptime()
        self.contact_type = contact_type
        self.previously_contacted = False

    def __eq__(self, other):
        """
        Sets equality comparator to look at email_entry.email

        Returns:
            Boolean
        """
        
        try:
            return self.email_entry.email == other.email_entry.email
        except AttributeError:
            return False

    def __hash__(self):
        """
        Creates a hash of email_entry.email

        Returns:
            Hash
        """

        return hash(self.email_entry.email)

    def create_string_of_object_attrs(self):
        """
        Generates a string containing the concatenation of the object's deep attributes

        Returns:
            A string
        """

        attribute_list = [self.contact_name, self.email_entry.email, self.source_entry.domain,
                          self.source_entry.url, self.source_entry.trust_flow,
                          self.source_entry.topic, self.source_entry.topic_trust_flow,
                          self.target_entry.url, self.target_entry.domain, self.asset_entry.asset,
                          self.asset_entry.campaign, self.asset_entry.email_variant, self.asset_entry.email_notes]

        attr_string = u'|'.join(
            [set_string_to_unicode(attribute) if attribute else u' ' for attribute in attribute_list])
        return attr_string


class EmailEntry(object):
    """
    A class to represent the validation results for an email address.

    Attributes:
        email (unicode) : the email address (e.g., name@example.com)
        result (unicode) : the validation result for the email (deliverable, undeliverable, risky, or unknown)
        date (unicode) : the current date (mm/dd/yyyy)
        reason (unicode) : the reason returned by Kickbox.io email validation
        sendex (unicode) : the sendex returned by Kickbox.io email validation
        source (unicode) : location of where this email's Kickbox data came from - cache or fresh lookup
        kickbox_action (unicode) : the action to be applied to this email based on Kickbox (ex: Blacklist, Keep, etc.)
        action (unicode) : the action to be applied to this email based on domain checks
        cache (unicode) : the caching instruction for the domain entry
        domain (unicode) : the email address' domain
        subdomain (unicode) : the email address' subdomain
        whitelisted_domain (bool) : is the domain whitelisted
        blacklisted_domain (bool) : is the domain blacklisted
        whitelisted_subdomain (bool) : is the subdomain whitelisted
        blacklisted_subdomain (bool) : is the subdomain blacklisted
        tld_entry (TLDEntry) : a TLDEntry object containing TLD extract data for the email domain
        whois_entry (WHOISEntry) : a WHOISEntry object containing WHOIS data for the email domain
        ip_geo_entry (IPGeoEntry) : a IPGeoEntry object containing geographic data for the email subdomain
            (based on IP address)
    """

    def __init__(self, email=None):
        """
        Initializes an EmailEntry object.

        Args:
            email (unicode) : Optional. the email address (e.g., name@example.com)
        """

        self.email = email
        self.date = set_string_to_unicode(time.strftime('%m/%d/%Y'))
        self.result = None
        self.reason = None
        self.sendex = None
        self.source = None
        self.kickbox_action = None
        self.action = None
        self.cache = None
        self.domain = None
        self.subdomain = None
        self.whitelisted_domain = False
        self.blacklisted_domain = False
        self.whitelisted_subdomain = False
        self.blacklisted_subdomain = False
        self.ip_geo_entry = None
        self.whois_entry = None
        self.tld_entry = None
        self.iw_result = None
        self.iw_npd = None
        self.iw_cc = None
        self.iw_ttp = None
        self.iw_action = None
        self.iw_source = None
        self.iw_cache_is_active = True
        self.iw_cache_expiration = None


class DomainEntry(object):
    """
    A class to represent a domain and its related information.

    Attributes:
        domain (unicode) : the domain (e.g., google.com)
        subdomain (unicode) : the subdomain (e.g., www.google.com)
        url (unicode) : the url of the domain (e.g., http://www.google.com)
        trust_flow (unicode) : the trust flow (from Majestic API) of the domain
        topic (unicode) : the topic (from Majestic API) of the domain
        topic_trust_flow (unicode) : the topical trust flow (from Majestic API) of the domain
        ip_address (unicode) : the ip address of domain
        whois_entry (WHOISEntry) : a WHOISEntry object containing WHOIS data for the domain
        ip_geo_entry (IPGeoEntry) : a IPGeoEntry object containing geographic data for the domain
            (based on IP address)
        tld_entry (TLDEntry) : a TLDEntry object containing TLD extract data for the domain
        whitelisted_domain (bool) : is the domain whitelisted
        blacklisted_domain (bool) : is the domain blacklisted
        whitelisted_subdomain (bool) : is the subdomain whitelisted
        blacklisted_subdomain (bool) : is the subdomain blacklisted
        action (unicode) : the action to be applied to this domain (ex: Blacklist, Keep, etc.)
        cache (unicode) : the caching instruction for the domain entry
    """

    def __init__(self, domain=None, url=None, trust_flow=None, topic=None, topic_trust_flow=None):
        """
        Initializes a DomainEntry object.

        Args:
            domain (unicode) : Optional. the domain (e.g., google.com)
            url (unicode) : Optional. the url of the domain (e.g., http://www.google.com)
            trust_flow (unicode) : Optional. the trust flow (from Majestic API) of the domain
            topic (unicode) : Optional. the topic (from Majestic API) of the domain
            topic_trust_flow (unicode) : Optional. the topical trust flow (from Majestic API) of the domain
        """

        self.domain = domain
        self.subdomain = None
        self.url = url
        self.trust_flow = trust_flow
        self.topic = topic
        self.topic_trust_flow = topic_trust_flow
        self.ip_address = None
        self.whois_entry = None
        self.ip_geo_entry = None
        self.tld_entry = None
        self.whitelisted_domain = False
        self.blacklisted_domain = False
        self.whitelisted_subdomain = False
        self.blacklisted_subdomain = False
        self.action = None
        self.cache = None
        self.entity_name = None
        self.entity_type = None
        self.city = None
        self.state = None
        self.entity_website = None
        self.entity_subgroup = None
        self.entity_subgroup_url = None
        self.entity_subgroup_resources_url = None


class AssetEntry(object):
    """
    A class to represent an Asset and its related information.

    Attributes:
        asset (unicode) : the asset name
        campaign (unicode) : the campaign name
        context (unicode) : the context report number for this asset
        site (unicode) : Optional. the site abbreviation for this asset
        asset_url (unicode) : Optional. the url for this asset (or report number if no url available)
        content_group (unicode) : Optional. the content group for this asset
        series (unicode) : Optional. the series for this asset
        audiences (unicode) : audience tags set during CLB research
        email_variant (unicode) : Optional. the campaign email variants
        email_notes (unicode) : the email notes for a particular contact
    """

    def __init__(self, asset=None, campaign=None, email_variant=None, email_notes=None, context=None, site=None,
                 asset_url=None, content_group=None, series=None, audiences=None):
        """
        Initializes an AssetEntry object.

        Args:
            asset (unicode) : Optional. the asset name
            campaign (unicode) : Optional. the campaign name
            context (unicode) : Optional. the context report number for this asset
            site (unicode) : Optional. the site abbreviation for this asset
            asset_url (unicode) : Optional. the url for this asset (or report number if no url available)
            content_group (unicode) : Optional. the content group for this asset
            series (unicode) : Optional. the series for this asset
            audiences (unicode) : audience tags set during CLB research
            email_variant (unicode) : Optional. the campaign email variants
            email_notes (unicode) : the email notes for a particular contact
        """

        self.asset = asset
        self.campaign = campaign
        self.context = context
        self.email_variant = email_variant
        self.email_notes = email_notes
        self.site = site
        self.asset_url = asset_url
        self.content_group = content_group
        self.series = series
        self.audiences = audiences


class TLDEntry(object):
    """
    A class to represent the TLD information related to a domain.

    Attributes:
        domain (unicode) : the domain (e.g., google.com)
        tld (unicode) : the top-level domain of domain
        tld_type (unicode) : whether the tld is blacklisted or not blacklisted from usage in mailers

    """

    def __init__(self, domain):
        """
        Initializes a TLDEntry object.

        All attributes except for domain are initialized as None. Domain is required upon initialization.

        Args:
            domain (unicode) : the domain (e.g., google.com)

        """

        self.domain = domain
        self.tld = None
        self.tld_type = None


class WHOISEntry(object):
    """
    A class to represent WHOIS registration data for a domain

    Attributes:
        domain (unicode) : the domain (e.g., google.com)
        registrar_country (unicode) : the country which the registrant claims as their physical location
        registrar_locale (unicode) : the location status (Domestic/International/Unknown) of country
        admin_country (unicode) : the country which the domain admin claims as their physical location
        admin_locale (unicode) : the location status (Domestic/International/Unknown) of admin_country
        tech_country (unicode) : the country which domain technical contact claims as their physical locatio
        tech_locale (unicode) : the location status (Domestic/International/Unknown) of tech_country

    """

    def __init__(self, domain):
        """
        Initializes a WHOISEntry object.

        All attributes except for domain are initialized as None. Domain is required upon initialization.

        Args:
            domain (unicode) : the domain (e.g., google.com)

        """

        self.domain = domain
        self.registrar_country = None
        self.tech_country = None
        self.admin_country = None
        self.registrar_locale = None
        self.tech_locale = None
        self.admin_locale = None


class IPGeoEntry(object):
    """
    A class to represent Geolocation (based on IP) data for a domain

    Attributes:
        domain (unicode) : a domain (e.g., google.com)
        ip_address (unicode) : an ip address (e.g. '74.125.239.134')
        country_name (unicode) : the name of the country where ip_address is hosted
        locale (unicode) : the location status (Domestic/International/Unknown) of domain, based on geolocation info

    """

    def __init__(self, domain):
        """
        Initializes the IPGeoEntry object.

        Args:
            domain (unicode) : the domain for the IP address

        """
        self.domain = domain
        self.ip_address = None
        self.country_name = None
        self.locale = None


class SendVarsEntry(object):

    def __init__(self):
        self.subject = None
        self.var1 = None
        self.var2 = None
        self.var3 = None
        self.var4 = None
        self.var5 = None
        self.var6 = None
        self.var7 = None
        self.var8 = None
        self.bucket = None
