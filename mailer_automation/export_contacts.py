import cPickle as pickle
import os
from helpers import print_message, ZohoFailureError
from mailer_creation_classes import ContactProcessor, DataCleaner, BlacklistScreener, DomainChecker, EmailValidator, \
                                    OutputWriter

def customize_outfile_names():
    """
    Updates the outfile names based on user input.

    Returns:
        a string representing the file name to save to
    """

    custom_key = raw_input("Please enter the custom name for the outfiles for this export.\n")
    if custom_key[:-4] != '.csv':
        custom_key += '.csv'
    return custom_key


def delete_files(file_list):
    """
    Deletes files in a file list

    Args:
        file_list:

    Returns:
        None
    """

    for filename in file_list:
        #os.remove(filename)
        continue
    return


def configure_export(output_file_key):
    """
    Performs a series of steps for a mailer of a specified type. These steps can all require manual input

    Args:
        mailer_type (str) : the name of the mailer type to use for internal reference
        output_file_key (str) : the name of the mailer type to be written into filenames

    Returns:
        Multiple filenames of files containing pickled contacts data
    """

    C = ContactProcessor()
    contacts = C.get_contacts_from_database_source_only()
    print_message("Pulled Contacts: {}".format(len(contacts)))
    D = DataCleaner(contacts)
    contacts, invalid_contacts = D.clean_contacts()
    invalids_filename = os.path.join(os.path.dirname(__file__),
                                     'run_configuration/temp_files/invalids_{}.p'.format(output_file_key))
    pickle.dump(invalid_contacts, open(invalids_filename, "wb"))
    print_message("Cleaned Contacts: {}".format(len(contacts)))
    contact_filename = os.path.join(os.path.dirname(__file__),
                                    'run_configuration/temp_files/contacts_{}.p'.format(output_file_key))
    pickle.dump(contacts, open(contact_filename, "wb"))
    print_message("*****Pausing execution to add additional exports.")
    return contact_filename, invalids_filename


def generate_contact_inventory_export(contacts_filename, invalids_filename, output_file_key):
    """
    Calls the remaining, fully automated features of prelim mailer generation

    Args:
        contacts_filename (str) : the pickle file containing contacts data
        invalids_filename (str): the pickle file containing invalid contacts cut data
        output_file_key (str) : the name of the mailer type to be written into filenames

    Returns:
        An integer counting the number of valid contacts written to output
    """

    contacts = pickle.load(open(contacts_filename, "rb"))
    invalid_contacts = pickle.load(open(invalids_filename, "rb"))
    B = BlacklistScreener(contacts)
    contacts, blacklisted_contacts = B.screen_contacts()
    print_message("Screened Contacts: {}".format(len(contacts)))
    M = DomainChecker(contacts)
    contacts, geo_cut_contacts = M.check_contacts_list_ownership()
    print_message("Checked Contacts: {}".format(len(contacts)))
    E = EmailValidator(contacts)
    contacts, email_cut_contacts = E.validate_all_emails()
    print_message("Validated Contacts: {}".format(len(contacts)))
    high_value_contacts = []
    impressionwise_cuts = [] # this is so ugly
    print_message("Standard Contacts: {}".format(len(contacts)))
    W = OutputWriter(contacts, blacklisted_contacts, geo_cut_contacts, email_cut_contacts, invalid_contacts,
                     high_value_contacts, impressionwise_cuts)
    W.export_ci_only(outfile_name=output_file_key)
    print('\a')
    print_message("Contact Inventory Export for {} complete.".format(output_file_key))
    return len(contacts)


def export_contacts():
    """
    Operates the main UI for mailer generation and controls the generation processes

    Returns:
        None
    """

    mailer_states = {'CI': []}
    while True:
        print_message("Please select the operation to perform:")
        selection = raw_input("1: Export Contacts\n"
                              "2: NONE (End Program)\n")
        try:
            int_selection = int(selection)
            mailer_category = ''
            if int_selection == 1:
                mailer_category = 'CI'
            elif int_selection == 2:
                break
            else:
                print_message("Invalid selection. Please enter a valid number.")
                continue
            outfile_name = customize_outfile_names()
            mailer_state = list(configure_export(outfile_name))
            mailer_state.append(outfile_name)
            mailer_states[mailer_category].append(tuple(mailer_state))
        except ValueError:
            print_message("Invalid selection. Please enter a valid number.")
        except ZohoFailureError:
            print_message("Unable to retrieve Zoho data. Please retry mailer.")
        except KeyboardInterrupt:
            raise
        except Exception as e:
            try:
                print_message("Something bad happened. {} did not complete and will need to be re-attempted:\n{}".format(outfile_name, e))
            except NameError:
                print_message("Something bad happened. A mailer (unknown, please check output) did not complete and will need to be re-attempted:\n{}".format(e))
    try:
        for mailer_state in mailer_states['CI']:
            contacts_pickle, invalids_pickle, outfile_key = mailer_state
            generate_contact_inventory_export(contacts_pickle, invalids_pickle, outfile_key)
            delete_files([contacts_pickle, invalids_pickle])
    except KeyboardInterrupt:
        raise
    except Exception as e:
        try:
            print_message(
                "Something bad happened. {} did not complete and will need to be re-attempted:\n{}".format(outfile_key, e))
        except NameError:
            print_message("Something bad happened. A mailer (unknown, please check output) did not complete and will need to be re-attempted:\n{}".format(e))
    print('\a')
    print_message("All mailers created. Program complete.")

if __name__ == '__main__':
    export_contacts()
