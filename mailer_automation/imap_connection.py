import imaplib
import email, email.header
import sys
import datetime
from mysql_database_connection import DBConnection

from helpers import set_string_to_unicode, set_unicode_to_string, print_message, selection_from_list, email_acct_creds, \
    domain_imap_servers, ZohoFailureError

class IMAPConnection(object):

    def __init__(self, account, password, IMAP_SERVER, PORT):
        self.account = account
        self.password = password
        self.IMAP_SERVER = IMAP_SERVER
        self.PORT = int(PORT)
        self.cxn = None

    def connect(self):
        """Create IMAP connection to server and assign to self.imap_cxn. Log in to user-defined email account."""
        try:
            self.cxn = imaplib.IMAP4_SSL(self.IMAP_SERVER, self.PORT)
        except:
            print_message("Error connecting to IMAP server.")
        try:
            self.cxn.login(self.account, self.password)
        except imaplib.IMAP4.error:
            print_message("Error logging into {}".format(self.account))

    def disconnect(self):
        if self.cxn:
            self.cxn.close()
            self.cxn.logout()

    def get_folders(self, top_level_folder=None, parent_folders_only=False):
        if top_level_folder:
            response, data = self.cxn.list(top_level_folder)
            folder_list = [top_level_folder] + [folder.split('"')[-2] for folder in data]
        else:
            response, data = self.cxn.list()
            folder_list = [folder.split('"')[-2] for folder in data]
        if parent_folders_only:
            folder_list = sorted(set([folder.split('/')[0] for folder in folder_list]))
        return folder_list

    def get_emails_set_from_folder(self, folder):
        """Retrieve all messages from folder, extracts 'From' email address, and appends to self.emails_list."""
        response, message_count = self.cxn.select(folder)
        count = int(message_count[0])
        emails_list = []
        if count > 0:
            messages = self.cxn.fetch('1:{}'.format(count), '(BODY.PEEK[HEADER])')
            for message in messages[1]:
                if message != ')':
                    try:
                        msg = email.message_from_string(message[1])
                    except AttributeError as e:
                        print_message('AttributeError: {}'.format(str(e)))
                    if '<' in msg['From']:
                        tmp = msg['From'].split('<')
                        sender = tmp[1].split('>')
                        sender = sender[0].lower()
                    else:
                        sender = msg['From'].lower()
                    try:
                        sender = set_string_to_unicode(sender)
                        emails_list.append(sender)
                    except:
                        print_message("Could not convert {} to unicode. Removed from responder list.".format(sender))
        return set(emails_list)

class IMAPHandler(object):
    """
    A class to handle the export of responding email addresses from our Zoho folders via IMAP

    Attributes:
        accounts (dict) : a multi-level dict containing all email account information by site
        site (basestring) : the site to export emails from, equivalent to top-level keys in the accounts dict
        account (basestring) : the account to export emails from, equivalent to second-level keys in the accounts dict
        password (basestring) : the password for self.account, equivalent to second-level values in accounts dict
        IMAP_SERVER (basestring) : Zoho.com's IMAP server
        PORT (int) : standard IMAP port
        imap_cxn (imaplib.IMAP4_SSL) : connection to Zoho's IMAP server
        folders (list) : list of folder names to be queried for email responses
        emails_list (list) : list of non-unique email "from" email addresses found by querying self.folders
        top_level_folder (basestring) : user-defined top-level campaign folder to be used to find all child folders

    """

    def __init__(self):
        """Initialize an IMAPHandler() object."""
        self.accounts = email_acct_creds
        self.site = None
        self.account = None
        self.password = None
        self.IMAP_SERVER = None
        self.PORT = 993
        self.imap_cxn = None
        self.folders = []
        self.emails_list = []
        self.top_level_folder = None
        self.zoho_failure = False
        self.inventory_cxn = None

    def connect_inventory_cxn(self):
        """
        Sets up a connection to the contact inventory database

        Returns:
            None
        """

        self.inventory_cxn = DBConnection('root', 'vagrant', '127.0.0.1', 'contact_inventory')
        self.inventory_cxn.connect()
        return

    def close_inventory_cxn(self):
        """
        Closes the inventory connection

        Returns:
            None
        """

        self.inventory_cxn.close()
        return

    def update_db_count(self):
        self.connect_inventory_cxn()
        statement = "INSERT INTO email_acct_use_counter (email_acct, site, last_date) VALUES (%s, %s, %s) " \
                    "ON DUPLICATE KEY UPDATE count=count+1, last_date=curdate()"
        values = tuple([self.account, self.site, datetime.date.today().strftime('%Y-%m-%d')])
        self.inventory_cxn.execute_update(statement, values)
        self.close_inventory_cxn()

    def prompt_account(self):
        """Prompt user to make selection from list of email accounts."""
        print_message("Please select the this campaign's email account.")
        self.account = selection_from_list(self.accounts[self.site], list_name=self.site, indent=2)
        self.password = self.accounts[self.site][self.account]
        return

    def prompt_site(self):
        """Prompt user to make selection from list of Abuv websites.

        Calls self.prompt_account() because self.account and self.password are the imperative data points. Self.site
        is just a logical way to subdivide possible choices.
        """
        print_message("Please select this campaign's site.")
        self.site = selection_from_list(self.accounts, list_name=u'sites')
        self.connect_imap_server()
        self.prompt_account()
        return

    def connect_imap_server(self):
        self.IMAP_SERVER = domain_imap_servers[self.site]
        self.imap_cxn = imaplib.IMAP4_SSL(self.IMAP_SERVER, self.PORT)

    def connect(self):
        """Create IMAP connection to server and assign to self.imap_cxn. Log in to user-defined email account."""
        try:
            self.imap_cxn = imaplib.IMAP4_SSL(self.IMAP_SERVER, self.PORT)
        except:
            print_message("Error connecting to IMAP server.")
        try:
            self.imap_cxn.login(self.account, self.password)
        except imaplib.IMAP4.error:
            print_message("Error logging into {}".format(self.account))
        return

    def prompt_folder(self):
        """Prompt user to make selection from top-level folders in self.account.

        Adds selection to self.folders before returning because we need to export responses in this folder also.
        """
        response, data = self.imap_cxn.list()
        folder_list = [folder.split('"')[-2] for folder in data]
        top_level_folders = sorted(set([folder.split('/')[0] for folder in folder_list]))
        self.top_level_folder = selection_from_list(top_level_folders, list_name=self.account, indent=3)
        self.folders.append(self.top_level_folder)
        return

    def find_folders(self):
        """Find all subfolders of self.top_level_folders and appends to self.folders."""
        response, subfolders = self.imap_cxn.list(self.top_level_folder)
        for subfolder in subfolders:
            if subfolder:
                folder_name = subfolder.split('"')[-2]
                if folder_name not in self.folders:
                    self.folders.append(folder_name)
        return

    def retrieve_folder_data(self, folder):
        """Retrieve all messages from folder, extracts 'From' email address, and appends to self.emails_list."""
        response, message_count = self.imap_cxn.select(folder)
        try:
            count = int(message_count[0])
        except ValueError:
            print_message("=== ERROR ===")
            print_message("Error extracting data from a folder.")
            print_message("Account: {}".format(self.account))
            print_message("Folder: {}".format(folder))
            print_message("Try renaming the folder in Zoho temporarily and rerunning the script.")
            print_message("=== PLEASE RETRY THIS MAILER ===\n")
            # sys.exit()
            raise ZohoFailureError
        if count > 0:
            messages = self.imap_cxn.fetch('1:{}'.format(count), '(BODY.PEEK[HEADER])')
            for message in messages[1]:
                if message != ')':
                    try:
                        msg = email.message_from_string(message[1])
                    except AttributeError as e:
                        print_message('AttributeError: {}'.format(str(e)))
                    if '<' in msg['From']:
                        tmp = msg['From'].split('<')
                        sender = tmp[1].split('>')
                        sender = sender[0].lower()
                    else:
                        sender = msg['From'].lower()
                    try:
                        sender = set_string_to_unicode(sender)
                        self.emails_list.append(sender)
                    except:
                        print_message("Could not convert {} to unicode. Removed from responder list.".format(sender))
        return

    def get_responders(self):
        """Call all methods necessary to export responding email addresses from hierarchy of folders.

        Returns:
            set(self.emails_list) : a set generated from self.emails_list, a unique list of responding email addresses
             in campaign folders
        """
        self.prompt_site()
        self.update_db_count()
        self.connect()
        self.prompt_folder()
        self.find_folders()
        try:
            for folder in self.folders:
                self.retrieve_folder_data(folder)
        except ZohoFailureError:
            raise ZohoFailureError
        return set(self.emails_list)


if __name__ == "__main__":
    # enable external use of module as script
    # will not work unless run from Marketing > Tools > Export Folders Script
    import csv, os, time, datetime

    I = IMAPHandler()
    I.prompt_site()
    I.connect()
    I.prompt_folder()

    start = time.time()

    I.find_folders()
    for folder in I.folders:
        I.retrieve_folder_data(folder)
    email_set = set(I.emails_list)

    end = time.time()
    timestamp = datetime.datetime.fromtimestamp(end).strftime('%m.%d.%y')
    out_file_location = u'.\Outfiles\{}_{}.csv'.format(I.top_level_folder.replace(' ', '-'), timestamp)
    with open(out_file_location, 'wb') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['Email'])
        for email in email_set:
            writer.writerow([set_unicode_to_string(email)])

    time_passed = end - start
    print_message("Completed in {} seconds.\n".format(time_passed))

    os.startfile(out_file_location)
