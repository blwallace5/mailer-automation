import csv
import os


def set_string_to_unicode(s):
    """
    Sets an object to unicode if it is a non-unicode string, otherwise ignores it.

    Args:
        s : a text object to be potentially converted to a unicode object
    """

    encodings_to_try = ['utf-8', 'ascii', 'latin_1', 'iso8859_2']
    for encoding in encodings_to_try:
        try:
            if isinstance(s, basestring) and not isinstance(s, unicode):
                s = unicode(s, encoding)
        except UnicodeDecodeError:
            continue
    return s


def set_unicode_to_string(u, encoding='utf-8'):
    """
    Converts a unicode text object to a string object for writing/printing

    Args:
        u : a text unicode object to be converted to a string
        encoding (string) : optionally specifies the encoding to use for unicode object to string, defaults to utf-8
    """

    if isinstance(u, basestring) and not isinstance(u, str):
        u = u.encode(encoding)
    return u


class CSVImporter(object):
    """
    A class to import text from a CSV

    Attributes:
        filename (basestring): the name of the CSV file in the local 'resources' directory to be imported
        data_list (list): a list of lists containing the data from the CSV import file
        headers_list (list): a list of header fields from the imported CSV file
        data_dict (dict) : a dict containing all of the data from the CSV import file in {header : [col data]} form
        headers_dict (dict) : a dict containing all of the headers from the CSV import file in {pos : header} form
    """

    def __init__(self, filename):
        """
        Initializes the CSVImporter class and calls the import function

        Args:
            filename (str): the name of the CSV file in the local directory to read from
        """

        self.filename = os.path.join(os.path.dirname(__file__), 'run_configuration/infiles/', filename)
        self.data_list = []
        self.headers_list = []
        self.data_dict = {}
        self.headers_dict = {}

    def import_csv(self):
        """
        Reads data from the object's filename attribute and transfers that data into the object's rows and headers

        Returns:
            None
        """

        with open(self.filename, 'rb') as csvfile:
            reader = csv.reader(csvfile)
            header_passed = False
            for line in reader:
                unicode_line = [set_string_to_unicode(col) for col in line]
                if not header_passed:
                    self.headers_list = unicode_line
                    header_passed = True
                    continue
                else:
                    self.data_list.append(unicode_line)
        return

    def import_csv_no_headers(self):
        """
        Reads data from the object's filename attribute and transfers that data into the object's rows attribute.

        Returns:
            None
        """

        with open(self.filename, 'rb') as csvfile:
            reader = csv.reader(csvfile)
            for line in reader:
                unicode_line = [set_string_to_unicode(col) for col in line]
                self.data_list.append(unicode_line)
        return

    def import_csv_to_dict(self, value_type=0):
        """
        Reads data from the object's filename attribute and transfers that data into the a dict containing the data,
            allowing data to be read in with unknown headers and an arbitrary number of columns

        Default value for type is 0, dict values contained in list. If type=1, dict values in set.

        Args:
            value_type (int) : Optional. The type of dict values that will be used

        Returns:
            None
        """

        with open(self.filename, 'rb') as csvfile:
            reader = csv.reader(csvfile)
            for line in reader:
                unicode_line = [set_string_to_unicode(col) for col in line]
                if not self.headers_dict:
                    pos = 0
                    for header in unicode_line:
                        fixed_header = header.strip().lower().replace(u' ', u'_')
                        self.headers_dict[pos] = fixed_header
                        pos += 1
                        if not value_type:
                            self.data_dict[fixed_header] = []
                        else:
                            self.data_dict[fixed_header] = set()
                else:
                    pos = 0
                    for data in unicode_line:
                        column_header = self.headers_dict[pos]
                        if not value_type:
                            self.data_dict[column_header].append(data)
                        else:
                            self.data_dict[column_header].add(data)
                        pos += 1
        return


class CSVExporter(object):
    """
    A class to export data to a CSV

    Attributes:
        data_list (list): a list of lists containing data to be written to CSV
        headers_list (list): a list containing the headers for data to be written to CSV
        filename (str): the name of the CSV file in the local directory to be write to
        data_dict (dict) : a dict containing all of the data for the CSV export file in {header : [col data]} form
        headers_dict (dict) : a dict containing all of the headers for the CSV export file in {pos : header} form
    """

    def __init__(self, filename, data_list=None, headers_list=None, data_dict=None, headers_dict=None):
        """
        Initializes the CSVExporter class

        Args:
            data_list (list): a list of lists containing data to be written to CSV
            headers_list (list): a list containing the headers for data to be written to CSV
            filename (basestring): the name of the CSV file in the local directory to be write to
            data_dict (dict) : a dict containing all of the data for the CSV export file in {header : [col data]} form
            headers_dict (dict) : a dict containing all of the headers for the CSV export file in {pos : header} form
        """

        self.data_list = data_list
        self.headers_list = headers_list
        self.filename = os.path.join(os.path.dirname(__file__), 'run_configuration/outfiles/', filename)
        self.data_dict = data_dict
        self.headers_dict = headers_dict

    def write_csv(self):
        """
        Writes the data and headers attributes to filename CSV

        Returns:
            None
        """

        with open(self.filename, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(self.headers_list)
            for row in self.data_list:
                string_row = [set_unicode_to_string(col) for col in row]
                writer.writerow(string_row)
        return

    def write_csv_from_dict(self):
        """
        Writes the data from the full_data_dict data to filename CSV

        Returns:
            None
        """

        with open(self.filename, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            headers = [header for (pos, header) in sorted(self.headers_dict.iteritems())]
            string_headers = [set_unicode_to_string(col) for col in headers]
            writer.writerow(string_headers)
            header_warnings = set()
            row_num = 0
            out_of_range = False
            while not out_of_range:
                string_row = []
                for header in headers:
                    try:
                        string_row.append(set_unicode_to_string(self.data_dict[header][row_num]))
                    except AttributeError:
                        string_row.append('')
                    except KeyError:
                        if header not in header_warnings:
                            print("No data for header {} in data set.".format(header))
                            header_warnings.add(header)
                        continue
                    except IndexError:
                        out_of_range = True
                        break
                writer.writerow(string_row)
                row_num += 1
        return