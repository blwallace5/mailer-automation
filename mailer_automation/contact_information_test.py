import unittest
from mock import patch, PropertyMock
import contact_classes
import time
from copy import deepcopy


class TestContact(unittest.TestCase):

    def setUp(self):
        email_patcher = patch('contact_information.EmailEntry')
        self.addCleanup(email_patcher.stop)
        self.mock_email_entry = email_patcher.start()
        mock_email = PropertyMock(return_value='email@example.com')
        type(self.mock_email_entry).email = mock_email
        domain_patcher = patch('contact_information.DomainEntry')
        self.addCleanup(domain_patcher.stop)
        self.mock_source_entry = domain_patcher.start()
        self.mock_target_entry = deepcopy(self.mock_source_entry)
        mock_source_domain = PropertyMock(return_value='sourcedomain.com')
        type(self.mock_source_entry).domain = mock_source_domain
        mock_target_domain = PropertyMock(return_value='targetdomain.com')
        type(self.mock_target_entry).domain = mock_target_domain
        asset_patcher = patch('contact_information.AssetEntry')
        self.addCleanup(asset_patcher.stop)
        self.mock_asset_entry = asset_patcher.start()
        mock_asset = PropertyMock(return_value='asset')
        mock_campaign = PropertyMock(return_value='campaign')
        mock_variable = PropertyMock(return_value='variable')
        mock_context = PropertyMock(return_value='context')
        type(self.mock_asset_entry).asset = mock_asset
        type(self.mock_asset_entry).campaign = mock_campaign
        type(self.mock_asset_entry).variable = mock_variable
        type(self.mock_asset_entry).context = mock_context
        self.dummy_name = 'FirstName LastName'
        self.contact = contact_classes.Contact(self.dummy_name, self.mock_email_entry, self.mock_source_entry,
                                               self.mock_target_entry, self.mock_asset_entry)

    def test_contactInitializationContactNameSetToContactNameArgument(self):
        self.assertEqual(self.contact.contact_name, self.dummy_name)
    
    def test_contactInitializationContactEmailEntryFieldsSet(self):
        self.assertEqual(self.contact.email_entry.email, 'email@example.com')
        
    def test_contactInitializationContactSourceEntryFieldsSet(self):
        self.assertEqual(self.contact.source_entry.domain, 'sourcedomain.com')
        
    def test_contactInitializationContactTargetEntryFieldsSet(self):
        self.assertEqual(self.contact.target_entry.domain, 'targetdomain.com')
        
    def test_contactInitializationContactAssetEntryFieldsSet(self):
        self.assertEqual(self.contact.asset_entry.asset, 'asset')
        self.assertEqual(self.contact.asset_entry.campaign, 'campaign')
        self.assertEqual(self.contact.asset_entry.variable, 'variable')
        self.assertEqual(self.contact.asset_entry.context, 'context')


class TestEmailEntry(unittest.TestCase):
    
    def setUp(self):
        self.dummy_email = 'dummy_email@example.com'
        self.email_entry = contact_classes.EmailEntry(self.dummy_email)
        
    def test_emailEntryInitializationEmailAddress(self):
        self.assertEqual(self.email_entry.email, self.dummy_email)
    
    def test_emailEntryInitializationDateCurrentDate(self):
        current_date = time.strftime('%m/%d/%Y')
        self.assertEqual(self.email_entry.date, current_date)
    
    def test_emailEntryInitializationResultIsNone(self):
        self.assertIsNone(self.email_entry.result)
        
    def test_emailEntryInitializationReasonIsNone(self):
        self.assertIsNone(self.email_entry.reason)
        
    def test_emailEntryInitializationSendexIsNone(self):
        self.assertIsNone(self.email_entry.sendex)
        
    def test_emailEntryInitializationSourceIsNone(self):
        self.assertIsNone(self.email_entry.source)
    
    def test_emailEntryInitializationActionIsNone(self):
        self.assertIsNone(self.email_entry.action)
        

class TestDomainEntry(unittest.TestCase):
    
    def setUp(self):
        self.dummy_domain = 'dummy_domain.com'
        self.dummy_url = 'http://www.dummyurl.com'
        self.dummy_trust = 'trust_flow'
        self.dummy_topic = 'topic'
        self.dummy_topic_trust = 'topic_trust'
        self.domain_entry = contact_classes.DomainEntry(self.dummy_domain, self.dummy_url, self.dummy_trust,
                                                        self.dummy_topic, self.dummy_topic_trust)
        
    def test_domainEntryInitializationDomain(self):
        self.assertEqual(self.domain_entry.domain, self.dummy_domain)
    
    def test_domainEntryInitializationURL(self):
        self.assertEqual(self.domain_entry.url, self.dummy_url)
    
    def test_domainEntryInitializationTrustFlow(self):
        self.assertEqual(self.domain_entry.trust_flow, self.dummy_trust)
        
    def test_domainEntryInitializationTopic(self):
        self.assertEqual(self.domain_entry.topic, self.dummy_topic)
        
    def test_domainEntryInitializationTopicTrustFlow(self):
        self.assertEqual(self.domain_entry.topic_trust_flow, self.dummy_topic_trust)
        
    def test_domainEntryInitializationIPAddressIsNone(self):
        self.assertIsNone(self.domain_entry.ip_address)
    
    def test_domainEntryInitializationWhoisEntryIsNone(self):
        self.assertIsNone(self.domain_entry.whois_entry)
        
    def test_domainEntryInitializationIpGeoEntryIsNone(self):
        self.assertIsNone(self.domain_entry.ip_geo_entry)
        
    def test_domainEntryInitializationTLDEntryIsNone(self):
        self.assertIsNone(self.domain_entry.tld_entry)


class TestAssetEntry(unittest.TestCase):
    
    def setUp(self):
        self.dummy_asset = 'asset'
        self.dummy_campaign = 'campaign'
        self.dummy_variable = 'variable'
        self.dummy_context = 'context'
        self.asset_entry = contact_classes.AssetEntry(self.dummy_asset, self.dummy_campaign, self.dummy_variable,
                                                      self.dummy_context)

    def test_assetEntryInitializationAsset(self):
        self.assertEqual(self.asset_entry.asset, self.dummy_asset)

    def test_assetEntryInitializationCampaign(self):
        self.assertEqual(self.asset_entry.campaign, self.dummy_campaign)

    def test_assetEntryInitializationVariable(self):
        self.assertEqual(self.asset_entry.variable, self.dummy_variable)

    def test_assetEntryInitializationContext(self):
        self.assertEqual(self.asset_entry.context, self.dummy_context)

if __name__ == '__main__':
    unittest.main(buffer=True)
