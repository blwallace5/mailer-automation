'''
To do:

1. Save outfiles to subdirectory
2. House db and log files in subdirectory (hidden?)
3. Better DB update logic
    - Don't cache unknowns
    - Cache unknowns differently -- if returned as unknown 3 times, then cut forever, etc
    - 

'''

import kickbox
import csv
import time
import datetime
import sqlite3
import os
from multiprocessing.dummy import Pool as ThreadPool


class EmailEntry(object):
    """
    A class to represent the validation results for an email address.
    
    Attributes:
        email (str) : the email address (e.g., name@example.com)
        result (str) : the validation result for the email (deliverable, undeliverable, risky, or unknown)
        score (float) : A 0-1 score representing email quality/deliverability
        success (boolean) : true if the API request was successful
        
    """
    
    
    def __init__(self, email):
        """
        Initializes an EmailEntry object.
        
        All attributes except for email are initialized as None. Email is required upon initialization.
        
        Args:
            email (str) : the email address (e.g., name@example.com)
            
        """
        
        self.email = email
        self.date = time.strftime('%m/%d/%Y')
        self.result = None
        self.reason = None
        self.sendex = None
        self.source = None


class CacheManager(object):
    """
    A class to handle import from and export to cache database.
    """
    
    def __init__(self):
        self.cache_dict = {}
        self.conn = sqlite3.connect('.\Cache\kickbox_cache.db')
        self.c = self.conn.cursor()
        
    def import_from_db(self):
        
        select = self.c.execute('SELECT * FROM cache')
        all = select.fetchall()
        for i in all:
            self.cache_dict[i[0].lower()] = i[1].lower()
        
    def get_cache_dict(self):
        """
        Returns the dict built by import_from_csv.
        """
        return self.cache_dict
        
    def update_db(self, new_entries):
    
        print ("Adding {} new entries to database...".format(len(new_entries)))
        
        for entry in new_entries:
            self.c.execute('INSERT INTO cache VALUES (?,?,?,?,?)', (entry.email, entry.result, entry.reason, entry.sendex, entry.date))
        
        self.conn.commit()
        
        print ("Complete!\n")
    
        
class EmailValidator(object):
    """
    A class used to process all emails. Email addresses are ready into memory from a CSV, then checked individually against Kickbox API. This class does most of the module's heavy lifting.
    
    Attributes:
        email_entries (list) : list of all email addresses submitted
        email_set (set) : set of all unique email addresses to be validated
    """
    
    def __init__(self, cache):
        """
        Initializes an EmailValidator object. 
        """
        self.email_entries = []
        self.email_set = set([])
        self.db_updates = []
        self.cache = cache
        self.cached_emails = cache.keys()
        self.kickbox_instance = self.instantiate_client()
        self.timeout = 1000
    
    def instantiate_client(self):
    
        APIkey = '3e7f950cc61833203c66772bde6504a7ae8aaad74ca635898152c91dd200664c'
        client = kickbox.Client(APIkey)
        kickbox_instance = client.kickbox()
        
        return kickbox_instance
        
    def email_list_from_csv(self, file_location):
        """
        Reads each line of csv infile individually and passes to process_csv_line for parsing.
        """
        
        print "\nReading infile..."
        
        with open(file_location, 'rb') as csvfile:
            reader = csv.reader(csvfile)
            header_passed = False
            
            for line in reader:
            
                if not header_passed:
                    header_passed = True
                    continue
                    
                self.process_csv_line(line)
        
        print "Infile read complete!\n"
        return
        
    def process_csv_line(self, line):
        """
        Receives single lines from csv infile and extracts email addresses. If email not already in set, adds to set.
        """
        
        email = line[0].lower()
        
        if email not in self.email_set:
            email_entry = EmailEntry(email)
            self.email_entries.append(email_entry)
            self.email_set.add(email)
            
        return
    
    def print_all(self):
        """
        Method used to test csv processing
        """
        for entry in self.email_entries:
            print ("{}: {}".format(entry.email, entry.result))
    
    def validate_all_emails(self):
    
        print "Validating emails. This could take a while..."
                
        pool = ThreadPool(8)
        pool.map(self.validate_email, self.email_entries)
        
        print "Complete!\n"
        return
    
    def validate_email(self, email_entry):
    
        email = email_entry.email
        
        if email in self.cache:
            email_entry.result = self.cache[email_entry.email]
            email_entry.source = 'Cache'
        else:
            email_entry.source = 'Lookup'
            results = self.kickbox_instance.verify(email, {'timeout': self.timeout})
                
            email_entry.result = results.body['result']
            email_entry.reason = results.body['reason']
            email_entry.sendex = results.body['sendex']
        
            self.db_updates.append(email_entry)
        
        return
        
    def write_results(self, file_location):
    
        with open(file_location, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            header = ['Email', 'Action', 'Result', 'Source']
            
            writer.writerow(header)
            print("Writing information to outfile...")
            
            for email_entry in self.email_entries:
                email = email_entry.email
                result = email_entry.result
                source = email_entry.source
                if result == 'unknown' or result == 'undeliverable':
                    action = 'Cut'
                else:
                    action = 'Keep'
                
                new_line = [email, action, result, source]
                
                writer.writerow(new_line)
                
            return
    
    def update_log(self, time_passed):
    
        with open('.\Cache\execution_time_log.csv', 'ab') as csvfile:
            writer = csv.writer(csvfile)
            
            size = len(self.email_entries)
            lookups = len(self.db_updates)
            in_cache = size - lookups
            total_time = time_passed
            time_per = total_time / size
            date = time.strftime('%m/%d/%Y')
            
            new_line = [size, lookups, in_cache, total_time, time_per, date]
            
            writer.writerow(new_line)
                
            return
        

if __name__ == '__main__':
    in_file_location = raw_input("\nName of infile, including extension (e.g. infile.csv) : ")
    
    start = time.time()
    
    cache_manager = CacheManager()
    cache_manager.import_from_db()
    cache = cache_manager.get_cache_dict()
    
    email_validator = EmailValidator(cache)
    email_validator.email_list_from_csv(in_file_location)
    email_validator.validate_all_emails()
    
    cache_manager.update_db(email_validator.db_updates)
    
    ts = time.time()
    timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y_%m_%d_%H_%M_%S')
    out_file_location = '.\Outfiles\outfile_{}.csv'.format(timestamp)
    email_validator.write_results(out_file_location)
    
    time_passed = time.time()-start
    print ("Completed in {} seconds.\n".format(time_passed))
    
    email_validator.update_log(time_passed)
    
    os.startfile(out_file_location)