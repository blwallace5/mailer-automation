import unittest
from mock import patch, Mock, PropertyMock
import check_domains
import whoisxml_connection


class TestDomainEntry(unittest.TestCase):

    def setUp(self):
        self.dummy_domain = 'www.example.com'
        self.domain_entry = check_domains.DomainEntry(self.dummy_domain)

    def test_domainEntryInitialization(self):
        self.assertEqual(self.domain_entry.domain, self.dummy_domain)
        self.assertIsNone(self.domain_entry.ip_address)
        self.assertIsNone(self.domain_entry.whois_entry)
        self.assertIsNone(self.domain_entry.ip_geo_entry)
        self.assertIsNone(self.domain_entry.tld_entry)


class TestTLDEntry(unittest.TestCase):

    def setUp(self):
        self.dummy_domain = 'www.example.com'
        self.tld_entry = check_domains.TLDEntry(self.dummy_domain)

    def test_tldEntryInitialization(self):
        self.assertEqual(self.tld_entry.domain, self.dummy_domain)
        self.assertIsNone(self.tld_entry.tld)
        self.assertIsNone(self.tld_entry.tld_type)

    def test_enterTldResultsUpdatesTldAndTldTypeAttributes(self):
        self.assertIsNone(self.tld_entry.tld)
        self.assertIsNone(self.tld_entry.tld_type)
        self.tld_entry.enter_tld_results('com', 'Not Blacklist')
        self.assertEqual(self.tld_entry.tld, 'com')
        self.assertEqual(self.tld_entry.tld_type, 'Not Blacklist')

    def test_getTldTypeReturnsTldTypeAttribute(self):
        self.assertIsNone(self.tld_entry.tld_type)
        self.tld_entry.enter_tld_results('com', 'Not Blacklist')
        self.assertEqual(self.tld_entry.get_tld_type(), 'Not Blacklist')


class TestWhoisEntry(unittest.TestCase):

    def setUp(self):
        self.dummy_domain = 'www.example.com'
        self.whois_entry = check_domains.WHOISEntry(self.dummy_domain)

    def test_whoisEntryInitialization(self):
        self.assertEqual(self.whois_entry.domain, self.dummy_domain)
        self.assertIsNone(self.whois_entry.country)
        self.assertIsNone(self.whois_entry.tech_country)
        self.assertIsNone(self.whois_entry.admin_country)
        self.assertIsNone(self.whois_entry.country_locale)
        self.assertIsNone(self.whois_entry.tech_locale)
        self.assertIsNone(self.whois_entry.admin_locale)

    @patch('whoisxml.WhoisXMLHandler')
    def test_enterWhoisXmlResultsSetsCountriesAndLocales_whenCalledWithAllAvailableInformation(self, mock_xml_handler):
        mock_xml_handler.get_country.return_value = 'test_country'
        dummy_locale_dict = {'test_country': 'test status'}
        self.whois_entry.enter_whois_xml_results(mock_xml_handler, dummy_locale_dict)
        self.assertEqual(self.whois_entry.country, 'test_country')
        self.assertEqual(self.whois_entry.tech_country, 'test_country')
        self.assertEqual(self.whois_entry.admin_country, 'test_country')
        self.assertEqual(self.whois_entry.country_locale, 'test status')
        self.assertEqual(self.whois_entry.admin_locale, 'test status')
        self.assertEqual(self.whois_entry.tech_locale, 'test status')

    @patch('whoisxml.WhoisXMLHandler')
    def test_enterWhoisXmlResultsSetsLocalesAsUnknown_whenCalledWithNoInformationInDict(self, mock_xml_handler):
        mock_xml_handler.get_country.return_value = 'test_country'
        dummy_locale_dict = {}
        self.whois_entry.enter_whois_xml_results(mock_xml_handler, dummy_locale_dict)
        self.assertEqual(self.whois_entry.country, 'test_country')
        self.assertEqual(self.whois_entry.tech_country, 'test_country')
        self.assertEqual(self.whois_entry.admin_country, 'test_country')
        self.assertEqual(self.whois_entry.country_locale, 'unknown')
        self.assertEqual(self.whois_entry.admin_locale, 'unknown')
        self.assertEqual(self.whois_entry.tech_locale, 'unknown')

    @patch('whoisxml.WhoisXMLHandler')
    def test_enterWhoisXmlResultsSetsCountriesAsBlank_whenCalledXmlHandlerDoesNotHaveCountryInfo(self, mock_xml_handler):
        mock_xml_handler.get_country.side_effect = Exception
        dummy_locale_dict = {}
        self.whois_entry.enter_whois_xml_results(mock_xml_handler, dummy_locale_dict)
        self.assertEqual(self.whois_entry.country, '')
        self.assertEqual(self.whois_entry.tech_country, '')
        self.assertEqual(self.whois_entry.admin_country, '')
        self.assertEqual(self.whois_entry.country_locale, 'unknown')
        self.assertEqual(self.whois_entry.admin_locale, 'unknown')
        self.assertEqual(self.whois_entry.tech_locale, 'unknown')

    def test_getCountryNameReturnsCountryAttributes(self):
        self.whois_entry.country = 'United States'
        self.whois_entry.tech_country = 'Canada'
        self.whois_entry.admin_country = 'Russia'
        self.assertTupleEqual(self.whois_entry.get_country_names(),
                              (self.whois_entry.country, self.whois_entry.admin_country, self.whois_entry.tech_country))

    def test_getLocaleNameReturnsCountryLocaleAttributes(self):
        self.whois_entry.country_locale = 'Domestic'
        self.whois_entry.tech_locale = 'International'
        self.whois_entry.admin_locale = 'International'
        self.assertTupleEqual(self.whois_entry.get_locale_names(),
                              (self.whois_entry.country_locale, self.whois_entry.admin_locale,
                               self.whois_entry.tech_locale))


class TestIPGeoEntry(unittest.TestCase):

    def setUp(self):
        self.dummy_ip = '12.34.567.890'
        self.ipgeo_entry = check_domains.IPGeoEntry(self.dummy_ip)

    def test_ipGeoEntryInitialization(self):
        self.assertEqual(self.ipgeo_entry.ip_address, self.dummy_ip)
        self.assertIsNone(self.ipgeo_entry.country_name, None)
        self.assertIsNone(self.ipgeo_entry.locale, None)

    def test_enterGeoResultsSetsCountryNameAndLocale_whenCalledWithAllAvailableInformation(self):
        mock_ip_result = Mock(country=Mock())
        temp_prop = PropertyMock(return_value='USA')
        type(mock_ip_result.country).name = temp_prop
        mock_locale_dict = {'usa': 'domestic'}
        self.ipgeo_entry.enter_geo_results(mock_ip_result, mock_locale_dict)
        self.assertEqual(self.ipgeo_entry.country_name, 'USA')
        self.assertEqual(self.ipgeo_entry.locale, 'domestic')

    def test_enterGeoResultSetsCountryNameAsUnknown_whenIPGeoResultHandlerRaisesError(self):
        mock_ip_result = Mock()
        mock_ip_result.country = Exception
        mock_locale_dict = {}
        self.ipgeo_entry.enter_geo_results(mock_ip_result, mock_locale_dict)
        self.assertEqual(self.ipgeo_entry.country_name, 'unknown')
        self.assertEqual(self.ipgeo_entry.locale, 'unknown')

    def test_enterGeoResultSetsCountryNameAsUnknown_whenCountryNotFoundInLocaleDict(self):
        mock_ip_result = Mock()
        mock_ip_result.country.name = 'USA'
        mock_locale_dict = {}
        self.ipgeo_entry.enter_geo_results(mock_ip_result, mock_locale_dict)
        self.assertEqual(self.ipgeo_entry.country_name, 'USA')
        self.assertEqual(self.ipgeo_entry.locale, 'unknown')

    def test_getCountryNameReturnsCountryNameAttribute(self):
        self.ipgeo_entry.country_name = 'USA'
        self.assertEqual(self.ipgeo_entry.get_country_name(), self.ipgeo_entry.country_name)

    def test_getLocaleNameReturnsLocaleAttribute(self):
        self.ipgeo_entry.locale = 'domestic'
        self.assertEqual(self.ipgeo_entry.get_locale_name(), self.ipgeo_entry.locale)


class TestCSVToDictCompiler(unittest.TestCase):

    def setUp(self):
        self.dummy_file_location = 'folder/file.csv'
        self.csv_dict_compiler = check_domains.CSVToDictCompiler(self.dummy_file_location)

    def test_csvDictCompilerInitialization(self):
        self.assertEqual(self.csv_dict_compiler.file_location, self.dummy_file_location)
        self.assertDictEqual(self.csv_dict_compiler.csv_dict, {})

    def test_getCsvDictReturnsCsvDictAttribute(self):
        self.csv_dict_compiler.csv_dict = {'Test': 'Dict'}
        self.assertDictEqual(self.csv_dict_compiler.get_csv_dict(), self.csv_dict_compiler.csv_dict)


class TestDomainChecker(unittest.TestCase):

    def setUp(self):
        self.dummy_tld_dict = {'com': 'not blacklist', 'ca': 'blacklist'}
        self.dummy_locale_dict = {'USA': 'domestic', 'Canada': 'international', 'Russia': 'international'}
        self.domain_checker = check_domains.DomainChecker(self.dummy_tld_dict, self.dummy_locale_dict)
        self.dummy_domain_entry = check_domains.DomainEntry('www.example.com')
        self.dummy_domain_entry.ip_address = '12.34.567.890'
        self.dummy_domain_entry.whois_entry = check_domains.WHOISEntry('www.example.com')
        self.dummy_domain_entry.whois_entry.country = 'USA'
        self.dummy_domain_entry.whois_entry.tech_country = 'Canada'
        self.dummy_domain_entry.whois_entry.admin_country = 'Russia'
        self.dummy_domain_entry.whois_entry.country_locale = 'domestic'
        self.dummy_domain_entry.whois_entry.tech_locale = 'international'
        self.dummy_domain_entry.whois_entry.admin_locale = 'international'
        self.dummy_domain_entry.ip_geo_entry = check_domains.IPGeoEntry('12.34.567.890')
        self.dummy_domain_entry.ip_geo_entry.country_name = 'USA'
        self.dummy_domain_entry.ip_geo_entry.locale = 'domestic'
        self.dummy_domain_entry.tld_entry = check_domains.TLDEntry('www.example.com')
        self.dummy_domain_entry.tld_entry.tld = 'com'
        self.dummy_domain_entry.tld_entry.tld_type = 'not blacklist'

    def test_domainCheckerInitialization(self):
        self.assertListEqual(self.domain_checker.domain_entries, [])
        self.assertSetEqual(self.domain_checker.domain_set, set([]))
        self.assertDictEqual(self.domain_checker.tld_dict, self.dummy_tld_dict)
        self.assertDictEqual(self.domain_checker.location_dict, self.dummy_locale_dict)
        self.assertListEqual(self.domain_checker.domain_check_results, [])
        self.assertDictEqual(self.domain_checker.domain_check_results_dict, {})

    def test_processDomainCheckResultsReturnsCorrectLines_whenCalledWithInformation(self):
        self.expected_results = [['www.example.com', 'USA', 'Russia', 'Canada', 'USA', 'domestic', 'international',
                                 'international', 'domestic', 'not blacklist', 'Cut', 'Blacklist']]
        self.domain_checker.domain_entries.append(self.dummy_domain_entry)
        self.assertListEqual(self.domain_checker.process_domain_check_results(), self.expected_results)

    @patch('check_domains.WHOISEntry.get_country_names')
    def test_processDomainCheckResultsYieldsUnknownRegistrarLocales_whenDomainEntryMissingRegistrarInformation(self, mock_get_cname):
        mock_get_cname.side_effect = Exception
        self.expected_results = [['www.example.com', '', '', '', 'USA', 'unknown', 'unknown',
                                 'unknown', 'domestic', 'not blacklist', 'Keep', '2wks']]
        self.domain_checker.domain_entries.append(self.dummy_domain_entry)
        self.assertListEqual(self.domain_checker.process_domain_check_results(), self.expected_results)

    @patch('check_domains.IPGeoEntry.get_country_names')
    def test_processDomainCheckResultsYieldsUnknownRegistrarLocales_whenDomainEntryMissingRegistrarInformation(self, mock_get_cname):
        mock_get_cname.side_effect = Exception
        self.expected_results = [['www.example.com', 'USA', 'Russia', 'Canada', '', 'domestic', 'international',
                                 'international', 'unknown', 'not blacklist', 'Keep', '2wks']]
        self.domain_checker.domain_entries.append(self.dummy_domain_entry)
        self.assertListEqual(self.domain_checker.process_domain_check_results(), self.expected_results)

    @patch('check_domains.TLDEntry.get_tld_type')
    def test_processDomainCheckResultsYieldsUnknownRegistrarLocales_whenDomainEntryMissingRegistrarInformation(self, mock_get_tldtype):
        mock_get_tldtype.side_effect = Exception
        self.expected_results = [['www.example.com', 'USA', 'Russia', 'Canada', 'USA', 'domestic', 'international',
                                 'international', 'domestic', 'unknown', 'Cut', 'Blacklist']]
        self.domain_checker.domain_entries.append(self.dummy_domain_entry)
        self.assertListEqual(self.domain_checker.process_domain_check_results(), self.expected_results)

    def test_getDomainCheckResultsDictReturnsDomainCheckResultsDict(self):
        self.domain_checker.domain_check_results_dict = {'Some': 'Dict'}
        self.assertDictEqual(self.domain_checker.get_domain_check_results_dict(), self.domain_checker.domain_check_results_dict)

    def test_domainCheckResultsToDictTransformsListToDict_withZeroIndexAsKeyAndRemainingListAsValues(self):
        self.domain_checker.domain_check_results = [['www.example.com', '1', '2', '3', '4', '5'],
                                                    ['www.example.net', 'a', 3, 'string']]
        self.expected_results = {'www.example.com': ['1', '2', '3', '4', '5'], 'www.example.net': ['a', 3, 'string']}
        self.domain_checker.domain_check_results_to_dict()
        self.assertDictEqual(self.domain_checker.domain_check_results_dict, self.expected_results)

    def test_checkActionWithIIIIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithIIIDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithIIIUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithIIDIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithIIDDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithIIDUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithIIUIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithIIUDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithIIUUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithIDIIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithIDIDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithIDIUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithIDDIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithIDDDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithIDDUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithIDUIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithIDUDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithIDUUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithIUIIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithIUIDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithIUIUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithIUDIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithIUDDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithIUDUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithIUUIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithIUUDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithIUUUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithDIIIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithDIIDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithDIIUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithDIDIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithDIDDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithDIDUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithDIUIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithDIUDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithDIUUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithDDIIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithDDIDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithDDIUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithDDDIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithDDDDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithDDDUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithDDUIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithDDUDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithDDUUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithDUIIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithDUIDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithDUIUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithDUDIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithDUDDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithDUDUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithDUUIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithDUUDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithDUUUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithUIIIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithUIIDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithUIIUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithUIDIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithUIDDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithUIDUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithUIUIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithUIUDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithUIUUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithUDIIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithUDIDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithUDIUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithUDDIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithUDDDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithUDDUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithUDUIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithUDUDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithUDUUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithUUIIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithUUIDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithUUIUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithUUDIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithUUDDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithUUDUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithUUUIBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkActionWithUUUDBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkActionWithUUUUBPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkActionWithDDDDNPatternIsKeep(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'domestic', 'domestic', 'not blacklist'), 'Keep')

    def test_checkActionWithDDUDNPatternIsKeep(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'unknown', 'domestic', 'not blacklist'), 'Keep')

    def test_checkActionWithDUDDNPatternIsKeep(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'domestic', 'domestic', 'not blacklist'), 'Keep')

    def test_checkActionWithDUUDNPatternIsKeep(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'unknown', 'domestic', 'not blacklist'), 'Keep')

    def test_checkActionWithUDDDNPatternIsKeep(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'domestic', 'domestic', 'not blacklist'), 'Keep')

    def test_checkActionWithUDUDNPatternIsKeep(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'unknown', 'domestic', 'not blacklist'), 'Keep')

    def test_checkActionWithUUDDNPatternIsKeep(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'domestic', 'domestic', 'not blacklist'), 'Keep')

    def test_checkActionWithIIIINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithIIIDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithIIIUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithIIDINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithIIDDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'domestic', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithIIDUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithIIUINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithIIUDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'unknown', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithIIUUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithIDIINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithIDIDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithIDIUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithIDDINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithIDDDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'domestic', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithIDDUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithIDUINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithIDUDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'unknown', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithIDUUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithIUIINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithIUIDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithIUIUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithIUDINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithIUDDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'domestic', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithIUDUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithIUUINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithIUUDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'unknown', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithIUUUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithDIIINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithDIIDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithDIIUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithDIDINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithDIDDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'domestic', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithDIDUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithDIUINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithDIUDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'unknown', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithDIUUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithDDIINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithDDIDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithDDIUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithDDDINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithDDUINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithDUIINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithDUIDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithDUIUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithDUDINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithDUUINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithUIIINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithUIIDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithUIIUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithUIDINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithUIDDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'domestic', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithUIDUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithUIUINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithUIUDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'unknown', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithUIUUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithUDIINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithUDIDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithUDIUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithUDDINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithUDUINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithUUIINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithUUIDNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkActionWithUUIUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithUUDINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithUUUINPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkActionWithDDDUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithDDUUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithDUDUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithDUUUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithUDDUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithUDUUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithUUDUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkActionWithUUUUNPatternIsCut(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'unknown', 'unknown', 'not blacklist'), 'Unknown Locales! Update Locale List')

    def test_checkActionWithUUUDNPatternIsKeepGovAndLibraryCutTheRest(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'unknown', 'domestic', 'not blacklist'), 'Keep .gov and library, cut the rest')

    def test_checkCacheWithIIIIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithIIIDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithIIIUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithIIDIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithIIDDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithIIDUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithIIUIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithIIUDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithIIUUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithIDIIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithIDIDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithIDIUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithIDDIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithIDDDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithIDDUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithIDUIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithIDUDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithIDUUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithIUIIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithIUIDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithIUIUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithIUDIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithIUDDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithIUDUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithIUUIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithIUUDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithIUUUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithDIIIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithDIIDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithDIIUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithDIDIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithDIDDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithDIDUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithDIUIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithDIUDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithDIUUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithDDIIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithDDIDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithDDIUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithDDDIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithDDDDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithDDDUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithDDUIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithDDUDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithDDUUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithDUIIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithDUIDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithDUIUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithDUDIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithDUDDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithDUDUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithDUUIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithDUUDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithDUUUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithUIIIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithUIIDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithUIIUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithUIDIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithUIDDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithUIDUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithUIUIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithUIUDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithUIUUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithUDIIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithUDIDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithUDIUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithUDDIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithUDDDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithUDDUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithUDUIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithUDUDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithUDUUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithUUIIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'international', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithUUIDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'international', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithUUIUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'international', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithUUDIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'domestic', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithUUDDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'domestic', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithUUDUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'domestic', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithUUUIBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'unknown', 'international', 'blacklist'), 'Cut')

    def test_checkCacheWithUUUDBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'unknown', 'domestic', 'blacklist'), 'Cut')

    def test_checkCacheWithUUUUBPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'unknown', 'unknown', 'blacklist'), 'Cut')

    def test_checkCacheWithDDDDNPatternIs2wks(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'domestic', 'domestic', 'not blacklist'), 'Keep')

    def test_checkCacheWithDDUDNPatternIs2wks(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'unknown', 'domestic', 'not blacklist'), 'Keep')

    def test_checkCacheWithDUDDNPatternIs2wks(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'domestic', 'domestic', 'not blacklist'), 'Keep')

    def test_checkCacheWithDUUDNPatternIs2wks(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'unknown', 'domestic', 'not blacklist'), 'Keep')

    def test_checkCacheWithUDDDNPatternIs2wks(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'domestic', 'domestic', 'not blacklist'), 'Keep')

    def test_checkCacheWithUDUDNPatternIs2wks(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'unknown', 'domestic', 'not blacklist'), 'Keep')

    def test_checkCacheWithUUDDNPatternIs2wks(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'domestic', 'domestic', 'not blacklist'), 'Keep')

    def test_checkCacheWithIIIINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithIIIDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithIIIUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithIIDINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithIIDDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'domestic', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithIIDUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithIIUINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithIIUDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'unknown', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithIIUUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'international', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithIDIINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithIDIDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithIDIUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithIDDINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithIDDDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'domestic', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithIDDUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithIDUINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithIDUDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'unknown', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithIDUUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'domestic', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithIUIINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithIUIDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithIUIUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithIUDINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithIUDDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'domestic', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithIUDUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithIUUINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithIUUDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'unknown', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithIUUUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('international', 'unknown', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithDIIINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithDIIDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithDIIUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithDIDINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithDIDDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'domestic', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithDIDUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithDIUINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithDIUDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'unknown', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithDIUUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'international', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithDDIINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithDDIDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithDDIUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithDDDINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithDDUINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithDUIINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithDUIDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithDUIUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithDUDINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithDUUINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithUIIINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithUIIDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithUIIUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithUIDINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithUIDDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'domestic', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithUIDUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithUIUINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithUIUDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'unknown', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithUIUUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'international', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithUDIINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithUDIDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithUDIUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithUDDINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithUDUINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithUUIINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'international', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithUUIDNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'international', 'domestic', 'not blacklist'), 'Cut')

    def test_checkCacheWithUUIUNPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'international', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithUUDINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'domestic', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithUUUINPatternIsBlacklist(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'unknown', 'international', 'not blacklist'), 'Cut')

    def test_checkCacheWithDDDUNPatternIsNone(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithDDUUNPatternIsNone(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'domestic', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithDUDUNPatternIsNone(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithDUUUNPatternIsNone(self):
        self.assertEqual(self.domain_checker.check_action('domestic', 'unknown', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithUDDUNPatternIsNone(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithUDUUNPatternIsNone(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'domestic', 'unknown', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithUUDUNPatternIsNone(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'domestic', 'unknown', 'not blacklist'), 'Cut')

    def test_checkCacheWithUUUUNPatternIsNone(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'unknown', 'unknown', 'not blacklist'), 'Unknown Locales! Update Locale List')

    def test_checkCacheWithUUUDNPatternIsNone(self):
        self.assertEqual(self.domain_checker.check_action('unknown', 'unknown', 'unknown', 'domestic', 'not blacklist'), 'Keep .gov and library, cut the rest')

    def test_createDomainSetFromListAddsDomainEntryToDomainEntriesList(self):
        self.dummy_domain_list = ['www.example.com', 'www.example.net']
        self.dummy_domain_entry_a = check_domains.DomainEntry('www.example.com')
        self.dummy_domain_entry_b = check_domains.DomainEntry('www.example.net')
        self.domain_checker.create_domain_set_from_list(self.dummy_domain_list)
        self.assertIsInstance(self.domain_checker.domain_entries[0], check_domains.DomainEntry)
        self.assertIsInstance(self.domain_checker.domain_entries[1], check_domains.DomainEntry)
        self.assertEqual(self.domain_checker.domain_entries[0].domain, 'www.example.com')
        self.assertEqual(self.domain_checker.domain_entries[1].domain, 'www.example.net')

    def test_createDomainSetFromListAddsDomainToDomainSet(self):
        self.dummy_domain_list = ['www.example.com', 'www.example.net']
        self.domain_checker.create_domain_set_from_list(self.dummy_domain_list)
        self.assertSetEqual(self.domain_checker.domain_set, {'www.example.com', 'www.example.net'})

    def test_processCSVLineAddsDomainEntryToDomainEntriesList(self):
        self.dummy_line = ['www.example.com', 1, 2, 3]
        self.dummy_domain_entry = check_domains.DomainEntry('www.example.com')
        self.domain_checker.process_csv_line(self.dummy_line)
        self.assertIsInstance(self.domain_checker.domain_entries[0], check_domains.DomainEntry)

    def test_processCSVLineAddsDomainToDomainSet(self):
        self.dummy_line = ['www.example.com', 1, 2, 3]
        self.domain_checker.process_csv_line(self.dummy_line)
        self.assertSetEqual(self.domain_checker.domain_set, {'www.example.com'})

    @patch('check_domains.DomainChecker.check_geo')
    @patch('check_domains.DomainChecker.get_ip_address')
    @patch('check_domains.DomainChecker.check_whois_xml_api')
    @patch('check_domains.DomainChecker.check_tld')
    def test_checkDomainOwnershipCallsCheckTLDCheckWHOISGetIPCheckGeo(self, mock_tld, mock_whois, mock_ip, mock_geo):
        self.domain_checker.check_domain_ownership(self.dummy_domain_entry)
        self.assertTrue(mock_tld.called)
        self.assertTrue(mock_whois.called)
        self.assertTrue(mock_ip.called)
        self.assertTrue(mock_geo.called)


if __name__ == '__main__':
    unittest.main(buffer=True)











