from __future__ import print_function
import mysql.connector


def set_string_to_unicode(s):
    """
    Sets an object to unicode if it is a non-unicode string, otherwise ignores it.

    Args:
        s : a text object to be potentially converted to a unicode object
    """

    encodings_to_try = ['utf-8', 'ascii', 'latin_1', 'iso8859_2']
    for encoding in encodings_to_try:
        try:
            if isinstance(s, basestring) and not isinstance(s, unicode):
                s = unicode(s, encoding)
        except UnicodeDecodeError:
            continue
    return s


def set_unicode_to_string(u, encoding='utf-8'):
    """
    Converts a unicode text object to a string object for writing/printing

    Args:
        u : a text unicode object to be converted to a string
        encoding (string) : optionally specifies the encoding to use for unicode object to string, defaults to utf-8
    """

    if isinstance(u, basestring) and not isinstance(u, str):
        u = u.encode(encoding)
    return u


class DBConnection(object):
    """
    A class to establish and maintain a connection to a MySQL database.

    Attributes:
        username (str) : the database username login
        password (str) : the database password for username
        host (str) : the database host location
        database (str) : the database first_name
        connection (MySQLConnection) : an established database connection
        cursor (MySQLConnection.cursor) : the current connection's cursor
    """

    def __init__(self, username, password, host, database):
        """
        Initializes attributes of DBConnection with arguments provided.

        Args:
            username (str) : the database username login
            password (str) : the database password for username
            host (str) : the database host location
            database (str) : the database first_name
        """

        self.username = username
        self.password = password
        self.host = host
        self.database = database
        self.connection = None
        self.cursor = None

    def connect(self):
        """
        Establishes a connection to a database using DBConnection object attributes

        Returns:
            None
        """

        try:
            self.connection = mysql.connector.connect(user=self.username, password=self.password, host=self.host,
                                                      database=self.database)
        except mysql.connector.Error as err:
            self.print_statement_error("Could not connect to database database.", err)
        return

    def close(self):
        """
        Closes the DBConnection object's MySQL connection

        Returns:
            None
        """

        try:
            self.connection.close()
        except mysql.connector.Error as err:
            self.print_statement_error("Could not close database.", err)
        return

    def set_cursor(self):
        """
        Sets the object's cursor as the connection's cursor.

        Returns:
            None
        """

        try:
            self.cursor = self.connection.cursor()
        except mysql.connector.Error as err:
            self.print_statement_error("Unable to get cursor to database.", err)

        return

    @staticmethod
    def print_statement_error(custom_message, mysql_connector_error, query_statement=None, query_args=None):
        """
        Prints a series of messages when returning error messages from MySQL.

        Args:
            custom_message (str) : Describes the particular error thrown in plain terms
            query_statement (str) : The MySQL statement to have been executed.
            mysql_connector_error (mysql.connector.Error) : An error object raised by mysql connector
            query_args (str) : a string representation of args to have been escape-substituted into statement

        Returns:
            None
        """

        print("MySQL Problem Encountered: {}".format(custom_message))
        print("Query Statement: {}".format(query_statement))
        print("Args: {}".format(query_args))
        print("Error Message: {}".format(mysql_connector_error))

        return

    def execute_select(self, statement, args=None):
        """
        Executes a MySQL SELECT statement in the connected database.

        Args:
            statement (str) : a mysql statement to be executed
            args (tuple) : a tuple of arguments to be escape-substituted into statement by cursor.execute()

        Returns:
            None
        """

        self.set_cursor()
        if args:
            string_args = tuple([set_unicode_to_string(arg) for arg in args])
            try:
                self.cursor.execute(statement, string_args)
            except mysql.connector.Error as err:
                args_string = ','.join([str(arg) for arg in string_args])
                self.print_statement_error("Select query unable to complete.", err, statement, args_string)
                self.cursor.close()
        else:
            try:
                self.cursor.execute(statement)
            except mysql.connector.Error as err:
                self.print_statement_error("Select query unable to complete.", err, statement)
                self.cursor.close()
        return

    def execute_update(self, statement, args=None):
        """
        Executes a MySQL INSERT, UPDATE or DELETE statement into the connected database

        Args:
            statement (str) : an INSERT, UPDATE, OR DELETE statement properly formatted for MySQL connector
            args (tuple) : a tuple of values to be substituted into the statement

        Returns:
            None
        """

        self.set_cursor()
        if args:
            string_args = tuple([set_unicode_to_string(arg) for arg in args])
            try:
                self.cursor.execute(statement, string_args)
                self.connection.commit()
            except mysql.connector.Error as err:
                args_string = ','.join([str(arg) for arg in string_args])
                self.print_statement_error("Update query unable to complete.", err, statement, args_string)
                self.cursor.close()
        else:
            try:
                self.cursor.execute(statement)
                self.connection.commit()
            except mysql.connector.Error as err:
                self.print_statement_error("Update query unable to complete.", err, statement)
                self.cursor.close()
        return

    def execute_update_many(self, statement, args_list):
        """
        Executes a MySQL INSERT, UPDATE or DELETE statement into the connected database for batch purposes

        Args:
            statement (str) : an INSERT, UPDATE, OR DELETE statement properly formatted for MySQL connector
            args_list (list) : a list of tuples to be substituted into the statement

        Returns:
            None
        """

        self.set_cursor()
        string_args_list = []
        for arg_tuple in args_list:
            string_arg_tuple = [set_unicode_to_string(arg) for arg in arg_tuple]
            string_args_list.append(string_arg_tuple)
        try:
            self.cursor.executemany(statement, string_args_list)
            self.connection.commit()
        except mysql.connector.Error as err:
            args_string_list = []
            for arg_tuple in string_args_list:
                args_string_list.append('(')
                for arg in arg_tuple:
                    args_string_list.append(str(arg))
                args_string_list.append('),')
            args_string = ''.join(args_string_list)
            self.print_statement_error("Update query unable to complete.", err, statement, args_string)
            self.cursor.close()
        return

    def call_procedure(self, statement, args=None):
        """
        Executes a MySQL stored procedure call.

        Args:
            statement (str) : the MySQL stored procedure to call
            args (tuple) : Optional. A tuple of args to be passed to the stored procedure

        Returns:
            None
        """

        self.set_cursor()

        if args:
            try:
                self.cursor.callproc(statement, args)
            except mysql.connector.Error as err:
                args_string = ','.join([set_unicode_to_string(arg) for arg in args])
                self.print_statement_error("Unable to read data from cursor.", err, statement, args_string)
                self.cursor.close()
        else:
            try:
                self.cursor.callproc(statement)
            except mysql.connector.Error as err:
                self.print_statement_error("Unable to read data from cursor.", err, statement)
                self.cursor.close()

        return

    def cursor_to_list_of_rows(self):
        """
        Returns a list of rows selected in a MySQL cursor.

        Returns:
            A list of lists if successful, else None
        """

        cursor_rows = []
        try:
            while True:
                current_rows = self.cursor.fetchmany(size=100)
                if not current_rows:
                    break
                for row in current_rows:
                    unicode_row = [unicode(col) for col in row]
                    cursor_rows.append(unicode_row)
            self.cursor.close()
            return cursor_rows
        except mysql.connector.Error as err:
            self.print_statement_error("Unable to read data from cursor.", err)
            return None

    def cursor_to_flat_list(self):
        """
        Returns a list of values selected in a single-column MySQL cursor

        Returns:
            A one-dimensional list if successful, else None
        """

        cursor_row = []
        try:
            while True:
                current_rows = self.cursor.fetchmany(size=100)
                if not current_rows:
                    break
                for row in current_rows:
                    cursor_row.append(unicode(row[0]))
            self.cursor.close()
            return cursor_row
        except mysql.connector.Error as err:
            self.print_statement_error("Unable to read data from cursor.", err)
            return None

    def cursor_to_dict(self):
        """
        Return a dict formed by values in the first two columns held in a MySQL cursor

        Returns:
            A dictionary if successful, else None
        """

        cursor_dict = {}
        try:
            while True:
                current_rows = self.cursor.fetchmany(size=100)
                if not current_rows:
                    break
                for row in current_rows:
                    key, value = unicode(row[0]), unicode(row[1])
                    cursor_dict[key] = value
            self.cursor.close()
            return cursor_dict
        except mysql.connector.Error as err:
            self.print_statement_error("Unable to read data from cursor.", err)
            return

    def cursor_to_dict_by_headers(self):
        """
        Returns a dict formed by the headers and resulting rows from a mysql cursor query

        Returns:
            A dict if successful, else None
        """

        data_dict = {}
        headers_dict = {}
        try:
            pos = 0
            for i in self.cursor.description:
                header = unicode(i[0])
                data_dict[header] = []
                headers_dict[pos] = header
                pos += 1
            while True:
                current_rows = self.cursor.fetchmany(size=100)
                if not current_rows:
                    break
                for row in current_rows:
                    pos = 0
                    for column in row:
                        header = headers_dict[pos]
                        data_dict[header].append(unicode(column))
                        pos += 1
            self.cursor.close()
            return data_dict
        except mysql.connector.Error as err:
            self.print_statement_error("Unable to read data from cursor.", err)
            return

    def cursor_to_dict_by_first_column(self):
        """
        Returns a dict formed by the first column values and headers as a nested dictionary

        Returns:
            A dict if successful, else None
        """

        data_dict = {}
        headers_dict = {}
        try:
            pos = 0
            for i in self.cursor.description:
                header = unicode(i[0])
                headers_dict[pos] = header
                pos += 1
            while True:
                current_rows = self.cursor.fetchmany(size=100)
                if not current_rows:
                    break
                for row in current_rows:
                    pos = 0
                    first_column = unicode(row[0])
                    data_dict[first_column] = {}
                    for column in row:
                        header = headers_dict[pos]
                        data_dict[first_column][header] = unicode(column)
                        pos += 1
            self.cursor.close()
            return data_dict
        except mysql.connector.Error as err:
            self.print_statement_error("Unable to read data from cursor.", err)
        except IndexError as err:
            self.print_statement_error("Method called on MySQL cursor with only one column.", err)
        return

    def cursor_to_set(self):
        """
        Returns a set formed by the values in the first column held in a MySQL cursor

        Returns:
            A set if successful, else None
        """

        cursor_set = set([])
        try:
            while True:
                current_rows = self.cursor.fetchmany(size=100)
                if not current_rows:
                    break
                for row in current_rows:
                    entry = unicode(row[0])
                    cursor_set.add(entry)
            self.cursor.close()
            return cursor_set
        except mysql.connector.Error as err:
            self.print_statement_error("Unable to read data from cursor.", err)
            return
