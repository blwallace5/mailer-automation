from __future__ import division
import time
import datetime
from backports.shutil_get_terminal_size import get_terminal_size
from math import ceil

domain_imap_servers = {u'ACO': u'imap.fastmail.com', u'ASO': u'imappro.zoho.com', u'CSO': u'imappro.zoho.com',
                       u'FS': u'imappro.zoho.com', u'GG': u'imap.fastmail.com', u'LGS': u'imappro.zoho.com',
                       u'LHTB': u'imappro.zoho.com', u'LP': u'imappro.zoho.com', u'MG': u'imappro.zoho.com'}

email_acct_creds = {u'ACO': {u'matt@affordablecollegecommunity.org': u's4esS#Jnkm',
                             u'megan@affordablecollegecommunity.org': u'3%zxxAhmjJ',
                             u'ryan@affordablecollegecommunity.org': u'vcdpwxjjhf3xmtan',
                             u'angela@affordablecollegesgroup.org': u'angelarocks',
                             u'angela@affordablecollegesonline.org': u'angelarocks',
                             u'dan@affordablecollegegroup.org': u'danrocks',
                             u'dan@affordablecollegesgroup.org': u'danrocks',
                             u'dominic@affordablecollegesgroup.org': u'dominicrocks',
                             u'doug@affordablecollegegroup.org': u'dougrocks',
                             u'doug@affordablecollegesgroup.org': u'dougrocks',
                             u'matt@affordablecollegesgroup.org': u'mattrocks',
                             u'megan@affordablecollegesgroup.org': u'nnk9Kg%Qgr',
                             u'meganfilbin@affordablecollegesgroup.org': u'meganrocks',
                             u'ryan@affordablecollegesgroup.org': u'a3alfgdsb5ncwea7',
                             u'shannon@affordablecollegesgroup.org': u'shannonrocks'},
                    u'ASO': {u'angela@accreditedschoolsonline.org': u'angelarocks',
                             u'dominic@accreditedschoolsonline.org': u'dominicrocks',
                             u'doug@accreditedschoolsonline.org': u'dougrocks',
                             u'ryan@accreditedschoolsonline.org': u'ryanrocks',
                             u'shannon@accreditedschoolsonline.org': u'shannonrocks',
                             u'angela@accreditedschoolscrew.org': u'angelarocks', u'angela@cfaos.org': u'angelarocks'},
                    u'CSO': {u'angela@computersciencegroup.org': u'angelarocks',
                             u'angela@computerscienceonline.org': u'angelarocks',
                             u'dan@computerscienceonline.org': u'danrocks',
                             u'dominic@computerscienceonline.org': u'dominicrocks',
                             u'matt@computersciencegroup.org': u'mattrocks',
                             u'ryan@computersciencegroup.org': u'ryanrocks',
                             u'ryan@computerscienceonline.org': u'ryanrocks',
                             u'teresa@computerscienceonline.org': u'teresarocks'},
                    u'FS': {u'angela@firescience.org': u'angelarocks', u'matt@firescience.org': u'mattrocks',
                            u'ryan@firescience.org': u'ryanrocks', u'teresa@firescience.org': u'teresarocks'},
                    u'GG': {u'angela@gograd.org': u'angelarocks', u'angela@gogradonline.org': u'angelarocks',
                            u'matt@gograd.org': u'mattrocks', u'matt@gogradonline.org': u'fxx6B%jpPu',
                            u'megan@gogradonline.org': u'm7jtn4ltyukrrt9e', u'ryan@gogradonline.org': u'ryanrocks'},
                    u'LGS': {u'laura@letsgosolar.com': u'laurarocks', u'matt@letsgosolar.com': u'mattrocks',
                             u'sara@letsgosolar.com': u'sararocks'},
                    u'LHTB': {u'angela@learnhowtobecome.org': u'angelarocks',
                              u'dominic@learnhowtobecome.org': u'dominicrocks',
                              u'kyle@learnhowtobecome.org': u'kylerocks',
                              u'matt@learnhowtobecome.org': u'mattrocks',
                              u'ryan@learnhowtobecome.org': u'ryanrocks',
                              u'dominic@lhtbcommunications.org': u'dominicrocks', u'kim@lhtbcareers.org': u'kimrocks',
                              u'kim@lhtbcommunications.org': u'kimrocks'},
                    u'LP': {u'dominic@learnpsychology.org': u'dominicrocks',
                            u'doug@learnpsychology.org': u'dougrocks',
                            u'kyle@learnpsychology.org': u'kylerocks',
                            u'matt@learnpsychology.org': u'mattrocks', u'kyle@learnpsych.org': u'kylerockslp'},
                    u'MG': {u'dominic@moneygeek.com': u'dominicrocks', u'gin@moneygeek.com': u'ginrocks',
                            u'gin@moneygeekonline.com': u'ginrocks', u'greg@moneygeek.com': u'gregrocks',
                            u'greg@moneygeekonline.com': u'gregrocks', u'heidi@moneygeek.com': u'heidirocks',
                            u'heidi@moneygeekonline.com': u'heidirocks', u'lydia@moneygeek.com': u'lydiarocks',
                            u'lydia@moneygeekonline.com': u'lydiarocks', u'mark@moneygeek.com': u'fPZNqVrNyuCdV2M9m7dU',
                            u'mark@moneygeekonline.com': u'mDuRPNzLk44wUxJMGLjM', u'matt@moneygeek.com': u'mattrocks',
                            u'polyana@moneygeek.com': u'polyanarocks', u'ryan@moneygeek.com': u'ryanrocks',
                            u'lydia@moneygeekteam.com': u'lydiarocks'}}

whitelisted_email_domains = {u'aol.com', u'aol.net', u'aol.org', u'att.com', u'att.net', u'att.org', u'bcps.com',
                             u'bcps.net', u'bcps.org', u'bellsouth.com', u'bellsouth.net', u'bellsouth.org',
                             u'biblio.com', u'biblio.net', u'biblio.org', u'cableone.com', u'civitasmedia.net',
                             u'civitasmedia.org', u'comcast.com', u'comcast.net', u'comcast.org', u'cox.com',
                             u'cox.net', u'cox.org', u'earthlink.com', u'earthlink.net', u'earthlink.org', u'esc17.com',
                             u'esc17.net', u'esc17.org', u'frontiernet.com', u'frontiernet.net', u'frontiernet.org',
                             u'gmail.com', u'gmail.net', u'gmail.org', u'googlemail.com', u'hcahealthcare.com',
                             u'hcahealthcare.net', u'hcahealthcare.org', u'hotmail.com', u'hotmail.net', u'hotmail.org',
                             u'icloud.com', u'iowatelecom.com', u'iowatelecom.net', u'iowatelecom.org', u'juno.com',
                             u'juno.net', u'juno.org', u'live.com', u'live.net', u'live.org', u'mac.com', u'mail.com',
                             u'mail.mil', u'mail.net', u'mail.org', u'me.com', u'me.net', u'me.org', u'metrocast.com',
                             u'metrocast.net', u'metrocast.org', u'mnps.com', u'mnps.net', u'mnps.org', u'msn.com',
                             u'msn.net', u'msn.org', u'nvbell.net', u'oplin.com', u'oplin.net', u'oplin.org',
                             u'optonline.com', u'optonline.net', u'optonline.org', u'outlook.com', u'outlook.net',
                             u'outlook.org', u'oz.net', u'pacbell.com', u'pacbell.net', u'pacbell.org',
                             u'pahousegop.com', u'pps.com', u'pps.net', u'pps.org', u'region16.net', u'rocketmail.com',
                             u'sbcglobal.com', u'sbcglobal.net', u'sbcglobal.org', u'schoolwires.com',
                             u'townsquare.com', u'townsquare.net', u'townsquare.org', u'us.af.mil', u'verizon.com',
                             u'verizon.net', u'verizon.org', u'windstream.com', u'windstream.net', u'windstream.org',
                             u'wlsmail.com', u'wlsmail.net', u'wlsmail.org', u'yahoo.ca', u'yahoo.com', u'yahoo.net',
                             u'yahoo.org', u'ymail.com'}


def set_string_to_unicode(s):
    """
    Sets an object to unicode if it is a non-unicode string, otherwise ignores it.

    Args:
        s : a text object to be potentially converted to a unicode object
    """

    encodings_to_try = ['utf-8', 'ascii', 'latin_1', 'iso8859_2']
    for encoding in encodings_to_try:
        try:
            if isinstance(s, basestring) and not isinstance(s, unicode):
                s = unicode(s, encoding)
        except UnicodeDecodeError:
            continue
    return s


def set_unicode_to_string(u, encoding='utf-8'):
    """
    Converts a unicode text object to a string object for writing/printing

    Args:
        u : a text unicode object to be converted to a string
        encoding (string) : optionally specifies the encoding to use for unicode object to string, defaults to utf-8
    """

    if isinstance(u, basestring) and not isinstance(u, str):
        u = u.encode(encoding)
    return u


def print_message(message):
    """
    Custom print statement adds timestamp to beginning of printed line.

    Args:
        message (basestring): the message to print out to user

    Returns:
        None
    """

    raw_time = time.time()
    timestamp = datetime.datetime.fromtimestamp(raw_time).strftime('%H:%M:%S')
    message = set_unicode_to_string(message)
    print('{} : {}'.format(timestamp, message))
    return


def selection_from_list(choices_list, list_name=None, indent=1, base_indent=4, sorted=True, spacer_px=5, separator=": ",
                        cols_disabled=False):
    '''Returns None if zero_selection is chosen by user'''

    choices_list = filter(None, choices_list)

    # constants
    indent_string = ' ' * (base_indent * indent)
    terminal_width, terminal_height = get_terminal_size()
    terminal_width -= len(indent_string)
    max_len_entry = max([len(choice) for choice in choices_list]) + spacer_px + len(separator)
    cols = int(terminal_width / max_len_entry)
    padding = len(str(len(choices_list)))
    if terminal_width % max_len_entry == 0:  # print looks wrong if we max the screen width, reduce one column
        cols -= 1
    if cols < 1:  # need at least one column
        cols = 1
    if cols_disabled:
        cols = 1
    rows = int(ceil(len(choices_list) / cols))

    if sorted:
        choices_list.sort()

    # create rows
    choices_dict = {key: choice for key, choice in enumerate(choices_list, start=1)}
    rows_dict = {i: [] for i in xrange(rows)}
    for k, v in choices_dict.iteritems():
        rows_dict[(k - 1) % rows].append((k, v))

    # print output
    if list_name:
        print("{}{}".format(indent_string, list_name.upper()))
    print"{}=====".format(indent_string)

    for row_num, row_data in rows_dict.iteritems():
        row = indent_string
        for k, v in row_data:
            row += "{}: {}".format(str(k).rjust(padding), str(v)).ljust(max_len_entry)
        print row

    print"{}=====".format(indent_string)

    # prompt selection
    while True:
        try:
            selected_int = int(
                raw_input("{}Selection: ".format(indent_string)))
            selection = choices_dict[selected_int]
            break
        except ValueError:
            print_message("Invalid selection.")
        except KeyError:
            print_message("Invalid selection.")

    print("{}Selected: {}".format(indent_string, selection))
    return selection


def verify_not_null(item):
    disallowed = [u'n/a', u'none', u' ']
    if item in disallowed:
        item = None
    return item


def create_alternate_urls_from_item(item):
    """
    Creates a list of possible variants of a URL

    Args:
        item (unicode): a url

    Returns:
        A list of variant urls, including the original URL
    """

    item = set_string_to_unicode(item)
    alternate_items = {item}
    if item[-1] == u'/':
        alt = item[:-1]
    else:
        alt = item + u'/'
    alternate_items.add(alt)
    new_alts = set()
    for alt_item in alternate_items:
        if alt_item[4] == u's':
            alt = alt_item[:4] + alt_item[5:]
        else:
            alt = alt_item[:4] + u's' + alt_item[4:]
        new_alts.add(alt)
    for new_alt in new_alts:
        alternate_items.add(new_alt)
    new_alts = set()
    for alt_item in alternate_items:
        if u'www.' in alt_item:
            alt = alt_item.replace(u'www.', u'')
        else:
            if alt_item[4] == u's':
                alt = alt_item[:8] + u'www.' + alt_item[8:]
            else:
                alt = alt_item[:7] + u'www.' + alt_item[7:]
        new_alts.add(alt)
    for new_alt in new_alts:
        alternate_items.add(new_alt)
    return list(alternate_items)


class ZohoFailureError(Exception):
    pass
