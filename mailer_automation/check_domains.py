from __future__ import print_function
import csv
import socket
import errno
import geoip2.webservice
import time
import datetime
import tldextract
import whoisxml_connection
import os
from mysql_database_connection import DBConnection
from multiprocessing.dummy import Pool as ThreadPool


class DomainEntry(object):
    """
    A class to represent a domain and its ownership information.

    Attributes:
        domain (str) : the domain (e.g., google.com)
        ip_address (str) : the ip address of domain
        whois_entry (WHOISEntry) : a WHOISEntry object containing WHOIS data for the domain
        ip_geo_entry (IPGeoEntry) : a IPGeoEntry object containing geographic data for the domain
            (based on IP address)
        tld_entry (TLDEntry) : a TLDEntry object containing TLD extract data for the domain

    """

    def __init__(self, domain):
        """
        Initializes a DomainEntry object.

        All attributes except for domain are initialized as None. Domain is required upon initialization.

        Args:
            domain (str) : the domain (e.g., google.com)

        """

        self.domain = domain
        self.ip_address = None
        self.whois_entry = None
        self.ip_geo_entry = None
        self.tld_entry = None


class TLDEntry(object):
    """
    A class to represent the TLD information related to a domain.

    Attributes:
        domain (str) : the domain (e.g., google.com)
        tld (str) : the top-level domain of domain
        tld_type (str) : whether the tld is blacklisted or not blacklisted from usage in mailers

    """

    def __init__(self, domain):
        """
        Initializes a TLDEntry object.

        All attributes except for domain are initialized as None. Domain is required upon initialization.

        Args:
            domain (str) : the domain (e.g., google.com)

        """

        self.domain = domain
        self.tld = None
        self.tld_type = None

    def enter_tld_results(self, tld, tld_type):
        """
        A method to update TLDEntry's attributes.

        Args:
            tld (str) : the tld of TLDEntry's domain
            tld_type (str) : the tld_type of TLDEntry's domain

        Returns:
            None

        """

        self.tld = tld
        self.tld_type = tld_type
        return

    def get_tld_type(self):
        """
        A method to return a TLDEntry's tld_type.

        Returns:
            The TLDEntry's tld_type attribute

        """

        return self.tld_type


class WHOISEntry(object):
    """
    A class to represent WHOIS registration data for a domain

    Attributes:
        domain (str) : the domain (e.g., google.com)
        country (str) : the country which the registrant claims as their physical location
        country_locale (str) : the location status (Domestic/International/Unknown) of country
        admin_country (str) : the country which the domain admin claims as their physical location
        admin_locale (str) : the location status (Domestic/International/Unknown) of admin_country
        tech_country (str) : the country which domain technical contact claims as their physical locatio
        tech_locale (str) : the location status (Domestic/International/Unknown) of tech_country

    """

    def __init__(self, domain):
        """
        Initializes a WHOISEntry object.

        All attributes except for domain are initialized as None. Domain is required upon initialization.

        Args:
            domain (str) : the domain (e.g., google.com)

        """

        self.domain = domain
        self.country = None
        self.tech_country = None
        self.admin_country = None
        self.country_locale = None
        self.tech_locale = None
        self.admin_locale = None

    def enter_whois_xml_results(self, xml_handler, locale_dict):
        """
        A method to update WHOISEntry attributes.

        Args:
            xml_handler (whoisxml_connection.WhoisXMLHandler) : An object containing WHOIS XML API data
            locale_dict (dict) : a dict of the form {locale : status}

        Returns:
            None

        """

        try:
            self.country = xml_handler.get_country('registrant')
        except:
            self.country = ''
        try:
            self.country_locale = locale_dict[self.country.lower()]
        except:
            self.country_locale = 'unknown'
        try:
            self.admin_country = xml_handler.get_country('administrativeContact')
        except:
            self.admin_country = ''
        try:
            self.admin_locale = locale_dict[self.admin_country.lower()]
        except:
            self.admin_locale = 'unknown'
        try:
            self.tech_country = xml_handler.get_country('technicalContact')
        except:
            self.tech_country = ''
        try:
            self.tech_locale = locale_dict[self.tech_country.lower()]
        except:
            self.tech_locale = 'unknown'
        return

    def get_country_names(self):
        """
        Returns the country name attributes.

        Returns:
            A three-tuple containing the WHOISEntry's country, admin_country, and tech_country attributes

        """

        return tuple([self.country, self.admin_country, self.tech_country])

    def get_locale_names(self):
        """
        Returns the locale attributes.

        Returns:
            A three-tuple containing the WHOISEntry's country_locale, admin_locale, tech_locale attributes

        """

        return tuple([self.country_locale, self.admin_locale, self.tech_locale])


class IPGeoEntry(object):
    """
    A class to represent Geolocation (based on IP) data for a domain

    Attributes:
        ip_address (str) : an ip address (e.g. '74.125.239.134')
        country_name (str) : the name of the country where ip_address is hosted
        locale (str) : the location status (Domestic/International/Unknown) of domain, based on geolocation info

    """

    def __init__(self, ip_address):
        """
        Initializes the IPGeoEntry object.

        Args:
            ip_address (str) : an ip address (e.g., '74.125.239.134')

        """

        self.ip_address = ip_address
        self.country_name = None
        self.locale = None

    def enter_geo_results(self, ip_geo_result, locale_dict):
        """
        Updates the IPGeoEntry's attributes.

        Args:
            ip_geo_result (geoip2.webservice Response object) : a response object from Maxmind's geoip2 service
            locale_dict (dict) : a dict of the form {locale : status}

        Returns:
            None

        """
        try:
            self.country_name = ip_geo_result.country.name
        except:
            self.country_name = 'unknown'
        try:
            self.locale = locale_dict[self.country_name.lower()]
        except:
            self.locale = 'unknown'
        return

    def get_country_name(self):
        """
        Returns the IPGeoEntry's country_name attribute.

        Returns:
            The object's country_name attribute
        """

        return self.country_name

    def get_locale_name(self):
        """
        Returns the locale attributes.

        Returns:
            The object's locale attribute

        """

        return self.locale


class CSVToDictCompiler(object):
    """
    A class to create a dictionary of data from a two-column CSV.

    The class's primary function is to read a two-column CSV of the form [key, value]
    and output that information to a dictionary.

    Attributes:
        file_location (str) : the file location of the input CSV
        csv_dict (dict) : a dict of the form {key : value}

    """

    def __init__(self, file_location):
        """
        Initializes the CSVToDictCompiler object.

        Args:
            file_location (str) : the location of the file used for dictionary creation

        """

        self.file_location = file_location
        self.csv_dict = {}

    def compile_csv_dict(self):
        """
        Builds a dictionary of the form {key: value} from a two_column CSV of the form [key, value]

        Returns:
            None

        """

        with open(self.file_location) as csvfile:
            reader = csv.reader(csvfile)
            header_passed = False
            for line in reader:
                if not header_passed:
                    header_passed = True
                    continue
                key, value = line
                self.csv_dict[key.lower()] = value.lower()
        return

    def get_csv_dict(self):
        """
        Returns the CSVToDictCompiler's csv_dict attribute

        Returns:
            The object's csv_dict attribute

        """

        return self.csv_dict


class DomainChecker(object):
    """
    A class that checks the location and registration information for domains.

    This class manages the creation of DomainEntry, TLDEntry, IPGeoEntry, and WHOISEntry objects.

    Attributes:
        domain_entries (list) : a list of DomainEntry objects
        domain_set (set) : the set of all unique domains seen during program execution
        tld_dict (dict) : a dict of the form {tld: tld_type}
        location_dict (dict) : a dict of the form {locale : status}
        domain_check_results (list) : a list of the results from the domain check process
        domain_check_results_dict (dict) : a dict in the form {domain : [results]} built from domain_check_results

    """

    def __init__(self, top_level_domain_dict, locale_dict):
        """
        Initializes the DomainChecker class.

        Args:
            top_level_domain_dict (dict) : a dict of the form {tld: tld_type}
            locale_dict (dict) : a dict of the form {locale : status}

        """

        self.domain_entries = []
        self.domain_set = set([])
        self.whitelist_domain_set = set([])
        self.blacklist_domain_set = set([])
        self.tld_dict = top_level_domain_dict
        self.location_dict = locale_dict
        self.domain_check_results = []
        self.domain_check_results_dict = {}
        self.domains_to_cache = []
        self.domains_to_blacklist = []

    def domain_list_from_csv(self, file_location):
        """
        Generates a list of domains from a single-column CSV

        Args:
            file_location (str) : the file location of the CSV containing domains to read in

        Returns:
            None

        """

        with open(file_location, 'rb') as csvfile:
            reader = csv.reader(csvfile)
            header_passed = False
            for line in reader:
                if not header_passed:
                    header_passed = True
                    continue
                self.process_csv_line(line)
        return

    def process_domain_check_results(self):
        """
        Checks all domain entry results and enters them into property list

        Returns:
            None
        """
        for domain_entry in self.domain_entries:
            domain = domain_entry.domain
            try:
                registrar_country_tuple = domain_entry.whois_entry.get_country_names()
                registrar_country, admin_country, tech_country = registrar_country_tuple
                registrar_locale_tuple = domain_entry.whois_entry.get_locale_names()
                registrar_locale, admin_locale, tech_locale = registrar_locale_tuple
            except:
                print("WHOIS information not found for {}.".format(domain))
                registrar_country, admin_country, tech_country = "", "", ""
                registrar_locale, admin_locale, tech_locale = "unknown", "unknown", "unknown"
            try:
                ip_country = domain_entry.ip_geo_entry.get_country_names()
                ip_locale = domain_entry.ip_geo_entry.get_locale_names()
            except:
                print("IP-Geo information not found for {}.".format(domain))
                ip_country = "unknown"
                ip_locale = "unknown"
            try:
                tld_type = domain_entry.tld_entry.get_tld_type()
            except:
                print("TLD information not found for {}.".format(domain))
                tld_type = "unknown"
            action = self.check_action(registrar_locale, admin_locale, tech_locale, ip_locale, tld_type)
            cache = self.check_cache(registrar_locale, admin_locale, tech_locale, ip_locale, tld_type)
            if action == 'Keep' and cache == '2wks':
                self.domains_to_cache.append(domain)
            elif action == 'Cut' and cache == 'Blacklist':
                self.domains_to_blacklist.append(domain)
            new_line = [domain, registrar_country, admin_country, tech_country, ip_country, registrar_locale,
                        admin_locale, tech_locale, ip_locale, tld_type, action, cache]
            self.domain_check_results.append(new_line)
        return

    def add_whitelist_domains_to_results(self):
        """
        Adds whitelisted domains to the results of the domain check process

        Returns:
            None
        """

        for domain in self.whitelist_domain_set:
            action = 'Keep'
            cache = 'Already Cached'
            registrar_country = 'Whitelist Cache'
            admin_country = 'Whitelist Cache'
            tech_country = 'Whitelist Cache'
            ip_country = 'Whitelist Cache'
            registrar_locale = 'Whitelist Cache'
            admin_locale = 'Whitelist Cache'
            tech_locale = 'Whitelist Cache'
            ip_locale = 'Whitelist Cache'
            tld_type = 'Whitelist Cache'
            new_line = [domain, registrar_country, admin_country, tech_country, ip_country, registrar_locale,
                        admin_locale, tech_locale, ip_locale, tld_type, action, cache]
            self.domain_check_results.append(new_line)
        return

    def add_blacklist_domains_to_results(self):
        """
        Adds whitelisted domains to the results of the domain check process

        Returns:
            None
        """

        for domain in self.blacklist_domain_set:
            action = 'Cut'
            cache = 'Already Blacklisted'
            registrar_country = 'Blacklisted'
            admin_country = 'Blacklisted'
            tech_country = 'Blacklisted'
            ip_country = 'Blacklisted'
            registrar_locale = 'Blacklisted'
            admin_locale = 'Blacklisted'
            tech_locale = 'Blacklisted'
            ip_locale = 'Blacklisted'
            tld_type = 'Blacklisted'
            new_line = [domain, registrar_country, admin_country, tech_country, ip_country, registrar_locale,
                        admin_locale, tech_locale, ip_locale, tld_type, action, cache]
            self.domain_check_results.append(new_line)
        return

    def get_domain_check_results_dict(self):
        """
        Returns the domain_check_results_dict attribute

        Returns:
            A dict of the form {domain : [results]} containing the domains geocheck processes
        """

        return self.domain_check_results_dict

    def domain_check_results_to_dict(self):
        """
        Checks all domain entry results and enters them into property list

        Returns:
            None

        """

        for line in self.domain_check_results:
            domain = line[0]
            results = line[1:]
            self.domain_check_results_dict[domain] = results
        return

    def get_domains_to_cache(self):
        """
        Returns the domains_to_cache attribute

        Returns:
            A list containing domains to be added to the geo-checking cache

        """

        return self.domains_to_cache

    def get_domains_to_blacklist(self):
        """
        Returns the domains_to_blacklist attribute

        Returns:
            A list containing domains to be added to the geo-checking cache

        """

        return self.domains_to_blacklist

    def write_results(self, file_location):
        """
        Writes location and WHOIS data to an output CSV file.

        Args:
            file_location (str) : the file_location of the CSV where data will be written out to

        Returns:
            None

        """

        with open(file_location, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            header = ['Domain', 'Registrar Country', 'Admin Country', 'Tech Country', 'IP Address Country',
                      'Registrar Locale', 'Admin Locale', 'Tech Locale', 'IP Geo Locale', 'TLD Type', 'Action', 'Cache']
            writer.writerow(header)
            print("Writing information to output.")
            for domain_entry in self.domain_entries:
                domain = domain_entry.domain
                try:
                    registrar_country_tuple = domain_entry.whois_entry.get_country_names()
                    registrar_country, admin_country, tech_country = registrar_country_tuple
                    registrar_locale_tuple = domain_entry.whois_entry.get_locale_names()
                    registrar_locale, admin_locale, tech_locale = registrar_locale_tuple
                except:
                    print("WHOIS information not found for {}.".format(domain))
                    registrar_country, admin_country, tech_country = "", "", ""
                    registrar_locale, admin_locale, tech_locale = "unknown", "unknown", "unknown"
                try:
                    ip_country = domain_entry.ip_geo_entry.get_country_names()
                    ip_locale = domain_entry.ip_geo_entry.get_locale_names()
                except:
                    print("IP-Geo information not found for {}.".format(domain))
                    ip_country = "unknown"
                    ip_locale = "unknown"
                try:
                    tld_type = domain_entry.tld_entry.get_tld_type()
                except:
                    print("TLD information not found for {}.".format(domain))
                    tld_type = ""
                action = self.check_action(registrar_locale, admin_locale, tech_locale, ip_locale, tld_type)
                cache = self.check_cache(registrar_locale, admin_locale, tech_locale, ip_locale, tld_type)
                new_line = [domain, registrar_country, admin_country, tech_country, ip_country, registrar_locale,
                            admin_locale, tech_locale, ip_locale, tld_type, action, cache]
                writer.writerow(new_line)
        return

    @staticmethod
    def check_action(registrar_locale, admin_locale, tech_locale, ip_locale, tld_type):
        """
        Determines the mailer action to take based on locale data

        Args:
            registrar_locale (str) : the location status (Domestic/International/Unknown) of a registration country
            admin_locale (str) : the location status (Domestic/International/Unknown) of an admin country
            tech_locale (str) : the location status (Domestic/International/Unknown) of a technical contact country
            ip_locale (str) : the location status (Domestic/International/Unknown) of a geolocation country
            tld_type (str) : the tld status (Blacklist/Not Blacklist) of a tld

        Returns:
            The appropriate action to take for the combination of locales

        """

        if tld_type == 'blacklist' or  registrar_locale == 'international' or admin_locale == 'international' or \
                tech_locale == 'international' or ip_locale == 'international':
            action = 'Cut'
        elif registrar_locale == admin_locale == tech_locale == ip_locale == 'unknown':
            action = 'Unknown Locales! Update Locale List'
        elif ip_locale == 'unknown':
            action = 'Cut'
        elif registrar_locale == admin_locale == tech_locale == 'unknown':
            action = 'Keep .gov and library, cut the rest'
        else:
            action = 'Keep'
        return action

    @staticmethod
    def check_cache(registrar_locale, admin_locale, tech_locale, ip_locale, tld_type):
        """
        Determines the mailer cache status based on locale data

        Args:
            registrar_locale (str) : the location status (Domestic/International/Unknown) of a registration country
            admin_locale (str) : the location status (Domestic/International/Unknown) of an admin country
            tech_locale (str) : the location status (Domestic/International/Unknown) of a technical contact country
            ip_locale (str) : the location status (Domestic/International/Unknown) of a geolocation country
            tld_type (str) : the tld status (Blacklist/Not Blacklist) of a tld

        Returns:
            The appropriate cache status for the combination of locales

        """

        if tld_type == 'blacklist' or  registrar_locale == 'international' or admin_locale == 'international' or \
                tech_locale == 'international' or ip_locale == 'international':
            cache = 'Blacklist'
        elif registrar_locale == admin_locale == tech_locale == ip_locale == 'unknown':
            cache = 'Unknown Locales! Update Locale List'
        elif tld_type == 'not blacklist' and not (registrar_locale == admin_locale == tech_locale == 'unknown'):
            cache = '2wks'
        else:
            cache = 'None'
        return cache

    def create_domain_set_from_list(self, domain_list, whitelist_domains=set([]), blacklist_domains=set([])):
        """
        Populates the object's domain set from a list of domains

        Args:
            domain_list (list) : a list of domains to be checked
            whitelist_domains (set) : Optional. a set of domains which have been already checked and have positive results
            blacklist_domains (set) : Optional. a set of domains which have been already checked and have negative results

        Returns:
            None
        """

        for domain in domain_list:
            if domain not in self.domain_set:
                if domain in whitelist_domains:
                    self.whitelist_domain_set.add(domain)
                if domain in blacklist_domains:
                    self.blacklist_domain_set.add(domain)
                else:
                    domain_entry = DomainEntry(domain)
                    self.domain_entries.append(domain_entry)
                    self.domain_set.add(domain)
        return

    def process_csv_line(self, line):
        """
        Creates a DomainEntry object from a CSV line.

        Args:
            line (list) : a list of N > 0 items, where list[0] is a valid domain

        Returns:
            None

        """

        domain = line[0]
        if domain not in self.domain_set:
            domain_entry = DomainEntry(domain)
            self.domain_entries.append(domain_entry)
            self.domain_set.add(domain)
        return

    def check_ownership_info(self):
        """
        A multi-threading handler for checking ownership information for DomainEntries

        Returns:
            None

        """

        pool = ThreadPool(8)
        pool.map(self.check_domain_ownership, self.domain_entries)
        return

    def check_domain_ownership(self, domain_entry):
        """
        A control process to manage the checking of domain ownership information for a single DomainEntry

        Args:
            domain_entry (DomainEntry) : the DomainEntry object to have ownership info checked

        Returns:
            None

        """

        self.check_tld(domain_entry)
        self.check_whois_xml_api(domain_entry)
        self.get_ip_address(domain_entry)
        self.check_geo(domain_entry)
        return

    def check_whois_xml_api(self, domain_entry):
        """
        Checks the whois registration status of a DomainEntry.

        Creates a WhoisXMLHandler from whoisxml_connection.py and executes its run() method.

        Args:
            domain_entry (DomainEntry) : the DomainEntry object to have WHOIS XML data checked

        Returns:
            None
        """

        domain = domain_entry.domain
        try:
            xml_handler = whoisxml_connection.WhoisXMLHandler('doug@abuvmedia.com', 'AbuvMedia8')
            xml_handler.run(domain)
            whois_entry = WHOISEntry(domain)
            whois_entry.enter_whois_xml_results(xml_handler, self.location_dict)
            domain_entry.whois_entry = whois_entry
        except Exception as e:
            print("WHOIS lookup error: {}, {}".format(domain, e.args))
        return

    @staticmethod
    def get_ip_address(domain_entry):
        """
        Uses the socket built-in module to get the IP address for a domain

        Args:
            domain_entry (DomainEntry) : the DomainEntry object to have the IP address determined for

        Returns:
            None

        """

        domain = domain_entry.domain
        try:
            ip_address = socket.gethostbyname(domain)
            domain_entry.ip_address = ip_address
        except socket.gaierror:
            domain_entry.ip_address = None
        except socket.error as error:
            if error.errno == errno.WSAECONNRESET:
                try:
                    ip_address = socket.gethostbyname(domain)
                    domain_entry.ip_address = ip_address
                except:
                    domain_entry.ip_address = None
            else:
                domain_entry.ip_address = None
        except:
            domain_entry.ip_address = None
        return

    def check_tld(self, domain_entry):
        """
        Extracts the top-level domain from the domain in a DomainEntry object and checks its tld_type

        Args:
            domain_entry (DomainEntry) : the DomainEntry object to have the IP address determined for

        Returns:
            None
        """

        domain = domain_entry.domain
        tld_entry = TLDEntry(domain)
        try:
            domain_parse = tldextract.extract(domain)
            tld = domain_parse.suffix
        except:
            tld = ''
        tld_entry.tld = tld
        if tld in self.tld_dict:
            tld_type = self.tld_dict[tld]
        else:
            tld_type = ''
        tld_entry.tld_type = tld_type
        domain_entry.tld_entry = tld_entry
        return

    def check_geo(self, domain_entry):
        """
        Queries the Maxmind geoip2 webservice to retrieve the geolocation info for a DomainEntry object

        Args:
            domain_entry (DomainEntry) : the DomainEntry object to have the geolocation information looked up

        Returns:
            None
        """

        ip_address = str(domain_entry.ip_address)
        ip_geo_entry = IPGeoEntry(ip_address)
        domain_entry.ip_geo_entry = ip_geo_entry
        if ip_address == 'None':
            print("No IP Address!")
            ip_geo_entry.enter_geo_results(None, self.location_dict)
            return
        client = geoip2.webservice.Client(107734, 'UN0ERXzi0Prb')
        try:
            ip_geo_result = client.country(ip_address)
            ip_geo_entry.enter_geo_results(ip_geo_result, self.location_dict)
        except geoip2.webservice.AddressNotFoundError:
            print("Address {} not found! Maxmind".format(ip_address))
            ip_geo_entry.enter_geo_results(None, self.location_dict)
            return
        except geoip2.webservice.InvalidRequestError:
            print("Invalid request! Maxmind")
            ip_geo_entry.enter_geo_results(None, self.location_dict)
            return
        except geoip2.webservice.OutOfQueriesError:
            print("Out of queries! Maxmind")
            ip_geo_entry.enter_geo_results(None, self.location_dict)
            return
        except geoip2.webservice.AuthenticationError:
            print("Authentication error! Maxmind")
            ip_geo_entry.enter_geo_results(None, self.location_dict)
            return
        except geoip2.webservice.HTTPError:
            print("HTTP Error! Maxmind")
            ip_geo_entry.enter_geo_results(None, self.location_dict)
            return
        except geoip2.webservice.GeoIP2Error:
            print("GeoIP2Error! Maxmind")
            ip_geo_entry.enter_geo_results(None, self.location_dict)
            return
        # except:
        #    print("Other Error! Maxmind")
        #    ip_geo_entry.enter_geo_results(None, self.location_dict)
        #    return
        return


class CheckHandler(object):
    """
    A class to manage the running of the domain check objects and processes in tandem with the VA application export

    Attributes:
        tld_compiler (CSVToDictCompiler) : generates a list of all available TLDs and their categorization for check
        tld_dict (dict) : a dictionary of all available TLDs and the appropriate action to take for them
        location_compiler (CSVToDictCompiler) : generates a list of all available locales and their categorization
        location_dict (dict) : a dictionary of all known locales and the appropriate action to take for them
        domain_checker (DomainChecker) : an object to check all domains in a domains_list
        va_output_lines (list) : a list of lines from the va application
        domain_list (list) : a list of source domains extracted from the va output lines
        domain_check_results_dict (dict) : a dict of results following the functions of domain_checker
        geochecked_lines (list) : a list of lists containing the va application lines updated with geocheck data
        inventory_cxn (DBConnection) : a connection to a database

    """

    def __init__(self):
        """
        Initializes the CheckHandler object.

        """

        self.tld_compiler = None
        self.tld_dict = {}
        self.location_compiler = None
        self.location_dict = {}
        self.domain_checker = None
        self.va_output_lines = []
        self.domain_list = []
        self.domain_check_results_dict = {}
        self.geochecked_lines = []
        self.inventory_cxn = None
        self.whitelist_domains_cache = set([])
        self.blacklist_domains_cache = set([])

    def set_db_connection(self):
        """
        Opens the CheckHandler's db connection for the domain cache

        Returns:
            None

        """

        self.inventory_cxn = DBConnection('root', 'vagrant', '127.0.0.1', 'contact_inventory')
        self.inventory_cxn.connect()
        return

    def build_geo_domains_blacklist_cache(self):
        """
        Retrieves the geo domains blacklist cache from the inventory database and sets to attribute

        Returns:
            None
        """

        self.inventory_cxn.execute_select("SELECT domain FROM geo_domains_blacklist_cache")
        self.blacklist_domains_cache = self.inventory_cxn.cursor_to_set()
        return

    def build_geo_domains_whitelist_cache(self):
        """
        Retrieves the geo domains whitelist cache from the inventory database and sets to attribute

        Returns:
            None
        """

        self.inventory_cxn.execute_select("SELECT domain FROM geo_domains_whitelist_cache")
        self.whitelist_domains_cache = self.inventory_cxn.cursor_to_set()
        return

    def update_geo_domains_whitelist_cache(self):
        """
        Updates the geo domains whitelist cache in the inventory database by removing old entries

        Returns:
            None
        """

        self.inventory_cxn.execute_update("DELETE from geo_domains_whitelist_cache \
                                           WHERE datediff(insert_date, curdate()) < -14;")
        return

    def insert_new_geo_domains_cache(self):
        """
        Updates the geo domains whitelist cache in the inventory database by adding newly queried items

        Returns:
            None

        """

        new_whitelist_domains = self.domain_checker.get_domains_to_cache()
        new_blacklist_domains = self.domain_checker.get_domains_to_blacklist()
        statement = "INSERT INTO geo_domains_whitelist_cache VALUES(%s, curdate())"
        for domain in new_whitelist_domains:
            values = (domain,)
            self.inventory_cxn.execute_update(statement, values)
        statement = "INSERT INTO geo_domains_blacklist_cache VALUES(%s, curdate())"
        for domain in new_blacklist_domains:
            values = (domain,)
            self.inventory_cxn.execute_update(statement, values)
        return

    def build_domain_list(self):
        """
        Extracts the source domains from the va_output_lines

        Returns:
            None
        """

        for line in self.va_output_lines:
            domain = line[6].strip().lower()
            self.domain_list.append(domain)
        return

    def run_domains_check_from_va_application_output(self, va_output_lines):
        """
        Sets up all of the necessary lists to check a domain against and runs the domain check functions

        Args:
            va_output_lines (list) : a list of domains to be checked

        Returns:
            None

        """

        self.tld_compiler = CSVToDictCompiler(os.path.join(os.path.dirname(__file__), 'resources/tld_list.csv'))
        self.tld_compiler.compile_csv_dict()
        self.tld_dict = self.tld_compiler.get_csv_dict()
        self.location_compiler = CSVToDictCompiler(os.path.join(os.path.dirname(__file__), 'resources/locale_list.csv'))
        self.location_compiler.compile_csv_dict()
        self.location_dict = self.location_compiler.get_csv_dict()
        self.domain_checker = DomainChecker(self.tld_dict, self.location_dict)
        self.va_output_lines = va_output_lines
        self.set_db_connection()
        self.update_geo_domains_whitelist_cache()
        self.build_geo_domains_whitelist_cache()
        self.build_geo_domains_blacklist_cache()
        self.build_domain_list()
        self.domain_checker.create_domain_set_from_list(self.domain_list,
                                                        self.whitelist_domains_cache,
                                                        self.blacklist_domains_cache)
        self.domain_checker.check_ownership_info()
        self.domain_checker.process_domain_check_results()
        self.domain_checker.add_whitelist_domains_to_results()
        self.domain_checker.add_blacklist_domains_to_results()
        self.domain_checker.domain_check_results_to_dict()
        self.domain_check_results_dict = self.domain_checker.get_domain_check_results_dict()
        self.update_va_results()
        self.insert_new_geo_domains_cache()
        return

    def update_va_results(self):
        """
        Adds the domain check results dict's information to the va output

        Returns:
            None
        """

        for line in self.va_output_lines:
            domain = line[6]
            try:
                geocheck_results = self.domain_check_results_dict[domain.strip().lower()]
            except:
                print("Something went wrong! {} | {}".format(domain, line))
                geocheck_results = []
            new_line = line + geocheck_results
            self.geochecked_lines.append(new_line)
        return

    def get_domain_check_results(self):
        """
        Returns the domain_check_results list

        Returns:
            A list containing the va application output updated with data from domains checks

        """

        return self.geochecked_lines

if __name__ == '__main__':

    start = time.time()
    in_file_location = ''
    out_file_location = ''

    tld_compiler = CSVToDictCompiler('tld_list.csv')
    tld_compiler.compile_csv_dict()
    tld_dict = tld_compiler.get_csv_dict()

    location_compiler = CSVToDictCompiler('locale_list.csv')
    location_compiler.compile_csv_dict()
    location_dict = location_compiler.get_csv_dict()

    domain_checker = DomainChecker(tld_dict, location_dict)
    domain_checker.domain_list_from_csv('infile.csv')

    try:
        domain_checker.check_ownership_info()
    except:
        print("Unable to complete domain checkup! PLEASE CHECK OUTPUT FILE FOR PARTIAL COMPLETION!")
    ts = time.time()
    timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y_%m_%d_%H_%M_%S')
    domain_checker.write_results('outfile_{}.csv'.format(timestamp))
    print("Completed in {} seconds.".format(time.time() - start))
