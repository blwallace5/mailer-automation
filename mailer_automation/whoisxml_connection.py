import urllib2
import xml.etree.ElementTree as etree


class WhoisXMLHandler(object):

    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.format = 'XML'
        self.domain = None
        self.endpoint = None
        self.result_xml = None
        self.result_dict = None

    def run(self, domain):
        self.load(domain)
        self.query()
        self.build_result_dict(self.result_xml)

    def load(self, domain):
        self.domain = domain
        self.endpoint = 'http://www.whoisxmlapi.com/whoisserver/WhoisService?domainName=' + self.domain + '&username=' + self.username + '&password=' + self.password + '&outputFormat=' + self.format

    def query(self):
        try:
            self.result_xml = etree.parse(urllib2.urlopen(self.endpoint)).getroot()
        except urllib2.HTTPError, e:
            print "WHOIS Query Returned Error: {}, {}".format(e.code, e.msg)
            self.result_xml = None

    def recursivePrettyPrint(self, obj, indent):
        for x in list(obj):
            if isinstance(obj[x], dict):
                print (' '*indent + str(x)[0:50] + ": ")
                self.recursivePrettyPrint(obj[x], indent + 5)
            elif isinstance(obj[x], list):
                print(' '*indent + str(x)[0:50] + ": " + str(list(obj[x])))
            else:
                print (' '*indent + str(x)[0:50] + ": " + str(obj[x])[0:50].replace("\n",""))

    def build_result_dict(self, xml_root):
        try:
            self.result_dict = {xml_root.tag : self.xml_tree_to_dict(xml_root)}
        except:
            print "WHOIS Query Failed. No result set."
            self.result_dict = {}

    def xml_tree_to_dict(self, top_node):
        if len(list(top_node)) == 0:
            xml_dict = top_node.text
        else:
            xml_dict = {}
            for node in list(top_node):
                xml_dict[node.tag] = self.xml_tree_to_dict(node)
                if isinstance(xml_dict[node.tag], dict):
                    xml_dict[node.tag] = xml_dict[node.tag]
        return xml_dict

    def get_country_generic(self, contact_type, contact_string):

        has_whoisrecord = True if 'WhoisRecord' in self.result_dict else False
        has_contact_type = True if has_whoisrecord and contact_type in self.result_dict['WhoisRecord'] else False
        has_country = True if has_contact_type and 'country' in self.result_dict['WhoisRecord'][contact_type] else False

        if has_whoisrecord and has_contact_type and has_country:
            return self.result_dict['WhoisRecord'][contact_type]['country']

        elif not has_whoisrecord:
            return 'No WHOIS Record Found'

        elif not has_contact_type:
            return 'No WHOIS {} Contact Found'.format(contact_string)

        elif not has_country:
            return 'No WHOIS {} Contact Country Found'.format(contact_string)


    def get_country_registrydata(self, contact_type, contact_string):

        has_whoisrecord = True if 'WhoisRecord' in self.result_dict else False
        has_registrydata = True if has_whoisrecord and 'registryData' in self.result_dict['WhoisRecord'] else False
        has_contact_type = True if has_registrydata and contact_type in self.result_dict['WhoisRecord']['registryData'] else False
        has_country = True if has_contact_type and 'country' in self.result_dict['WhoisRecord']['registryData'][contact_type] else False

        if has_whoisrecord and has_registrydata and has_contact_type and has_country:
            return self.result_dict['WhoisRecord']['registryData'][contact_type]['country']

        elif not has_whoisrecord:
            return 'No WHOIS Record Found'

        elif not has_contact_type:
            return 'No WHOIS {} Contact Found'.format(contact_string)

        elif not has_country:
            return 'No WHOIS {} Contact Country Found'.format(contact_string)


    def get_country(self, contact_type):
        if contact_type == 'registrant':
            contact_string = 'Registrant'
        elif contact_type == 'administrativeContact':
            contact_string = 'Administrative'
        elif contact_type == 'technicalContact':
            contact_string = 'Technical'
        country_result = self.get_country_generic(contact_type, contact_string)
        if self.validate_country_result(country_result, contact_string):
            return country_result
        country_result = self.get_country_registrydata(contact_type, contact_string)
        if self.validate_country_result(country_result, contact_string):
            return country_result
        return country_result

    def validate_country_result(self, result, contact_string):
        if result == 'No WHOIS {} Contact Found'.format(contact_string):
            return False
        else:
            return True


if __name__ == '__main__':
    pass