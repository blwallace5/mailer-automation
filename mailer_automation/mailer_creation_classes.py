from __future__ import print_function
import tldextract
import re
import kickbox
import whoisxml_connection
import socket
import errno
import geoip2.webservice
import glob
import sys
import requests
from mysql_database_connection import *
from multiprocessing.dummy import Pool as ThreadPool
from contact_classes import *
from csv_connection import *
from copy import deepcopy
from helpers import *
from imap_connection import IMAPConnection
from majestic_connection import MajesticConnection
from collections import OrderedDict


class BlacklistScreener(object):
    """
    An object that checks the domain and email within a contact against a blacklist

    Attributes:
        blacklist_domains_set (set) : a set of blacklisted domains
        blacklist_emails_set (set) : a set of blacklisted emails
        unscreened_contacts (list) : a list of contacts that need to be screened against blacklist
        blacklisted_contacts (list) : a list of contacts that have been blacklisted
        accepted_contacts (list) : a list of contacts that have passed the screen against the blacklist

    """

    def __init__(self, unscreened_contacts):
        """
        Initializes the BlacklistScreener object

        Args:
            unscreened_contacts (list) : a list of contacts that need to be screened against blacklist
        """

        self.inventory_cxn = None
        self.blacklist_domains_set = set()
        self.blacklist_emails_set = set()
        self.unscreened_contacts = unscreened_contacts
        self.blacklisted_contacts = []
        self.accepted_contacts = []
        self.whitelisted_email_domains = whitelisted_email_domains

    def connect_inventory_cxn(self):
        """
        Sets up a connection to the contact inventory database

        Returns:
            None
        """

        self.inventory_cxn = DBConnection('root', 'vagrant', '127.0.0.1', 'contact_inventory')
        self.inventory_cxn.connect()
        return

    def close_inventory_cxn(self):
        """
        Closes the inventory connection

        Returns:
            None
        """

        self.inventory_cxn.close()
        return

    def load_blacklists(self):
        """
        Loads the blacklist from the contact inventory database table

        Returns:
            None
        """

        self.inventory_cxn.execute_select("SELECT domain FROM domains_blacklist WHERE category = 'portfolio'")
        for blacklist_domain in self.inventory_cxn.cursor_to_flat_list():
            self.blacklist_domains_set.add(blacklist_domain.strip().lower())
        print_message("Loaded {} domains from the portfolio blacklist.".format(len(self.blacklist_domains_set)))
        self.inventory_cxn.execute_select("SELECT email FROM emails_blacklist WHERE category = 'portfolio'")
        for blacklist_email in self.inventory_cxn.cursor_to_flat_list():
            self.blacklist_emails_set.add(blacklist_email.strip().lower())
        print_message("Loaded {} emails from the portfolio blacklist.".format(len(self.blacklist_emails_set)))
        return

    def screen_contacts(self):
        """
        Iterates over the unscreened contacts list and checks domain and email entry against blacklist

        Returns:
            The object's accepted_contacts attributes
        """

        self.connect_inventory_cxn()
        print_message("Loading portfolio blacklists.")
        self.load_blacklists()
        print_message("Blacklists loaded! Now screening emails and domains against the portfolio blacklists...")
        for contact in self.unscreened_contacts:
            blacklisted = False
            source_domain = contact.source_entry.domain
            source_subdomain = contact.source_entry.subdomain
            email_domain = contact.email_entry.domain
            email_subdomain = contact.email_entry.subdomain
            if source_domain and source_domain.strip().lower() in self.blacklist_domains_set:
                blacklisted = True
                contact.contact_action = u'Blacklist Web Domain'
            if source_subdomain and source_subdomain.strip().lower() in self.blacklist_domains_set:
                blacklisted = True
                contact.contact_action = u'Blacklist Web Subdomain'
            if email_domain and email_domain.strip().lower() in self.blacklist_domains_set:
                blacklisted = True
                contact.contact_action = u'Blacklist Email Domain'
            if email_subdomain and email_subdomain.strip().lower() in self.blacklist_domains_set:
                blacklisted = True
                contact.contact_action = u'Blacklist Email Subdomain'
            email = contact.email_entry.email
            if email.lower() in self.blacklist_emails_set:
                blacklisted = True
                contact.contact_action = u'Blacklist Email'
            if blacklisted:
                self.blacklisted_contacts.append(contact)
            else:
                self.accepted_contacts.append(contact)
        self.close_inventory_cxn()
        print_message("Email and domain blacklist screening complete!")
        return self.accepted_contacts, self.blacklisted_contacts


class VerticalBlacklistScreener(BlacklistScreener):
    """
    An object that checks the domain and email within a contact against a blacklist

    Attributes:
        vertical_dict (dict) : a dict containing the {site: vertical} pairs for the portfolio
        vertical_blacklist_domains_dict (dict) : the domains blacklisted per relevant vertical
        vertical_blacklist_emails_dict (dict) : the emails blacklisted per relevant vertical
    """

    def __init__(self, unscreened_contacts):
        """
        Initializes the BlacklistScreener object

        Args:
            unscreened_contacts (list) : a list of contacts that need to be screened against blacklist
        """

        BlacklistScreener.__init__(self, unscreened_contacts)
        self.vertical_dict = {u'ACO': u'EDU',
                              u'ASO': u'EDU',
                              u'CSO': u'EDU',
                              u'FS': u'EDU',
                              u'GG': u'EDU',
                              u'LHTB': u'EDU',
                              u'LP': u'EDU',
                              u'TT': u'EDU',
                              u'MG': u'FIN',
                              u'LGS': u'SOL'}
        self.vertical_blacklist_domains_dict = {u'EDU': set([]),
                                                u'FIN': set([]),
                                                u'SOL': set([])}
        self.vertical_blacklist_emails_dict = {u'EDU': set([]),
                                               u'FIN': set([]),
                                               u'SOL': set([])}

    def manual_site_selection_multiples(self):
        """
        Prompts the user for manual site selection from list of available sites in vertical_dict. Allows multiples

        Returns:
            A set of unicode strings
        """

        ordered_sites_list = sorted([site for site in self.vertical_dict.iterkeys()])
        for index, site in enumerate(ordered_sites_list):
            print("{} : {}".format(index, site))
        while True:
            site_selection_string = raw_input(
                "Please enter the number(s) of the site(s) to select for this file. Separate multiple numbers by commas.\n")
            try:
                site_selection_set = set([ordered_sites_list[int(site_selection.strip())] for site_selection in
                                          site_selection_string.split(',')])
                return site_selection_set
            except (ValueError, IndexError):
                print("Invalid input. Please try again.")

    def manual_site_selection_single(self):
        """
        Prompts the user for manual site selection from list of available sites in vertical_dict.

        Returns:
            A unicode string
        """

        ordered_sites_list = sorted([site for site in self.vertical_dict.iterkeys()])
        for index, site in enumerate(ordered_sites_list):
            print("{} : {}".format(index, site))
        while True:
            site_selection_string = raw_input("Please enter the number of the site to select for this file.\n")
            try:
                site_selection = ordered_sites_list[int(site_selection_string.strip())]
                return site_selection
            except (ValueError, IndexError):
                print("Invalid input. Please try again.")

    def load_blacklists(self):
        """
        Loads the blacklist from the contact inventory database table

        Returns:
            None
        """

        sites_set = set([contact.asset_entry.site for contact in self.unscreened_contacts if contact.asset_entry.site])
        if not sites_set:
            print_message("No 'Site' column in infile. Prompting for manual input.")
            site_to_add = self.manual_site_selection_single()
            for contact in self.unscreened_contacts:
                contact.asset_entry.site = set_string_to_unicode(site_to_add)
            sites_set.add(site_to_add)
        verticals_to_check = set([self.vertical_dict[site] for site in sites_set])
        for vertical in verticals_to_check:
            vertical_string = set_unicode_to_string(vertical)
            self.inventory_cxn.execute_select("SELECT domain FROM domains_blacklist "
                                              "WHERE category = '{}'".format(vertical_string))
            for blacklist_domain in self.inventory_cxn.cursor_to_flat_list():
                blacklist_domain = set_string_to_unicode(blacklist_domain).strip().lower()
                if blacklist_domain not in self.whitelisted_email_domains:
                    self.vertical_blacklist_domains_dict[vertical].add(blacklist_domain)
            print_message(
                "Loaded {} domains from the {} blacklist.".format(len(self.vertical_blacklist_domains_dict[vertical]),
                                                                  vertical_string))
            self.inventory_cxn.execute_select("SELECT email FROM emails_blacklist "
                                              "WHERE category = '{}'".format(vertical_string))
            for blacklist_email in self.inventory_cxn.cursor_to_flat_list():
                blacklist_email = set_string_to_unicode(blacklist_email)
                self.vertical_blacklist_emails_dict[vertical].add(blacklist_email.strip().lower())
            print_message(
                "Loaded {} emails from the {} blacklist.".format(len(self.vertical_blacklist_emails_dict[vertical]),
                                                                 vertical_string))
        super(VerticalBlacklistScreener, self).load_blacklists()
        return

    def screen_contacts_verticals(self):
        """
        Iterates over the unscreened contacts list and checks domain and email entry against blacklist

        Returns:
            The object's accepted_contacts attributes
        """

        self.connect_inventory_cxn()
        print_message("Loading blacklists.")
        self.load_blacklists()
        print_message("Blacklists loaded! Now screening emails and domains against the blacklists...")
        for contact in self.unscreened_contacts:
            blacklisted = False
            source_domain = contact.source_entry.domain
            source_subdomain = contact.source_entry.subdomain
            email_domain = contact.email_entry.domain
            email_subdomain = contact.email_entry.subdomain
            vertical = self.vertical_dict[contact.asset_entry.site]
            if source_domain and source_domain.strip().lower() in self.blacklist_domains_set:
                blacklisted = True
                contact.contact_action = u'Blacklist Web Domain'
            if source_domain and source_domain.strip().lower() in self.vertical_blacklist_domains_dict[vertical]:
                blacklisted = True
                contact.contact_action = u'Vertical Blacklist Web Domain'
            if source_subdomain and source_subdomain.strip().lower() in self.blacklist_domains_set:
                blacklisted = True
                contact.contact_action = u'Blacklist Web Subdomain'
            if source_subdomain and source_subdomain.strip().lower() in self.vertical_blacklist_domains_dict[vertical]:
                blacklisted = True
                contact.contact_action = u'Vertical Blacklist Web Subdomain'
            if email_domain and email_domain.strip().lower() in self.blacklist_domains_set:
                blacklisted = True
                contact.contact_action = u'Blacklist Email Domain'
            if email_domain and email_domain.strip().lower() in self.vertical_blacklist_domains_dict[vertical]:
                blacklisted = True
                contact.contact_action = u'Vertical Blacklist Email Domain'
            if email_subdomain and email_subdomain.strip().lower() in self.blacklist_domains_set:
                blacklisted = True
                contact.contact_action = u'Blacklist Email Subdomain'
            if email_subdomain and email_subdomain.strip().lower() in self.vertical_blacklist_domains_dict[vertical]:
                blacklisted = True
                contact.contact_action = u'Vertical Blacklist Email Subdomain'
            email = contact.email_entry.email
            if email.lower() in self.blacklist_emails_set:
                blacklisted = True
                contact.contact_action = u'Blacklist Email'
            if email.lower() in self.vertical_blacklist_emails_dict[vertical]:
                blacklisted = True
                contact.contact_action = u'Vertical Blacklist Email'
            if blacklisted:
                self.blacklisted_contacts.append(contact)
            else:
                self.accepted_contacts.append(contact)
        self.close_inventory_cxn()
        print_message("Email and domain blacklist screening complete!")
        return self.accepted_contacts, self.blacklisted_contacts


class ContactDeduper(object):
    """
    A class to deduplicate contact lists based on email address
    """
    def __init__(self, non_unique_contacts):
        self.non_unique_contacts = non_unique_contacts
    def dedupe(self):
        return set(self.non_unique_contacts)

class ContactProcessor(object):
    """
    A class to process contacts sourced from one or many of several locations, including database, and csv

    Attributes:
        application_db_cxn (mysql_database_connection.DBConnection) : a database connection to the VA application
        inventory_cxn (mysql_database_connection.DBConnection) : a database connection to the contact inventory
        csv_importer (csv_connection.CSVImporter) : a connection to a CSV file containing contact data
        context (unicode) : the context report number for the current set of contacts to be processed
    """

    def __init__(self):
        """
        Initializes the object, takes no arguments
        """

        self.application_db_cxn = None
        self.inventory_cxn = None
        self.csv_importer = None
        self.context = None

    def build_db_connections(self):
        """
        Opens the ExportHandler's db connections

        Returns:
            None
        """

        self.application_db_cxn = DBConnection('root', 'vagrant', '127.0.0.1', 'va_automation_production')
        self.application_db_cxn.connect()
        self.inventory_cxn = DBConnection('root', 'vagrant', '127.0.0.1', 'contact_inventory')
        self.inventory_cxn.connect()
        return

    def close_db_connections(self):
        """
        Closes open db connections

        Returns:
            None
        """
        self.application_db_cxn.close()
        self.inventory_cxn.close()
        return

    def prompt_context(self):
        """
        Sets the context for the current export

        Returns:
            None
        """

        self.context = raw_input("Please enter the report context you would like to retrieve results from:\n")
        return

    def query_export_proc(self):
        """
        Queries the export stored procedure from the VA application

        Returns:
            None
        """
        self.application_db_cxn.call_procedure("export_query_new", (self.context,))
        return

    def query_context_lines(self):
        """
        Queries the contact inventory database for lines used in current context

        Returns:
            Two lists of contacts
        """

        self.inventory_cxn.execute_select("SELECT * FROM assigned WHERE context=%s", (self.context,))
        assigned_dict = self.inventory_cxn.cursor_to_dict_by_headers()
        assignment_contacts = self.contact_dict_to_contact_objects(assigned_dict)
        print_message("Retrieved {} results from Bullseye output sent to Quiver.".format(len(assignment_contacts)))
        self.inventory_cxn.execute_select("SELECT * FROM duplicates WHERE context=%s", (self.context,))
        duplicated_dict = self.inventory_cxn.cursor_to_dict_by_headers()
        duplicated_contacts = self.contact_dict_to_contact_objects(duplicated_dict)
        print_message(
            "Retrieved {} duplicate results from Bullseye output not sent to Quiver.".format(len(duplicated_contacts)))
        return assignment_contacts, duplicated_contacts

    def query_collected_contacts(self):
        """
        Retrieves the contacts collected by the VAs

        Returns:
            A collected_contacts list of contacts
        """

        self.inventory_cxn.execute_select("SELECT * FROM export_results")
        collected_dict = self.inventory_cxn.cursor_to_dict_by_headers()
        collected_contacts = self.contact_dict_to_contact_objects(collected_dict)
        print_message("Retrieved {} contacts collected in Quiver.".format(len(collected_contacts)))
        return collected_contacts

    def add_location_entries_to_contact(self, contact):
        """
        Adds location entry objects to contact

        Args:
            contact (Contact) : the contact to add location information for

        Returns:
            None
        """

        for entry in [contact.email_entry, contact.source_entry]:
            if not entry.tld_entry:
                entry.tld_entry = TLDEntry(entry.domain)
            if not entry.whois_entry:
                entry.whois_entry = WHOISEntry(entry.domain)
            if not entry.ip_geo_entry:
                entry.ip_geo_entry = IPGeoEntry(entry.subdomain)
        return

    def contact_dict_to_contact_objects(self, contact_dict):
        """
        Iterates over all contacts in a contact dict and translates entries to contact_information.Contact objects

        Headers must match those listed in the method below in order to be recognized. The only exception to this
        is the context field, which is filled directly from the ContextProcessor object's 'context' attribute

        Args:
            contact_dict (dict) : a dict containing contact info in the form {header : [fields in column...,]}
        """

        contacts_list = []
        index = 0
        while True:
            try:
                source_entry = DomainEntry()
                target_entry = DomainEntry()
                email_entry = EmailEntry()
                asset_entry = AssetEntry()
                send_vars_entry = SendVarsEntry()
                contact_name = u''
                date_collected = None
                contact_type = None
                for header in contact_dict:
                    if header == u'target_url' or header == u'targeturl':
                        target_entry.url = contact_dict[header][index].lower().strip()
                    elif header == u'target_domain' or header == u'targetdomain':
                        target_entry.domain = contact_dict[header][index].lower().strip()
                    elif header == u'source_url' or header == u'sourceurl':
                        source_entry.url = contact_dict[header][index].lower().strip()
                    elif header == u'source_domain' or header == u'sourcedomain' or header == u'contact_domain' \
                            or header == u'contactdomain':
                        source_entry.domain = contact_dict[header][index].lower().strip()
                    elif header == u'source_domain_trust_flow' or header == u'sourcedomaintrustflow':
                        source_entry.trust_flow = contact_dict[header][index]
                    elif header == u'source_domain_topic' or header == u'sourcedomaintopic':
                        source_entry.topic = contact_dict[header][index].strip()
                    elif header == u'source_domain_topic_trust_flow' or header == u'sourcedomaintopictrustflow' \
                            or header == u'source_domain_topic_trust' or header == u'sourcedomaintopictrust':
                        source_entry.topic_trust_flow = contact_dict[header][index]
                    elif header == u'contact_name' or header == u'contactname' or header == u'name':
                        contact_name = contact_dict[header][index].strip()
                    elif header == u'contact_email' or header == u'contactemail' or header == u'email':
                        email_entry.email = contact_dict[header][index].lower().strip()
                    elif header == u'asset':
                        asset_entry.asset = contact_dict[header][index].strip()
                    elif header == u'campaign' or header == u'campaign_':
                        asset_entry.campaign = contact_dict[header][index].strip()
                    elif header == u'variable' or header == u'email_variant' or header == u'emailvariant' \
                            or header == u'variable_':
                        asset_entry.email_variant = contact_dict[header][index].strip()
                    elif header == u'email_notes' or header == u'emailnotes':
                        asset_entry.email_notes = contact_dict[header][index].strip()
                    elif header == u'date_assigned' or header == u'dateassigned' or header == u'date_collected' \
                            or header == u'datecollected' or header == u'date_added' or header == u'dateadded':
                        date_collected = contact_dict[header][index].strip()
                    elif header == u'type' or header == u'type_':
                        contact_type = contact_dict[header][index].strip()
                    elif header == u'site':
                        asset_entry.site = contact_dict[header][index].strip()
                    elif header == u'asset_url' or header == u'asseturl':
                        asset_entry.asset_url = contact_dict[header][index].strip().lower()
                    elif header == u'content_group' or header == u'contentgroup':
                        asset_entry.content_group = contact_dict[header][index].strip()
                    elif header == u'series':
                        asset_entry.series = contact_dict[header][index].strip()
                    elif header == u'audience' or header == u'audiences':
                        asset_entry.audiences = contact_dict[header][index].strip()
                    elif header == u'context':
                        asset_entry.context = contact_dict[header][index].strip()
                    elif header == u'entity_name' or header == u'entityname' or header == u'school_name' \
                            or header == u'schoolname' or header == u'school' or header == u'institution_name' \
                            or header == u'institutionname':
                        source_entry.entity_name = contact_dict[header][index].strip()
                    elif header == u'entity_type' or header == u'entitytype':
                        source_entry.entity_type = contact_dict[header][index].strip()
                    elif header == u'city':
                        source_entry.city = contact_dict[header][index].strip()
                    elif header == u'state' or header == u'state_abbreviation':
                        source_entry.state = contact_dict[header][index].strip()
                    elif header == u'entity_subgroup' or header == u'entitysubgroup':
                        source_entry.entity_subgroup = contact_dict[header][index].strip()
                    elif header == u'entity_subgroup_url' or header == u'entitysubgroupurl' or header == u'subgroupurl' \
                            or header == u'subgroup_url' or header == u'department_website' or header == u'departmentwebsite':
                        source_entry.entity_subgroup_url = verify_not_null(contact_dict[header][index].strip().lower())
                    elif header == u'entity_subgroup_resources_url' or header == u'entitysubgroupresourcesurl' \
                            or header == u'department_resources' or header == u'departmentresources':
                        source_entry.entity_subgroup_resources_url = verify_not_null(
                            contact_dict[header][index].strip().lower())
                    elif header == u'entity_website' or header == u'entitywebsite' or header == u'school_website' or header == u'schoolwebsite':
                        source_entry.entity_website = verify_not_null(contact_dict[header][index].strip().lower())
                    elif header == u'subject' or header == u'subjectline' or header == u'subject_line':
                        send_vars_entry.subject = contact_dict[header][index]
                    elif header in [u'var' + str(i + 1) for i in range(8)]:  # look for vars 1-8
                        setattr(send_vars_entry, header, contact_dict[header][index])
                    elif header == u'bucket':
                        send_vars_entry.bucket = contact_dict[header][index]
                index += 1
                try:
                    if not asset_entry.context:
                        asset_entry.context = self.context
                    contact_entry = Contact(contact_name, date_collected, contact_type,
                                            email_entry, source_entry, target_entry, asset_entry, send_vars_entry)
                    self.update_source_domain_and_subdomain(contact_entry)
                    self.extract_email_domain(contact_entry.email_entry)
                    self.add_location_entries_to_contact(contact_entry)
                    contacts_list.append(contact_entry)
                except NameError:
                    print_message("Attempted to create contact with no name field in data. Check data source.")
            except IndexError:
                break
        return contacts_list

    def merge_contacts(self, assigned_contacts, duplicate_contacts, collected_contacts):
        """
        Merges together Contact objects by linking together on the same Contact.source_entry.url field

        Args:
            assigned_contacts (list) : a list of Contact objects containing empty name and email fields
            duplicate_contacts (list) : a list of Contact objects containing empty name and email fields
            collected_contacts (list) : a list of Contact objects containing contact information from the VA system

        Returns:
            A list of merged contacts from all sources and no missing fields
        """

        source_url_contact_lookup_dict = {}
        merged_contacts = []

        for contact in collected_contacts:
            source_url = contact.source_entry.url
            if source_url not in source_url_contact_lookup_dict:
                source_url_contact_lookup_dict[source_url] = []
            source_url_contact_lookup_dict[source_url].append(contact)
        for contact_list in (assigned_contacts, duplicate_contacts):
            for contact_entry in contact_list:
                current_source_url = contact_entry.source_entry.url
                if current_source_url in source_url_contact_lookup_dict:
                    target_contacts = source_url_contact_lookup_dict[current_source_url]
                    contact_name = target_contacts[0].contact_name
                    contact_email = target_contacts[0].email_entry.email
                    extra_contact = deepcopy(contact_entry) if len(target_contacts) > 1 else None
                    contact_entry.contact_name = contact_name
                    contact_entry.email_entry.email = contact_email
                    merged_contacts.append(contact_entry)
                    if extra_contact:
                        contact_name = target_contacts[1].contact_name
                        contact_email = target_contacts[1].email_entry.email
                        extra_contact.contact_name = contact_name
                        extra_contact.email_entry.email = contact_email
                        merged_contacts.append(extra_contact)
                else:  # email address was not found for contact.
                    continue
        return merged_contacts

    def build_csv_importer(self, filename):
        """
        Creates a CSVImporter object for ContactProcessor, takes no args

        Returns:
            None
        """

        filename_parts = filename.split('/')
        last_part_filename = filename_parts[-1]
        self.csv_importer = CSVImporter(last_part_filename)
        return

    def get_contacts_from_database(self):
        """
        Retrieves a list of contact objects from data stored in VA and contact inventory databases

        Returns:
            A list of Contact objects
        """
        print_message("Connecting to Databases...")
        self.build_db_connections()
        print_message("Connected!")
        self.prompt_context()
        print_message("Thank you! Now querying for results...")
        self.query_export_proc()
        collected_contacts = self.query_collected_contacts()
        print_message("Results retrieved! Now retrieving list of assigned lines...")
        assigned_contacts, duplicate_contacts = self.query_context_lines()
        print_message("Assigned lines retrieved! Now merging collected contacts into assigned lines...")
        contacts_list = self.merge_contacts(assigned_contacts, duplicate_contacts, collected_contacts)
        print_message("Merge complete!\n")
        self.close_db_connections()
        return contacts_list

    def get_contacts_from_csv(self):
        """
        Retrieves a list of contact objects from data stored in a CSV file.

        Returns:
            A list of Contact objects
        """

        file_list = [csvfile for csvfile in glob.glob('run_configuration/infiles/*.csv')]
        filename = selection_from_list(file_list)
        self.build_csv_importer(filename)
        self.csv_importer.import_csv_to_dict()
        contact_dict = self.csv_importer.data_dict
        contacts_list = self.contact_dict_to_contact_objects(contact_dict)
        print_message("Imported {} contacts from {} file.".format(len(contacts_list), filename))
        return contacts_list

    def select_current_file(self, file_list):
        index = 1
        file_key_dict = {}
        for filename in file_list:
            file_key_dict[index] = filename
            print('{}:  {}'.format(index, filename))
            index += 1
        selected_file = ''
        while True:
            try:
                selected_int = int(
                    raw_input("Please enter the number corresponding to the input file you wish to add.\n"))
                selected_file = file_key_dict[selected_int]
                break
            except ValueError:
                print_message("Invalid entry.")
            except KeyError:
                print_message("Invalid entry.")
        return selected_file

    def update_source_domain_and_subdomain(self, contact):
        """
        Updates the source_domain and subdomain for a contact using tldextract

        Returns:
            None
        """

        source_url = contact.source_entry.url
        entity_subgroup_resources_url = contact.source_entry.entity_subgroup_resources_url
        entity_subgroup_url = contact.source_entry.entity_subgroup_url
        entity_website = contact.source_entry.entity_website
        source_domain = contact.source_entry.domain
        attempts = [source_url, entity_subgroup_resources_url, entity_subgroup_url, entity_website, source_domain]
        for attempt in attempts:
            if attempt:
                extract_object = tldextract.extract(attempt)
                domain = u'.'.join([extract_object.domain, extract_object.suffix])
                if extract_object.subdomain:
                    subdomain = u'.'.join([extract_object.subdomain, extract_object.domain, extract_object.suffix])
                else:
                    subdomain = domain
                contact.source_entry.domain = domain
                contact.source_entry.subdomain = subdomain
                break
        return

    def get_contacts_from_sources(self):
        """
        A command line user interface to select data sources for the ContactProcessor

        Returns:
            A list of Contact objects
        """

        contacts_list = []
        while True:
            selection = raw_input("Please enter the number corresponding to the data source type to add: \n"
                                  "1 - CSV File \n"
                                  "2 - VA Application \n"
                                  "3 - No more data to add \n")
            if selection == '1':
                for contact in self.get_contacts_from_csv():
                    contacts_list.append(contact)
            elif selection == '2':
                for contact in self.get_contacts_from_database():
                    contacts_list.append(contact)
            elif selection == '3':
                break
            else:
                print_message(
                    "Unrecognized selection. Please enter 1 or 2 to select another dataset, or 3 to add no more.")
        return contacts_list

    def get_contacts_from_database_source_only(self):
        """
        A command line user interface to select data sources for the ContactProcessor

        Returns:
            A list of Contact objects
        """

        contacts_list = []
        while True:
            selection = raw_input("Please enter the number corresponding to the data source type to add: \n"
                                  "1 - VA Application \n"
                                  "2 - No more data to add \n")
            if selection == '1':
                for contact in self.get_contacts_from_database():
                    contacts_list.append(contact)
            elif selection == '2':
                break
            else:
                print_message(
                    "Unrecognized selection. Please enter 1 or 2 to select another dataset, or 3 to add no more.")
        return contacts_list

    def extract_email_domain(self, email_entry):
        """
        Extracts and returns the domain from an email address

        Args:
            email_entry (EmailEntry) : an EmailEntry object to have the domain and subdomain determined for

        Returns:
            None
        """

        if not email_entry.email:
            return
        email_domain_extract = tldextract.extract(email_entry.email)
        if email_domain_extract.subdomain:
            email_entry.subdomain = u'.'.join([email_domain_extract.subdomain, email_domain_extract.domain,
                                               email_domain_extract.suffix])
        else:
            email_entry.subdomain = u'.'.join([email_domain_extract.domain, email_domain_extract.suffix])
        email_entry.domain = u'.'.join([email_domain_extract.domain, email_domain_extract.suffix])
        return


class CompContactProcessor(ContactProcessor):
    def __init__(self):
        super(CompContactProcessor, self).__init__()
        self.initial_mailer_emails = []

    def get_initial_mailer_emails(self):
        file_list = [csvfile for csvfile in glob.glob('run_configuration/infiles/*.csv')]
        filename = selection_from_list(file_list)
        self.build_csv_importer(filename)
        self.csv_importer.import_csv_to_dict()
        contact_dict = self.csv_importer.data_dict
        self.contact_dict_to_email_list(contact_dict)
        return

    def contact_dict_to_email_list(self, contact_dict):
        index = 0
        while True:
            try:
                for header in contact_dict:
                    if header == u'contact_email' or header == u'contactemail' or header == u'email':
                        email = contact_dict[header][index].lower().strip()
                        self.initial_mailer_emails.append(email)
                index += 1
            except IndexError:
                break
        return

    def compare_contacts_to_previous_send(self, contact_objs):
        for contact in contact_objs:
            if contact.email_entry.email in self.initial_mailer_emails:
                contact.previously_contacted = True
        return

    def get_contacts_and_compare(self):
        print_message("Please select the prelim mailer for this campaign.")
        contacts = self.get_contacts_from_csv()
        print_message("Please select the intial mailer to compare against.")
        self.get_initial_mailer_emails()
        self.compare_contacts_to_previous_send(contacts)
        return contacts


class DataCleaner(object):
    """
    An object used to clean contacts names and emails according to syntactical and business rules

    corrected_emails_cache (dict) : a cache of emails that have been corrected by cleaning to avoid duplicate effort
    corrected_names_cache (dict) : a cache of names that have been corrected by cleaning to avoid duplicate effort
    contacts_list (list) : a list of contact objects
    tld_set (set) : a set of valid TLDs
    corrected_contacts_list (list) : a list of contacts with cleaned info
    invalid_contacts_list (list) : a list of contacts with invalid info that did not pass cleaning rules
    previously_fixed_names (dict) : a a cache of names cleaned in previous runs of this script
    previously_fixed_emails (dict) : a a cache of emails cleaned in previous runs of this script
    inventory_cxn (DBConnection) : a database connection for loading/updating the previously fixed email and name caches
    """

    def __init__(self, contacts_list):
        """
        Initializes the object

        Args:
            contacts_list (list) : A list of contacts objects with data not yet cleaned
        """

        self.corrected_emails_cache = {}
        self.corrected_names_cache = {}
        self.contacts_list = contacts_list
        self.tld_set = tldextract.tldextract.TLD_EXTRACTOR.tlds
        self.corrected_contacts_list = []
        self.invalid_contacts_list = []
        self.previously_fixed_names = {}
        self.previously_fixed_emails = {}
        self.fixed_names_to_add_to_cache = {}
        self.fixed_emails_to_add_to_cache = {}
        self.role_based_email_set = set()
        self.inventory_cxn = None

    def is_valid_email_syntax(self, email):
        """
        Runs a naive check for basic email syntax following form ___@___.___

        Args:
            email (unicode) : an email address to be checked for valid syntax

        Returns:
            True if valid syntax, False if not
        """

        if re.match(u'[^@]+@[^@]+\.[^@]+$', email):
            return True
        else:
            return False

    def is_role_based_email(self, email):
        """
        Checks to see if the email matches against any of the forbidden role-type emails

        Args:
            email (unicode): an email address to be checked for any of role-based substrings

        Returns:
            True if role-based, False if not
        """

        for role_based_substring in self.role_based_email_set:
            if role_based_substring in email:
                return True
        return False

    def fix_common_email_typos(self, email):
        """
        Fixes common email typos observed from VA team.

        Args:
            email (unicode) : an email address to be checked for common typos

        Returns:
            The input email, cleaned of typos.
        """
        email = email.lower()
        email = email.replace(u'%20', u'')
        email = email.replace(u'mailto:', u'')
        email = email.replace(u' ', u'')
        email = email.replace(u'.qov', u'.gov') if email.endswith(u'.qov') else email
        email = email.replace(u'.orr', u'.org') if email.endswith(u'.orr') else email
        email = email.replace(u'.go', u'.gov') if email.endswith(u'.go') else email
        email = email.replace(u'.ed', u'.edu') if email.endswith(u'.ed') else email
        email = email.replace(u'http://', u'') if email.startswith(u'http://') else email
        email = email.replace(u'https://', u'') if email.startswith(u'https://') else email
        return email

    def remove_punctuation_beginning_and_end(self, email):
        """
        Recursively removes several punctuation marks from the start and end of an email

        Args:
            email (unicode) : the email address to be checked for punctuation marks

        Returns:
            The input email, stripped of any punctuation marks at the beginning or end of end
        """

        punctuation_set = {u'.', u'?', u'!', u')', u'(', u',', u':', u';', u'<', u'>', u'@', u'[', u'\\', u']'}
        original_email = email
        email = email.strip()
        if not email:
            return u''
        if email[0] in punctuation_set:
            email = email[1:]
            if not email:
                return u''
        if email[-1] in punctuation_set:
            email = email[:-1]
            if not email:
                return u''
        if email == original_email:
            return email
        else:
            return self.remove_punctuation_beginning_and_end(email)

    def deduplicate_character(self, string, character):
        """
        Recursively de-duplicates consecutive occurrences of a character from a string

        Args:
            string (unicode) : a string to have a character deduplicated
            character (unicode) : the character to be de-duped within string

        Returns:
            A string with the stated character's consecutive occurrences deduplicated
        """

        new_string = string.replace(character * 2, character)
        if new_string == string:
            return new_string
        else:
            return self.deduplicate_character(new_string, character)

    def strip_extra_chars_after_domain(self, email, original_email=None):
        """
        Recursively strips any extra characters mistakenly added to end of email domain

        Args:
            email (unicode) : the email to be checked for extra characters
            original_email (unicode) : Optional. The original email as given to the first call of the function

        Returns:
            An email with extra characters stripped
        """

        if not original_email:
            original_email = email
        new_email = email
        final_dot = email.rfind(u'.')
        if final_dot == -1 or email.find(u'@') == -1:
            return self.prompt_manual_clean(original_email, 'Unable to parse email address. '
                                                            'Please enter the correct email address manually.')
        try:
            last_section = email[final_dot + 1:]
            if last_section not in self.tld_set:
                new_email = email[:final_dot]
            if new_email == email:
                return new_email
            else:
                return self.strip_extra_chars_after_domain(new_email, original_email)
        except IndexError:
            return self.strip_extra_chars_after_domain(email[:-1], original_email)

    def add_space_after_character(self, name, character):
        """
        Adds whitespace to the middle of a name, immediately after the first occurrence of character

        Args:
            name (unicode) : a name to have checked for character
            character (unicode) : a single character to have whitespace inserted after within name

        Returns:
            Name, with whitespace added after the first occurrence of character, if any
        """

        index = name.find(character)
        try:
            if not name[index + 1] == u' ':
                name = name[:index + 1] + u' ' + name[index + 1:]
            return name
        except IndexError:
            return name

    def fix_common_name_typos(self, name):
        """
        Fixes common email typos observed from VA team.

        Args:
            name (unicode) : a string containing the name to have common typos fixed

        Returns:
            The name string with typos fixed
        """

        name = name.strip()
        name = name.replace(u',', u'.')
        return name

    def check_period_after_dr(self, name):
        """
        Checks for a period after "Dr "

        Args:
            name (unicode) : the name to be corrected

        Returns:
            The corrected name
        """

        if name.startswith(u'Dr '):
            return name[:2] + u'.' + name[2:]
        else:
            return name

    def prompt_manual_clean(self, string, message):
        """
        Prompts user for a manual input based on string and message.

        Args:
            string (basestring) : the string that requires raw_input
            message (str) : the message describes the reason for action needed

        Returns:
            A string from raw_input
        """

        # string = string.replace('\0xa0', ' ')
        string = set_unicode_to_string(string)
        prompt = "\n{}\n{}\n".format(message, string)
        return raw_input(prompt)

    def check_spaces_in_name(self, name):
        """
        Checks to see if name has spaces and that space follows occurrence rules

        Args:
            name (unicode) : the name to be checked for spaces

        Returns:
            The corrected name, and bool stating whether manual clean was used
        """

        space_count = len(re.findall(u'\s', name))
        if space_count > 1:
            return self.prompt_manual_clean(name, 'Too many spaces in name. Please enter the correct '
                                                  'name manually.'), True
        elif space_count == 1:
            space_loc = name.find(u' ')
            period_loc = name.find(u'.')
            if period_loc + 1 != space_loc:
                return self.prompt_manual_clean(name, 'Space detected in unexpected location '
                                                      '(must follow a period). Please enter the '
                                                      'correct name manually.'), True
        elif space_count == 0 and name[:3] == u'Dr.':
            name = name[:3] + u' ' + name[3:]
        return name, False

    def check_periods_in_name(self, name):
        """
        Validates that name has periods in the correct locations.

        Periods are only permitted following the prefix "Dr" and in initials (e.g., "A.J.").
        Dr is the only allowable prefix.

        Args:
            name (unicode) : the name to check for periods

        Returns:
            The corrected name, and bool stating whether manual clean was used
        """

        period_count = len(re.findall(u'\.', name))
        if period_count > 1:
            try:
                if name[-1] == u'.' and not name[-3] == u'.':
                    name = name[:-1]
                    period_count -= 1
            except IndexError:
                return self.prompt_manual_clean(name, 'Multiple periods detected but name too short. '
                                                      'Please enter the correct name manually.'), True
            if name[0] == u'.':
                name = name[1:]
                period_count -= 1
            if period_count > 1 and not (name[1] == u'.' and name[3] == u'.' and len(name) == 4):
                return self.prompt_manual_clean(name, 'Too many periods detected in name. '
                                                      'Please enter the correct name manually.'), True
        if period_count == 1:
            period_loc = name.find(u'.')
            if period_loc != 2:
                if name[-1] == u'.':
                    return self.prompt_manual_clean(name, 'Period in unexpected location. '
                                                          'Please enter the correct name manually.'), True
                elif len(name) >= period_loc + 2 and name[period_loc + 1] == u' ':
                    return self.prompt_manual_clean(name, 'Unexpected period and space within name. '
                                                          'Please enter the correct name manually.'), True
                else:
                    return self.prompt_manual_clean(name, 'Period in unexpected location and not '
                                                          'followed by space. Please enter the '
                                                          'correct name manually.'), True
            elif name[:2] not in [u'Dr', u'dr']:
                return self.prompt_manual_clean(name, 'Invalid prefix (not "Dr.") used. '
                                                      'Please enter the correct name manually.'), True
            elif len(name) == 3:
                return self.prompt_manual_clean(name, 'Name too short to have a valid period used. '
                                                      'Please enter the correct name manually.'), True
        name = name.strip()
        return name, False

    def format_unknown_names(self, name):
        """
        Replaces unknown names with lowercase there.

        Args:
            name (unicode) : the name to be checked for needed formatting

        Returns:
            The formatted name
        """

        if name == u'':
            name = u'there'
        if name is None:
            name = u'there'
        if name == u'There':
            name = u'there'
        return name

    def check_numbers_in_name(self, name):
        """
        Checks to see if there are any numbers in the name and, if so, prompts manual clean

        Returns:
            A name and bool stating whether or not manual clean was used.
        """

        if bool(re.search(u'\d', name)):
            return self.prompt_manual_clean(name, 'Numbers detected in name. '
                                                  'Please enter correct name manually.'), True
        else:
            return name, False

    def check_at_symbol_in_name(self, name):
        """
        Checks to see if there is an @ symbol in the name and, if so, prompts manual cleaning

        Args:
            name (unicode) : a unicode string containing a name to be checked for an @ symbol

        Returns:
            A name and bool stating whether or not manual clean was used.

        """

        if bool(re.search(u'@', name)):
            return self.prompt_manual_clean(name, 'Detected "@" symbol in name. '
                                                  'Please enter correct name manually.'), True
        else:
            return name, False

    def titlecase_name(self, name):
        """
        Titlecases a word without forcing all non-first characters to be lowercased

        Args:
            name (unicode) : a unicode string containing a name to be titlecased

        Returns:
            A name with the first letter of each word capitalized, and bool stating whether manual clean was used
        """

        name_characters = []
        after_space_or_period = True
        for character in name:
            if after_space_or_period:
                name_characters.append(character.upper())
            elif character.isupper():
                return self.prompt_manual_clean(name, 'Capital letter detected in unexpected location. '
                                                      'Please enter the correct name manually.'), True
            else:
                name_characters.append(character)
            if character == u' ' or character == u'.':
                after_space_or_period = True
            else:
                after_space_or_period = False
        return ''.join(name_characters), False

    def extract_email_domain(self, email_entry):
        """
        Extracts and returns the domain from an email address

        Args:
            email_entry (EmailEntry) : an EmailEntry object to have the domain and subdomain determined for

        Returns:
            None
        """

        if not email_entry.email:
            return
        email_domain_extract = tldextract.extract(email_entry.email)
        if email_domain_extract.subdomain:
            email_entry.subdomain = u'.'.join([email_domain_extract.subdomain, email_domain_extract.domain,
                                               email_domain_extract.suffix])
        else:
            email_entry.subdomain = u'.'.join([email_domain_extract.domain, email_domain_extract.suffix])
        email_entry.domain = u'.'.join([email_domain_extract.domain, email_domain_extract.suffix])
        return

    def correct_email(self, email):
        """
        Applies all potential corrections to email

        Args:
            email (unicode) : the email to have corrections applied

        Returns:
            The corrected email string if the email is not an admissions one and is valid, else None
        """

        if not email:
            return None
        if email in self.previously_fixed_emails:
            return self.previously_fixed_emails[email]
        else:
            original_email = email
            email = self.remove_punctuation_beginning_and_end(email)
            email = self.deduplicate_character(email, u'@')
            email = self.deduplicate_character(email, u'.')
            email = self.fix_common_email_typos(email)
            email = self.strip_extra_chars_after_domain(email)
            self.fixed_emails_to_add_to_cache[original_email] = email
            is_valid_email = self.is_valid_email_syntax(email)
            if not is_valid_email or self.is_role_based_email(email):
                return None
            else:
                return email

    def correct_contact_email(self, contact):
        """
        Applies all potential corrections to a contact's email

        Args:
            contact (ContactEntry) : a contact to have the email corrected

        Returns:
            The corrected email strig if the email is not role-based and has valid syntax, else None
        """

        email = contact.email_entry.email
        if not email:
            contact.contact_action = u'Cut - No Email Address'
            return None
        if email in self.previously_fixed_emails:
            return self.previously_fixed_emails[email]
        else:
            original_email = email
            email = self.remove_punctuation_beginning_and_end(email)
            email = self.deduplicate_character(email, u'@')
            email = self.deduplicate_character(email, u'.')
            email = self.fix_common_email_typos(email)
            email = self.strip_extra_chars_after_domain(email)
            self.fixed_emails_to_add_to_cache[original_email] = email
            is_valid_email = self.is_valid_email_syntax(email)
            if not is_valid_email:
                contact.contact_action = u'Cut - Invalid Email Syntax'
                return None
            is_role_based = self.is_role_based_email(email)
            if is_role_based:
                contact.contact_action = u'Cut - Forbidden Role-Based Email'
                return None
            else:
                return email

    def correct_name(self, name):
        """
        Applies all potential corrections to name

        Args:
            name (unicode) : the name to have corrections applied

        Returns:
            The name string corrected
        """

        if name in self.previously_fixed_names:
            return self.previously_fixed_names[name]
        else:
            original_name = name
            if len(name) == 2:
                name = self.prompt_manual_clean(name,
                                                "Unable to automatically detect proper formatting of 2-letter names. "
                                                "Please enter correct name manually.")
            else:
                name = self.fix_common_name_typos(name)
                name = self.deduplicate_character(name, u' ')
                name = self.deduplicate_character(name, u'.')
                name = self.check_period_after_dr(name)
                name, manually_cleaned = self.check_spaces_in_name(name)
                if manually_cleaned:
                    name = self.format_unknown_names(name)
                    self.fixed_names_to_add_to_cache[original_name] = name
                    return name
                name, manually_cleaned = self.check_periods_in_name(name)
                if manually_cleaned:
                    name = self.format_unknown_names(name)
                    self.fixed_names_to_add_to_cache[original_name] = name
                    return name
                name, manually_cleaned = self.titlecase_name(name)
                if manually_cleaned:
                    name = self.format_unknown_names(name)
                    self.fixed_names_to_add_to_cache[original_name] = name
                    return name
                name, manually_cleaned = self.check_numbers_in_name(name)
                if manually_cleaned:
                    name = self.format_unknown_names(name)
                    self.fixed_names_to_add_to_cache[original_name] = name
                    return name
                name, manually_cleaned = self.check_at_symbol_in_name(name)
                if manually_cleaned:
                    name = self.format_unknown_names(name)
                    self.fixed_names_to_add_to_cache[original_name] = name
                    return name
            name = self.format_unknown_names(name)
            self.fixed_names_to_add_to_cache[original_name] = name
            return name

    def connect_inventory_cxn(self):
        """
        Sets up a connection to the contact inventory database

        Returns:
            None
        """

        self.inventory_cxn = DBConnection('root', 'vagrant', '127.0.0.1', 'contact_inventory')
        self.inventory_cxn.connect()
        return

    def build_previously_fixed_names_cache(self):
        """
        Builds the previously_fixed_names_cache dict from the contact inventory DB connection

        Returns:
            None
        """

        self.inventory_cxn.execute_select("SELECT incorrect_name, corrected_name FROM previously_fixed_names")
        self.previously_fixed_names = dict(self.inventory_cxn.cursor_to_dict())
        print_message("Retrieved {} names from the correction cache.".format(len(self.previously_fixed_names.keys())))
        return

    def build_previously_fixed_emails_cache(self):
        """
        Builds the previously_fixed_emails_cache dict from the contact inventory DB connection

        Returns:
            None
        """

        self.inventory_cxn.execute_select("SELECT incorrect_email, corrected_email FROM previously_fixed_emails")
        self.previously_fixed_emails = dict(self.inventory_cxn.cursor_to_dict())
        print_message("Retrieved {} emails from the correction cache.".format(len(self.previously_fixed_emails.keys())))
        return

    def build_role_based_emails_set(self):
        """
        Builds the role_based_emails_set containing substrings forbidden in emails

        Returns:
            None
        """

        self.inventory_cxn.execute_select("SELECT role_based_substring FROM role_based_emails")
        self.role_based_email_set = set(self.inventory_cxn.cursor_to_set())
        print_message(
            "Retrieved {} role-based email substrings from the blacklist.".format(len(self.role_based_email_set)))
        return

    def update_email_name_caches(self):
        """
        Updates the previously fixed emails and names caches in contact inventory DB with new values

        Returns:
            None
        """
        statement = "INSERT INTO previously_fixed_emails (incorrect_email, corrected_email) VALUES (%s, %s)"
        value_list = [(incorrect, correct) for incorrect, correct in self.fixed_emails_to_add_to_cache.iteritems()]
        if value_list:
            self.inventory_cxn.execute_update_many(statement, value_list)
        statement = "INSERT INTO previously_fixed_names (incorrect_name, corrected_name) VALUES (%s, %s)"
        value_list = [(incorrect, correct) for incorrect, correct in self.fixed_names_to_add_to_cache.iteritems()]
        if value_list:
            self.inventory_cxn.execute_update_many(statement, value_list)
        return

    def clean_contacts(self):
        """
        Applies cleaning methods to names and emails of contacts unless those emails and names have already been cached

        Returns:
            The object's corrected_contacts_list attribute
        """
        print_message("Retrieving email and name caches from database...")
        self.connect_inventory_cxn()
        self.build_previously_fixed_emails_cache()
        self.build_previously_fixed_names_cache()
        self.build_role_based_emails_set()

        print_message("Now cleaning contacts...")
        for contact in self.contacts_list:
            email = contact.email_entry.email
            if email not in self.corrected_emails_cache:
                self.corrected_emails_cache[email] = self.correct_contact_email(contact)
            contact.email_entry.email = self.corrected_emails_cache[email]
            self.extract_email_domain(contact.email_entry)
            name = contact.contact_name
            if not name:
                name = u''
            if name not in self.corrected_names_cache:
                self.corrected_names_cache[name] = self.correct_name(name)
            contact.contact_name = self.corrected_names_cache[name]
            if contact.email_entry.email:
                self.corrected_contacts_list.append(contact)
            else:
                self.invalid_contacts_list.append(contact)
        print_message("All contacts cleaned!")
        print_message("Updating email and name caches...")
        self.update_email_name_caches()
        return self.corrected_contacts_list, self.invalid_contacts_list


class DomainChecker(object):
    """
    A class that checks the location and registration information for domains

    Attributes:
        contacts_list (list) : a list of contacts objects to have geo/ip/registration info checked
        inventory_cxn (DBConnection) : a database connection for loading/updating the blacklist and whitelist caches
        blacklist_root_domains_cache (set) : a set of root domains that have been blacklisted
        whitelist_root_domains_cache (set) : a set of root domains that have been whitelisted
        blacklist_root_domains_cache_to_add (set) : a set of root domains to add to the blacklist cache in the DB
        whitelist_root_domains_cache_to_add (set) : a set of root domains to add to the whitelist cache in the DB
        blacklist_subdomains_cache (set) : a set of subdomains that have been blacklisted
        whitelist_subdomains_cache (set) : a set of subdomains that have been whitelisted
        blacklist_subdomains_cache_to_add (set) : a set of subdomains to add to the blacklist cache in the DB
        whitelist_subdomains_cache_to_add (set) : a set of subdomains to add to the whitelist cache in the DB
        tld_type_dict (dict) : a dict from CSVImporter that contains the tlds and their respective tld_type
        locale_type_dict (dict) : a dict from CSVImporter that contains the known locales and their classifications
        cut_contacts (list) : a list of contacts that need to be cut based on geo-checking rules
        kept_contacts (list) : a list of contacts that may be kept based on geo-checking rules
    """

    def __init__(self, contacts_list):
        """
        Initializes the DomainChecker object

        Args:
            contacts_list (list) : a list of contacts objects to have geo/ip/registration info checked
        """

        self.contacts_list = contacts_list
        self.inventory_cxn = None
        self.blacklist_root_domains_cache = set()
        self.whitelist_root_domains_cache = set()
        self.blacklist_root_domains_cache_to_add = set()
        self.whitelist_root_domains_cache_to_add = set()
        self.blacklist_subdomains_cache = set()
        self.whitelist_subdomains_cache = set()
        self.blacklist_subdomains_cache_to_add = set()
        self.whitelist_subdomains_cache_to_add = set()
        self.tld_type_dict = {}
        self.locale_type_dict = {}
        self.cut_contacts = []
        self.kept_contacts = []

    def connect_inventory_cxn(self):
        """
        Sets up a connection to the contact inventory database

        Returns:
            None
        """

        self.inventory_cxn = DBConnection('root', 'vagrant', '127.0.0.1', 'contact_inventory')
        self.inventory_cxn.connect()
        return

    def close_inventory_cxn(self):
        """
        Closes the inventory connection

        Returns:
            None
        """

        self.inventory_cxn.close()
        return

    def build_geo_caches(self):
        """
        Builds the geo cache sets from the contact inventory DB connection

        Returns:
            None
        """

        self.inventory_cxn.execute_select("SELECT domain FROM geo_domains_blacklist_cache WHERE level = 'domain'")
        self.blacklist_root_domains_cache = set([domain.lower() for domain in self.inventory_cxn.cursor_to_set()
                                                 if domain])
        print_message(
            "Retrieved {} domains from the geo domains blacklist cache.".format(len(self.blacklist_root_domains_cache)))
        self.inventory_cxn.execute_select("SELECT domain FROM geo_domains_blacklist_cache WHERE level = 'subdomain'")
        self.blacklist_subdomains_cache = set([domain.lower() for domain in self.inventory_cxn.cursor_to_set()
                                               if domain])
        print_message(
            "Retrieved {} domains from the geo subdomains blacklist cache.".format(
                len(self.blacklist_subdomains_cache)))
        self.inventory_cxn.execute_update("DELETE FROM geo_domains_whitelist_cache "
                                          "WHERE datediff(insert_date, curdate()) < -30")
        self.inventory_cxn.execute_select("SELECT domain FROM geo_domains_whitelist_cache WHERE level = 'domain'")
        self.whitelist_root_domains_cache = set([domain.lower() for domain in self.inventory_cxn.cursor_to_set()
                                                 if domain])
        print_message(
            "Retrieved {} domains from the geo domains whitelist cache.".format(len(self.whitelist_root_domains_cache)))
        self.inventory_cxn.execute_select("SELECT domain FROM geo_domains_whitelist_cache WHERE level = 'subdomain'")
        self.whitelist_subdomains_cache = set([domain.lower() for domain in self.inventory_cxn.cursor_to_set()
                                               if domain])
        print_message(
            "Retrieved {} domains from the geo subdomains whitelist cache.".format(
                len(self.whitelist_subdomains_cache)))
        return

    def update_inventory_with_caches(self):
        """
        Updates the blacklist and whitelist caches in the contact inventory DB with new values

        Returns:
            None
        """

        statement = "INSERT INTO geo_domains_whitelist_cache (domain, level, insert_date) VALUES (%s, %s, curdate())"
        value_list = []
        for domain in self.whitelist_root_domains_cache_to_add:
            value_list.append((domain, u'domain'))
        for subdomain in self.whitelist_subdomains_cache_to_add:
            value_list.append((subdomain, u'subdomain'))
        if value_list:
            self.inventory_cxn.execute_update_many(statement, value_list)
        statement = "INSERT INTO geo_domains_blacklist_cache (domain, level, insert_date) VALUES (%s, %s, curdate())"
        value_list = []
        for domain in self.blacklist_root_domains_cache_to_add:
            value_list.append((domain, u'domain'))
        for subdomain in self.blacklist_subdomains_cache_to_add:
            value_list.append((subdomain, u'subdomain'))
        if value_list:
            self.inventory_cxn.execute_update_many(statement, value_list)
        return

    def build_blacklist_tlds_dict(self):
        """
        Builds the blacklist_tlds_dict from the contact inventory DB connection

        Returns:
            None
        """

        self.inventory_cxn.execute_select("SELECT tld, tld_type FROM tld_classification")
        tld_dict = self.inventory_cxn.cursor_to_dict_by_headers()
        if not tld_dict:
            self.tld_type_dict = {}
        else:
            for tld, tld_type in zip(tld_dict[u'tld'], tld_dict[u'tld_type']):
                self.tld_type_dict[tld.lower()] = tld_type.lower()
        print_message("Retrieved {} tld types from the cache.".format(len(self.tld_type_dict.keys())))
        return

    def build_locale_information_dict(self):
        """
        Builds the locale_type_dict from the contact inventory DB connection

        Returns:
            None
        """

        self.inventory_cxn.execute_select("SELECT locale, locale_type FROM locale_classification")
        locale_dict = self.inventory_cxn.cursor_to_dict_by_headers()
        if not locale_dict:
            self.locale_type_dict = {}
        else:
            for locale, locale_type in zip(locale_dict[u'locale'], locale_dict[u'locale_type']):
                self.locale_type_dict[locale.lower()] = locale_type.lower()
        print_message("Retrieved {} locale types from the cache.".format(len(self.locale_type_dict.keys())))
        return

    def check_contacts_list_ownership(self):
        """
        Creates a pool of threads to check contact ownership information and maps it to the contact list

        Returns:
            The object's contacts list attribute
        """

        print_message("Loading geo blacklist and whitelist caches...")
        self.connect_inventory_cxn()
        self.build_geo_caches()
        self.build_blacklist_tlds_dict()
        self.build_locale_information_dict()
        print_message("Geo blacklist and whitelist caches loaded! Now checking contact ownership info. "
                      "For large mailers, this may take several minutes to complete. Please wait...")
        pool = ThreadPool(8)
        pool.map(self.check_contact_ownership, self.contacts_list)
        # for contact in self.contacts_list:
        #    self.check_contact_ownership(contact)
        print_message("Contact ownership info retrieved! Now updating new geo blacklist and whitelist caches...")
        self.update_inventory_with_caches()
        self.close_inventory_cxn()
        print_message("Geo blacklist and whitelist caches updated!")
        self.separate_contacts_list()
        return self.kept_contacts, self.cut_contacts

    def separate_contacts_list(self):
        """
        Separates the contacts list into a keep list and cut list

        Returns:
            None
        """

        for contact in self.contacts_list:
            if contact.contact_action == u'Cut':
                contact.contact_action = u'Cut - Geo Check'
                self.cut_contacts.append(contact)
            else:
                self.kept_contacts.append(contact)
        return

    def apply_cached_labels_to_entry_domain_fields(self, entry, cache_type):
        """
        Applies the cached labels to entry attributes dependent on domain

        Args:
            entry : either a DomainEntry or EmailEntry object
            cache_type (unicode) : the cache used to apply the rule

        Returns:
            None
        """

        entry.whois_entry.registrar_country = cache_type
        entry.whois_entry.admin_country = cache_type
        entry.whois_entry.tech_country = cache_type
        entry.whois_entry.registrar_locale = cache_type
        entry.whois_entry.tech_locale = cache_type
        entry.whois_entry.admin_locale = cache_type
        entry.tld_entry.tld_type = cache_type
        return

    def apply_cached_labels_to_entry_subdomain_fields(self, entry, cache_type):
        """
        Applies the cached labels to entry attributes dependent on subdomain

        Args:
            entry : either a DomainEntry or EmailEntry object
            cache_type (unicode) : the cache used to apply the rule

        Returns:
            None
        """

        entry.ip_geo_entry.country_name = cache_type
        entry.ip_geo_entry.locale = cache_type
        return

    def apply_cached_rule_to_entry(self, entry, action, reason):
        """
        Updates a DomainEntry or EmailEntry object with data based on cached rules

        Args:
            entry : either a DomainEntry or EmailEntry object
            action (unicode) : the action to be applied to the entry
            reason (unicode) : the reason to be applied to the entry

        Returns:
            None
        """

        entry.action = action
        entry.cache = reason
        return

    def check_contact_in_whitelist(self, contact):
        """
        Checks to see if a contact's source_entry domain  and subdomain are in the whitelist caches

        Returns:
            None
        """

        if contact.source_entry.domain and contact.source_entry.domain in self.whitelist_root_domains_cache:
            contact.source_entry.whitelisted_domain = True
            self.apply_cached_labels_to_entry_domain_fields(contact.source_entry, u'Domain Whitelist Cache')
        if contact.source_entry.subdomain and contact.source_entry.subdomain in self.whitelist_subdomains_cache:
            contact.source_entry.whitelisted_subdomain = True
            self.apply_cached_labels_to_entry_subdomain_fields(contact.source_entry, u'Subdomain Whitelist Cache')
        if contact.email_entry.domain and contact.email_entry.domain in self.whitelist_root_domains_cache:
            contact.email_entry.whitelisted_domain = True
            self.apply_cached_labels_to_entry_domain_fields(contact.email_entry, u'Domain Whitelist Cache')
        if contact.email_entry.subdomain and contact.email_entry.subdomain in self.whitelist_subdomains_cache:
            contact.email_entry.whitelisted_subdomain = True
            self.apply_cached_labels_to_entry_subdomain_fields(contact.email_entry, u'Subdomain Whitelist Cache')
        return

    def check_contact_in_blacklist(self, contact):
        """
        Checks to see if a contact's source_entry domain and subdomain are in the blacklist caches

        Returns:
            None
        """

        if contact.source_entry.domain and contact.source_entry.domain in self.blacklist_root_domains_cache:
            contact.source_entry.blacklisted_domain = True
            self.apply_cached_labels_to_entry_domain_fields(contact.source_entry, u'Domain Blacklist Cache')
        if contact.source_entry.subdomain and contact.source_entry.subdomain in self.blacklist_subdomains_cache:
            contact.source_entry.blacklisted_subdomain = True
            self.apply_cached_labels_to_entry_subdomain_fields(contact.source_entry, u'Subdomain Blacklist Cache')
        if contact.email_entry.domain and contact.email_entry.domain in self.blacklist_root_domains_cache:
            contact.email_entry.blacklisted_domain = True
            self.apply_cached_labels_to_entry_domain_fields(contact.email_entry, u'Domain Blacklist Cache')
        if contact.email_entry.subdomain and contact.email_entry.subdomain in self.blacklist_subdomains_cache:
            contact.email_entry.blacklisted_subdomain = True
            self.apply_cached_labels_to_entry_subdomain_fields(contact.email_entry, u'Subdomain Blacklist Cache')
        return

    def check_contact_tld(self, contact):
        """
        Determines the tld and tld_type of a contact's source_entry domain and email_entry domain

        Returns:
            None
        """

        for entry in [contact.email_entry, contact.source_entry]:
            domain = entry.domain
            try:
                tld = tldextract.extract(domain).suffix.lower()
            except:  # write the correct exception?
                tld = u'Unable to parse TLD from domain'
            entry.tld_entry.tld = tld
            if tld in self.tld_type_dict:
                entry.tld_entry.tld_type = self.tld_type_dict[tld]
            else:
                entry.tld_entry.tld_type = u'Unknown'
        return

    def check_contact_whois_xml_api(self, contact):
        """
        Checks the WHOIS registration information for a contacts source_entry and email_entry domains via WhoisXML API

        Returns:
            None
        """

        for entry in [contact.email_entry, contact.source_entry]:
            if entry.whitelisted_domain:
                continue
            domain = entry.domain
            xml_handler = whoisxml_connection.WhoisXMLHandler('doug@abuvmedia.com', 'AbuvMedia8')
            try:
                domain = set_unicode_to_string(domain)
                xml_handler.run(domain)
            except Exception as e:  # better except statement needed
                print_message("WHOIS lookup error: {}, {}".format(domain, e.args))
            try:
                r_country = entry.whois_entry.registrar_country = \
                    set_string_to_unicode(xml_handler.get_country('registrant').lower())
            except:  # better except statement needed
                r_country = entry.whois_entry.registrar_country = u''
            try:
                entry.whois_entry.registrar_locale = self.locale_type_dict[r_country]
            except:  # better except statement needed
                entry.whois_entry.registrar_locale = u'unknown'
            try:
                a_country = entry.whois_entry.admin_country = \
                    set_string_to_unicode(xml_handler.get_country('administrativeContact').lower())
            except:  # better except statement needed
                a_country = entry.whois_entry.admin_country = u''
            try:
                entry.whois_entry.admin_locale = self.locale_type_dict[a_country]
            except:  # better except statement needed
                entry.whois_entry.admin_locale = u'unknown'
            try:
                t_country = entry.whois_entry.tech_country = \
                    set_string_to_unicode(xml_handler.get_country('technicalContact').lower())
            except:  # better except statement needed
                t_country = entry.whois_entry.tech_country = u''
            try:
                entry.whois_entry.tech_locale = self.locale_type_dict[t_country]
            except:  # better except statement needed
                entry.whois_entry.tech_locale = u'unknown'
        return

    # this could be better written if we know what except is thrown when country name lookup fails by combining
    # all exceptions
    def check_contact_geolocation(self, contact):
        """
        Checks the geolocation information for a contact's source_entry domain via Maxmind API

        Returns:
            None
        """

        for entry in [contact.email_entry, contact.source_entry]:
            if entry.whitelisted_subdomain:
                continue
            self.get_ip_address_for_entry(entry)
            ip_address = entry.ip_geo_entry.ip_address
            if not ip_address:
                entry.ip_geo_entry.country_name = u'unknown'
                entry.ip_geo_entry.locale = u'unknown'
                continue
            maxmind_client = geoip2.webservice.Client(107734, 'UN0ERXzi0Prb')
            try:
                ip_geo_result = maxmind_client.country(ip_address)
            except geoip2.webservice.AddressNotFoundError:
                print_message("Address {} not found! Maxmind".format(ip_address))
                entry.ip_geo_entry.country_name = u'unknown'
                entry.ip_geo_entry.locale = u'unknown'
                continue
            except geoip2.webservice.InvalidRequestError:
                print_message("Invalid request! Maxmind")
                entry.ip_geo_entry.country_name = u'unknown'
                entry.ip_geo_entry.locale = u'unknown'
                continue
            except geoip2.webservice.OutOfQueriesError:
                print_message("Out of queries! Maxmind")
                entry.ip_geo_entry.country_name = u'unknown'
                entry.ip_geo_entry.locale = u'unknown'
                continue
            except geoip2.webservice.AuthenticationError:
                print_message("Authentication error! Maxmind")
                entry.ip_geo_entry.country_name = u'unknown'
                entry.ip_geo_entry.locale = u'unknown'
                continue
            except geoip2.webservice.HTTPError:
                print_message("HTTP Error! Maxmind")
                entry.ip_geo_entry.country_name = u'unknown'
                entry.ip_geo_entry.locale = u'unknown'
                continue
            except geoip2.webservice.GeoIP2Error:
                print_message("GeoIP2Error! Maxmind")
                entry.ip_geo_entry.country_name = u'unknown'
                entry.ip_geo_entry.locale = u'unknown'
                continue
            try:
                country = entry.ip_geo_entry.country_name = ip_geo_result.country.name.lower()
            except:  # better except statement needed
                country = entry.ip_geo_entry.country_name = u'unknown'
            try:
                entry.ip_geo_entry.locale = self.locale_type_dict[country]
            except KeyError:
                entry.ip_geo_entry.locale = u'unknown'
        return

    def get_ip_address_for_entry(self, entry):
        """
        Uses the socket built-in module to get the IP address for a domain

        Args:
            entry : either a DomainEntry or EmailEntry type object

        Returns:
            None
        """

        subdomain = set_unicode_to_string(entry.subdomain)
        try:
            ip_address = socket.gethostbyname(subdomain)
            entry.ip_geo_entry.ip_address = set_string_to_unicode(ip_address)
        except socket.gaierror:
            entry.ip_geo_entry.ip_address = None
        except socket.error as error:
            if error.errno == errno.WSAECONNRESET:
                try:
                    ip_address = socket.gethostbyname(subdomain)
                    entry.ip_geo_entry.ip_address = set_string_to_unicode(ip_address)
                except:  # needs better except condition
                    entry.ip_geo_entry.ip_address = None
            else:
                entry.ip_geo_entry.ip_address = None
        except:  # needs better except condition
            entry.ip_geo_entry.ip_address = None
        return

    def fill_in_empty_lookup_fields(self, contact):
        """
        Fills in empty contact attribute fields with "Not Checked Due to Results from Cache"

        Returns:
            None
        """

        cached_statement = u'Not Checked Due to Results from Cache'
        if not contact.source_entry.whois_entry.registrar_country:
            contact.source_entry.whois_entry.registrar_country = cached_statement
        if not contact.source_entry.whois_entry.admin_country:
            contact.source_entry.whois_entry.admin_country = cached_statement
        if not contact.source_entry.whois_entry.tech_country:
            contact.source_entry.whois_entry.tech_country = cached_statement
        if not contact.source_entry.ip_geo_entry.country_name:
            contact.source_entry.ip_geo_entry.country_name = cached_statement
        if not contact.source_entry.whois_entry.registrar_locale:
            contact.source_entry.whois_entry.registrar_locale = cached_statement
        if not contact.source_entry.whois_entry.admin_locale:
            contact.source_entry.whois_entry.admin_locale = cached_statement
        if not contact.source_entry.whois_entry.tech_locale:
            contact.source_entry.whois_entry.tech_locale = cached_statement
        if not contact.source_entry.ip_geo_entry.locale:
            contact.source_entry.ip_geo_entry.locale = cached_statement
        if not contact.source_entry.tld_entry.tld_type:
            contact.source_entry.tld_entry.tld_type = cached_statement
        if not contact.email_entry.whois_entry.registrar_country:
            contact.email_entry.whois_entry.registrar_country = cached_statement
        if not contact.email_entry.whois_entry.admin_country:
            contact.email_entry.whois_entry.admin_country = cached_statement
        if not contact.email_entry.whois_entry.tech_country:
            contact.email_entry.whois_entry.tech_country = cached_statement
        if not contact.email_entry.ip_geo_entry.country_name:
            contact.email_entry.ip_geo_entry.country_name = cached_statement
        if not contact.email_entry.whois_entry.registrar_locale:
            contact.email_entry.whois_entry.registrar_locale = cached_statement
        if not contact.email_entry.whois_entry.admin_locale:
            contact.email_entry.whois_entry.admin_locale = cached_statement
        if not contact.email_entry.whois_entry.tech_locale:
            contact.email_entry.whois_entry.tech_locale = cached_statement
        if not contact.email_entry.ip_geo_entry.locale:
            contact.email_entry.ip_geo_entry.locale = cached_statement
        if not contact.email_entry.tld_entry.tld_type:
            contact.email_entry.tld_entry.tld_type = cached_statement
        if not contact.source_entry.action:
            contact.source_entry.action = cached_statement
        if not contact.source_entry.cache:
            contact.source_entry.cache = cached_statement
        if not contact.email_entry.action:
            contact.email_entry.action = cached_statement
        if not contact.email_entry.cache:
            contact.email_entry.cache = cached_statement
        return

    def check_contact_ownership(self, contact):
        """
        Checks the contact's ownership information and determines the correct action and cache rule

        Returns:
            None
        """

        self.check_contact_in_blacklist(contact)
        self.check_contact_in_whitelist(contact)
        cached_results = []
        for entry in [contact.email_entry, contact.source_entry]:
            if entry.blacklisted_domain or entry.blacklisted_subdomain:
                self.apply_cached_rule_to_entry(entry, u'Cut', u'Blacklist')
                cached_results.append(u'Cut')
            if entry.whitelisted_domain and entry.whitelisted_subdomain:
                self.apply_cached_rule_to_entry(entry, u'Keep', u'Whitelist')
                cached_results.append(u'Keep')
        if u'Cut' in cached_results:
            contact.contact_action = u'Cut'
            self.fill_in_empty_lookup_fields(contact)
            return
        elif cached_results == [u'Keep', u'Keep']:
            contact.contact_action = u'Keep'
            self.fill_in_empty_lookup_fields(contact)
            return
        self.check_contact_tld(contact)
        self.check_contact_whois_xml_api(contact)
        self.check_contact_geolocation(contact)
        self.determine_action_for_contact(contact)
        self.determine_cache_rule_for_contact(contact)
        return

    def determine_action_for_contact(self, contact):
        """
        Determines the action for a contact based on its locale results

        Returns:
            None
        """

        entry_actions = set()
        for entry in [contact.email_entry, contact.source_entry]:
            if entry.action:
                entry_actions.add(entry.action)
                continue
            tld_type = entry.tld_entry.tld_type.lower()
            tld = entry.tld_entry.tld.lower()
            r_locale = entry.whois_entry.registrar_locale.lower()
            a_locale = entry.whois_entry.admin_locale.lower()
            t_locale = entry.whois_entry.tech_locale.lower()
            i_locale = entry.ip_geo_entry.locale.lower()
            if r_locale == u'international':
                action = u'Cut'
            elif a_locale == u'international':
                action = u'Cut'
            elif t_locale == u'international':
                action = u'Cut'
            elif i_locale == u'international':
                action = u'Cut'
            elif tld_type == u'blacklist':
                action = u'Cut'
            elif tld == u'gov':
                action = u'Keep'
            elif i_locale == u'unknown':
                action = u'Cut'
            elif r_locale == a_locale == t_locale == u'unknown':
                action = u'Cut'
            else:
                action = u'Keep'
            entry.action = action
            entry_actions.add(action)
        if not contact.contact_action:
            if u'Cut' in entry_actions:
                contact.contact_action = u'Cut'
            else:
                contact.contact_action = u'Keep'
        return

    def determine_cache_rule_for_contact(self, contact):
        """
        Determines the cache rule for a contact based on its locale rules

        Returns:
            None
        """

        for entry in [contact.email_entry, contact.source_entry]:
            if entry.cache:
                continue
            tld_type = entry.tld_entry.tld_type.lower()
            r_locale = entry.whois_entry.registrar_locale.lower()
            a_locale = entry.whois_entry.admin_locale.lower()
            t_locale = entry.whois_entry.tech_locale.lower()
            i_locale = entry.ip_geo_entry.locale.lower()
            if r_locale == u'international':
                cache = u'Blacklist'
                self.blacklist_root_domains_cache_to_add.add(entry.domain)
            elif a_locale == u'international':
                cache = u'Blacklist'
                self.blacklist_root_domains_cache_to_add.add(entry.domain)
            elif t_locale == u'international':
                cache = u'Blacklist'
                self.blacklist_root_domains_cache_to_add.add(entry.domain)
            elif i_locale == u'international':
                cache = u'Blacklist'
                self.blacklist_subdomains_cache_to_add.add(entry.subdomain)
            elif tld_type == u'blacklist':
                cache = u'Blacklist'
                self.blacklist_root_domains_cache_to_add.add(entry.domain)
            elif r_locale == a_locale == t_locale == i_locale == u'unknown':
                cache = u'None'
            elif tld_type == u'not blacklist' and not (r_locale == a_locale == t_locale == u'unknown'):
                cache = u'2wks'
                self.whitelist_root_domains_cache_to_add.add(entry.domain)
                self.whitelist_subdomains_cache_to_add.add(entry.subdomain)
            else:
                cache = u'None'
            entry.cache = cache
        return


class DomainSendsLimiter(object):
    """
    A class used to ensure that no more than three contacts from the same domain are kept

    Attributes:
        unlimited_contacts (list) : a list of contacts that needs to be checked against limit rules
        limited_contacts (list) : a list of contacts that have been checked against limit rules
        extra_contacts (list) : a list of contacts that exceed the business rules limit
        limit (int) : the maximum number of contacts per domain that can be sent
        domain_contacts_count (dict) : a dict tracking how many contacts have been seen per domain
    """

    def __init__(self, unlimited_contacts, limit=3):
        """
        Initializes the object

        Args:
            unlimited_contacts (list) : a list of contacts that need to be run through limiting rules
            limit (int) : the maximum number of contacts per domain that can be sent
        """

        self.unlimited_contacts = unlimited_contacts
        self.limited_contacts = []
        self.extra_contacts = []
        self.limit = limit
        self.domain_contacts_count = {}

    def limit_contacts(self):
        """
        Applies the limiting rule to the unlimited_contacts list

        Returns:
            A list of limited contacts and extra contaccts
        """

        for contact in self.unlimited_contacts:
            domain = contact.source_entry.domain
            if domain not in self.domain_contacts_count:
                self.domain_contacts_count[domain] = 0
            self.domain_contacts_count[domain] += 1
            if self.domain_contacts_count[domain] > self.limit:
                contact.contact_action = u'Already have {} emails to domain'.format(self.limit)
                self.extra_contacts.append(contact)
            else:
                self.limited_contacts.append(contact)
        return self.limited_contacts, self.extra_contacts


class CompDomainSendsLimiter(DomainSendsLimiter):
    def limit_contacts(self):
        self.sort_contacts_by_prev_send()
        return super(CompDomainSendsLimiter, self).limit_contacts()

    def sort_contacts_by_prev_send(self):
        self.unlimited_contacts.sort(key=lambda contact: contact.previously_contacted)
        return


class EmailValidator(object):
    """
    A class used to validate emails in contacts via the Kickbox.io API

    Attributes:
        contacts_list (list) : a list of Contact objects to have emails validated
        kickbox_api_key (str) : the key to the Kickbox.io API for the account to use
        kickbox_cxn (kickbox.Client) : a Kickbox API client
        email_cache (dict) : a dict containing the Kickbox email cache results
        email_cache_to_add (list) : a list of email cache data to add to the inventory DB
        inventory_cxn (DBConnection) : a connection to the contact inventory DB
        cut_contacts (list) : a list of contacts to cut
        kept_contacts (list) a list of contacts to keep

    """

    def __init__(self, contacts_list):
        """
        Initializes an EmailValidator object

        Args:
            contacts_list (list) : a list of Contact objects to have emails validated
        """

        self.contacts_list = contacts_list
        self.kickbox_api_key = '3e7f950cc61833203c66772bde6504a7ae8aaad74ca635898152c91dd200664c'
        self.kickbox_cxn = None
        self.email_cache = {}
        self.email_cache_to_add = []
        self.inventory_cxn = None
        self.cut_contacts = []
        self.kept_contacts = []

    def connect_inventory_cxn(self):
        """
        Sets up a connection to the contact inventory database

        Returns:
            None
        """

        self.inventory_cxn = DBConnection('root', 'vagrant', '127.0.0.1', 'contact_inventory')
        self.inventory_cxn.connect()
        return

    def close_inventory_cxn(self):
        """
        Closes the inventory connection

        Returns:
            None
        """

        self.inventory_cxn.close()
        return

    def instantiate_kickbox_client(self):
        """
        Connects to the Kickbox.io API

        Returns:
            None
        """

        client = kickbox.Client(self.kickbox_api_key)
        self.kickbox_cxn = client.kickbox()
        return

    def validate_all_emails(self):
        """
        Sets up multi-threaded pool to validate emails in Kickbox.io API via simultaneous connections

        Returns:
            The object's contacts_list attribute
        """

        print_message("Connecting to Kickbox.io API...")
        self.connect_inventory_cxn()
        self.instantiate_kickbox_client()
        print_message("Successfully connected to Kickbox.io API. Now loading email cache...")
        self.load_email_cache()
        print_message("Email cache loaded successfully. Now validating emails. For large mailers, this make "
                      "several minutes to complete. Please wait...")
        pool = ThreadPool(8)
        pool.map(self.validate_email, self.contacts_list)
        print_message("Emails validated. Now updating email cache with new results...")
        self.update_email_cache()
        self.close_inventory_cxn()
        print_message("Email cache updated.")
        self.separate_contacts_list()
        return self.kept_contacts, self.cut_contacts

    def separate_contacts_list(self):
        """
        Separates the contacts list into a keep list and cut list

        Returns:
            None
        """

        for contact in self.contacts_list:
            if contact.contact_action == u'Cut':
                contact.contact_action = u'Cut - Email Validation'
                self.cut_contacts.append(contact)
            else:
                self.kept_contacts.append(contact)
        return

    def validate_email(self, contact, timeout=10000):
        """
        Validates and updates the results for an EmailEntry in a Contact via the Kickbox.io API

        Args:
            contact (Contact) : a Contact object to have its EmailEntry email validated
            timeout (int) : time in milliseconds to wait for a response from the Kickbox.io API

        Returns:
            None
        """

        email = contact.email_entry.email

        if contact.contact_action == u'Cut':
            contact.email_entry.result = u'Not Checked. Cut during Geo Check'
            contact.email_entry.reason = u'Not Checked. Cut during Geo Check'
            contact.email_entry.sendex = u'Not Checked. Cut during Geo Check'
            contact.email_entry.kickbox_action = u'Not Checked. Cut during Geo Check'
            contact.email_entry.source = u'Not Checked. Cut during Geo Check'
        elif email in self.email_cache:
            contact.email_entry.result = self.email_cache[email][u'result']
            contact.email_entry.reason = self.email_cache[email][u'reason']
            contact.email_entry.sendex = self.email_cache[email][u'sendex']
            contact.email_entry.kickbox_action = self.email_cache[email][u'action']
            contact.contact_action = contact.email_entry.kickbox_action
            contact.email_entry.source = u'Cache'
        else:
            contact.email_entry.source = u'Lookup'
            email = set_unicode_to_string(email)
            results = self.kickbox_cxn.verify(email, {'timeout': timeout})
            contact.email_entry.result = set_string_to_unicode(results.body['result'])
            contact.email_entry.reason = set_string_to_unicode(results.body['reason'])
            contact.email_entry.sendex = set_string_to_unicode(results.body['sendex'])
            self.get_action_for_result(contact)
            if contact.email_entry.result != u'unknown':
                self.email_cache_to_add.append(contact)
        return

    def load_email_cache(self):
        """
        Loads the email cache from the contact inventory DB

        Returns:
            None
        """

        # self.inventory_cxn.execute_update("DELETE FROM kickbox_email_cache "
        #                                  "WHERE datediff(insert_date, curdate()) < -150")
        self.inventory_cxn.execute_select("SELECT email, result, reason, sendex, action "
                                          "FROM kickbox_email_cache")
        self.email_cache = self.inventory_cxn.cursor_to_dict_by_first_column()
        print_message("Retrieved {} email results from the Kickbox cache.".format(len(self.email_cache.keys())))
        return

    def update_email_cache(self):
        """
        Updates the email cache in the contact inventory DB with new Kickbox.io query results

        Returns:
            None
        """

        statement = "INSERT INTO kickbox_email_cache (email, result, reason, sendex, action, insert_date) " \
                    "VALUES (%s, %s, %s, %s, %s, curdate())"
        value_list = []
        for contact in self.email_cache_to_add:
            email = contact.email_entry.email
            result = contact.email_entry.result
            reason = contact.email_entry.reason
            sendex = contact.email_entry.sendex
            action = contact.email_entry.kickbox_action
            value_list.append((email, result, reason, sendex, action))
        if value_list:
            self.inventory_cxn.execute_update_many(statement, value_list)
        return

    def get_action_for_result(self, contact):
        """
        Determines the action for a particular type of email result

        Args:
            contact (Contact)

        Returns:
            None
        """

        if contact.email_entry.result == u'unknown' or contact.email_entry.result == u'undeliverable':
            contact.email_entry.kickbox_action = u'Cut'
            contact.contact_action = u'Cut'
        else:
            contact.email_entry.kickbox_action = u'Keep'
            contact.contact_action = u'Keep'
        return


class ImpressionwiseScreener(object):
    """
    A class used to validate emails in contacts via the Impressionwise API

    Attributes:
        unscreened_contacts (list) : a list of Contact objects to be checked against Impressionwise API
        api_base_url (str) : base URL for Impressionwise API, to be combined with params for each run
        api_base_params (dict) : a dict containing the parameters to be included in every API call
        cache (dict) : a dict to be populated with the runtime cached API results
        add_to_cache (list) : an empty list to be filled with contact objects which need to be added to DB cache
        cut_contacts (list) : an empty list to be filled with contacts that fail one or more Impressionwise rules
        keep_contacts (list) : an empty list to be filled with contacts that pass all Impressionwise rules
        inventory_cxn (DBConnection) : a DBConnection object initialized with standard contact_inventory credentials
        current_date (datetime) : date at runtime, formated for proper MySQL database insertion
    """

    def __init__(self, unscreened_contacts):
        """
        Initializes an ImpressionwiseScreener object

        Args:
            unscreened_contacts (list) : a list of Contact objects to be checked against Impressionwise API
        """
        self.unscreened_contacts = unscreened_contacts
        self.api_base_url = 'http://post.impressionwise.com/fastfeed.aspx'
        self.api_base_params = {'code': 'A53001', 'pwd': 'a8uVm7'}
        self.cache = {}
        self.add_to_cache = []
        self.cut_contacts = []
        self.keep_contacts = []
        self.inventory_cxn = DBConnection('root', 'vagrant', '127.0.0.1', 'contact_inventory')
        self.current_date = datetime.date.today().strftime('%Y-%m-%d')

    def api_query(self, contact):
        """Sends a single request to Impressionwise API and calls separate method to parse results"""
        payload = dict(self.api_base_params)
        payload['email'] = contact.email_entry.email
        r = requests.get(self.api_base_url, params=payload)
        self.parse_query_result(contact, r.content)

    def parse_query_result(self, contact, query_result):
        """Parse results from Impressionwise API single call. Method called by ImpressionwiseScreener.api_query()"""
        standardized_query_result = set_string_to_unicode(query_result.lower().strip())
        results_dict = {kv[0]: kv[1] for kv in [pair.split('=') for pair in standardized_query_result.split('&')]}
        for k, v in results_dict.iteritems():  # need to reset some values to None/Null to store correctly in DB
            if not results_dict[k]:
                results_dict[k] = None
            if results_dict[k] == 'na' and k != 'npd':
                results_dict[k] = None
        contact.email_entry.iw_result = results_dict['result']
        contact.email_entry.iw_cc = results_dict['cc']
        contact.email_entry.iw_npd = results_dict['npd']
        contact.email_entry.iw_ttp = results_dict['ttp']
        self.get_iw_action_for_result(contact)

    def get_iw_action_for_result(self, contact):
        """Applied business rules to parsed API results"""
        if contact.email_entry.iw_result in [u'certdom', u'netprotect']:
            contact.email_entry.iw_action = u'Keep'
            contact.email_entry.iw_cache_expiration = (datetime.date.today() + datetime.timedelta(days=30)).strftime(
                '%Y-%m-%d')
            # can build in deeper functionality here if we want to handle various NetProtects differently
        elif contact.email_entry.iw_result in [u'certint', u'key', u'quarantine', u'parked', u'seed', u'invalid',
                                               u'mole', u'trap']:
            contact.email_entry.iw_action = u'Cut'
            contact.email_entry.iw_cache_expiration = (datetime.date.today() + datetime.timedelta(days=180)).strftime(
                '%Y-%m-%d')
            if contact.email_entry.iw_result == u'quarantine':
                contact.email_entry.iw_cache_is_active = False
                contact.email_entry.iw_cache_expiration = self.current_date
        else:
            if contact.email_entry.iw_result == u'retry':
                # handle retries for timeouts
                pass
            elif contact.email_entry.iw_result == u'wrong_psw':
                # handle exit for incorrect password
                pass
        self.add_to_cache.append(contact)

    def retrieve_cache(self):
        """Retrieves cached API results from MySQL database, generates a dict of results and assigns it to self.cache"""
        self.inventory_cxn.execute_select('''
            SELECT
                IWCacheEmail,
                IWCacheResult,
                IWCacheNetProtectDetail,
                IWCacheCountryCode,
                IWCacheTimeToProcess,
                IWCacheAction
            FROM
                impressionwise_cache
            WHERE
                IWCacheIsActive = True
            ''')
        self.cache = self.inventory_cxn.cursor_to_dict_by_first_column()

    def process_all_contacts(self):
        """Class's driving method. Calls all necessary methods to run complete list of contacts against API"""
        self.inventory_cxn.connect()
        self.remove_expired_results_from_cache()
        print_message('Retrieving Impressionwise cache...')
        self.retrieve_cache()
        print_message('Cache retrieved! Screening contacts. This can take a while...')
        for contact in self.unscreened_contacts:
            email = contact.email_entry.email
            if email in self.cache:
                contact.email_entry.iw_result = set_string_to_unicode(self.cache[email]['IWCacheResult'])
                contact.email_entry.iw_npd = set_string_to_unicode(self.cache[email]['IWCacheNetProtectDetail'])
                contact.email_entry.iw_cc = set_string_to_unicode(self.cache[email]['IWCacheCountryCode'])
                contact.email_entry.iw_ttp = set_string_to_unicode(self.cache[email]['IWCacheTimeToProcess'])
                contact.email_entry.iw_action = set_string_to_unicode(self.cache[email]['IWCacheAction'])
                contact.email_entry.iw_source = u'Cache'
            else:
                self.api_query(contact)
            if contact.email_entry.iw_action == u'Keep':
                self.keep_contacts.append(contact)
            else:
                contact.contact_action = u'Cut - Impressionwise: {}'.format(contact.email_entry.iw_result)
                self.cut_contacts.append(contact)
        self.update_db()
        print_message('All contacts checked. Adding {} records to Impressionwise cache.'.format(len(self.add_to_cache)))
        self.inventory_cxn.close()
        return self.keep_contacts, self.cut_contacts

    def remove_expired_results_from_cache(self):
        """Sets the IsActive flag to False for cached API results that have expired"""
        self.inventory_cxn.execute_update('''
            UPDATE impressionwise_cache
            SET
                IWCacheIsActive = False
            WHERE
                IWCacheIsActive = True and
                datediff(curdate(), IWCacheExpirationDate) >= 0''')

    def update_db(self):
        """Generates a list of argument tuples to be used to insert all new API results into cache"""
        base_update = '''
            INSERT INTO impressionwise_cache
                (IWCacheEmail,
                IWCacheResult,
                IWCacheNetProtectDetail,
                IWCacheCountryCode,
                IWCacheTimeToProcess,
                IWCacheAction,
                IWCacheIsActive,
                IWCacheInsertDate,
                IWCacheExpirationDate)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)'''
        args_list = []
        for record in self.add_to_cache:
            args_list.append((record.email_entry.email, record.email_entry.iw_result, record.email_entry.iw_npd,
                              record.email_entry.iw_cc, record.email_entry.iw_ttp, record.email_entry.iw_action,
                              record.email_entry.iw_cache_is_active, self.current_date,
                              record.email_entry.iw_cache_expiration))
        self.inventory_cxn.execute_update_many(base_update, args_list)


class ManualSendScreener(object):  # create an abstract "Screener" class and
    # have this and blacklist screener be a subclass
    """
    A class that checks the domain and email within a contact to see if it needs to be a high-value manual send

    Attributes:
        high_value_domains_set (set) : a set of high value domains
        high_value_urls_set (set) : a set of high value urls
        inventory_cxn (mysql_database_connection.DBConnection) : a database connection
        unscreened_contacts (list) : a list of contacts that need to be screened against the high value list
        high_value_contacts (list) : a list of contacts that have are high value and need to be pulled out of automation
        standard_contacts (list) : a list of contacts that have are standard value
    """

    def __init__(self, unscreened_contacts):
        """
        Initiailzes the object

        unscreened_contacts (list) : a list of contacts that need to be screened against the high value list
        """

        self.unscreened_contacts = unscreened_contacts
        self.inventory_cxn = None
        self.high_value_contacts = []
        self.standard_contacts = []
        self.high_value_domains_set = set()
        self.high_value_urls_set = set()

    def connect_inventory_cxn(self):
        """
        Sets up a connection to the contact inventory database

        Returns:
            None
        """

        self.inventory_cxn = DBConnection('root', 'vagrant', '127.0.0.1', 'contact_inventory')
        self.inventory_cxn.connect()
        return

    def close_inventory_cxn(self):
        """
        Closes the inventory connection

        Returns:
            None
        """

        self.inventory_cxn.close()
        return

    def load_high_value_targets_list(self):
        """
        Loads the blacklist from the contact inventory database table

        Returns:
            None
        """

        self.inventory_cxn.execute_select("SELECT uri, uri_type, effective_date FROM high_value_exclusion")
        for row in self.inventory_cxn.cursor_to_list_of_rows():
            uri, uri_type, effective_date = row
            today_object = datetime.date.today()
            effective_date_object = datetime.datetime.strptime(effective_date, '%Y-%m-%d').date()
            if today_object < effective_date_object:
                continue
            if set_string_to_unicode(uri_type) == u'URL':
                for alternate_url in create_alternate_urls_from_item(uri):
                    self.high_value_urls_set.add(alternate_url.strip().lower())
            elif set_string_to_unicode(uri_type) == u'Domain':
                extract_object = tldextract.extract(uri)
                domain = u'.'.join([extract_object.domain, extract_object.suffix])
                self.high_value_domains_set.add(domain.strip().lower())
        print_message(
            "Loaded {} domains and {} URLs from the high value targets list.".format(len(self.high_value_domains_set),
                                                                                     len(self.high_value_urls_set)))
        return

    def screen_contacts(self):
        """
        Iterates over the unscreened contacts list and checks domain and email entry against blacklist

        Returns:
            The object's accepted_contacts attributes
        """

        self.connect_inventory_cxn()
        print_message("Loading high value targets list.")
        self.load_high_value_targets_list()
        print_message("High value targets list loaded! Now screening emails and domains against it...")
        for contact in self.unscreened_contacts:
            high_value = False
            source_domain = contact.source_entry.domain
            source_subdomain = contact.source_entry.subdomain
            source_url = contact.source_entry.url
            email_domain = contact.email_entry.domain
            email_subdomain = contact.email_entry.subdomain
            if source_url and source_url.strip().lower() in self.high_value_urls_set:
                high_value = True
                contact.contact_action = u'High Value URL'
            if source_domain and source_domain.strip().lower() in self.high_value_domains_set:
                high_value = True
                contact.contact_action = u'High Value Web Domain'
            if source_subdomain and source_subdomain.strip().lower() in self.high_value_domains_set:
                high_value = True
                contact.contact_action = u'High Value Web Domain'
            if email_domain and email_domain.strip().lower() in self.high_value_domains_set:
                high_value = True
                contact.contact_action = u'High Value Email Domain'
            if email_subdomain and email_subdomain.strip().lower() in self.high_value_domains_set:
                high_value = True
                contact.contact_action = u'High Value Email Domain'
            if high_value:
                self.high_value_contacts.append(contact)
            else:
                self.standard_contacts.append(contact)
        self.close_inventory_cxn()
        print_message("High value targets screening complete!")
        return self.standard_contacts, self.high_value_contacts


class OutputWriter(object):
    """
    A class to transform a list of contacts into a contacts dict that can be written by CSVExporter

    Attributes:
        ci_filename (str) : the filename to write the contacts information to for contact_inventory adds (deduped)
        prelim_filename (str) : the filename to write the contacts information to for prelim mailer (unique emails)
        cutfile_filename (str) : the filename to write the cutfile to
        contacts_list (list) : a list of contact objects
        prelim_mailer_headers_dict (dict) : a dict of headers for writing contact objects to CSV for the prelim mailer
        ci_headers_dict (dict) : a dict of headers for writing contact objects to CSV for insertion to inventory
        full_data_dict (dict) : a dict containing containing contact data from contacts_list grouped according to CSV headers
        blacklisted_contacts_list (list) : a list of contacts cut due to blacklist
        invalid_contacts_list (list) : a list of contacts cut due to not passing data cleaning validation rules
        geo_cut_contacts_list (list) : a list of contacts cut due to geo location lookup rules
        email_cut_contacts_list (list) : a list of contacts cut due to email validation rules
        deduplicated_contacts_list (list) : a list of deduplicated contacts based on attribute string method of Contact
        duplicated_contacts_list (list) : a list of duplicate contacts as compared to deduplicated_contacts_list
        unique_email_contacts_list (list) : a list of contacts deduplicated on email addresses
    """

    def __init__(self, contacts_list, blacklisted_contacts_list, geo_cut_contacts_list, email_cut_contacts_list,
                 invalid_contacts_list, high_value_contacts_list, iw_contacts_list):
        """
        Initializes the object

        Args:
            contacts_list (list) : a list of contact objects
            blacklisted_contacts_list (list) : a list of contacts cut due to blacklist
            geo_cut_contacts_list (list) : a list of contacts cut due to geo location lookup rules
            email_cut_contacts_list (list) : a list of contacts cut due to email validation rules
            invalid_contacts_list (list) : a list of contacts cut due to not passing data cleaning validation rules
            high_value_contacts_list (list) : a list of contacts cut due to being high value and requiring manual work
        """

        self.ci_filename = 'collected_contacts_for_inventory.csv'
        self.edu_ci_filename = 'edu_collected_contacts_for_inventory.csv'
        self.prelim_filename = 'preliminary_mailer.csv'
        self.high_value_filename = 'high_value_contacts.csv'
        self.cutfile_filename = 'cutfile.csv'
        self.contacts_list = contacts_list
        self.prelim_mailer_headers_dict = {0: u'contact_email', 1: u'email_subdomain', 2: u'source_subdomain',
                                           3: u'contact_name', 4: u'source_url', 5: u'source_domain', 6: u'target_url',
                                           7: u'target_domain', 8: u'asset', 9: u'campaign', 10: u'email_variant',
                                           11: u'email_notes', 12: u'audiences', 13: u'entity_name', 14: u'entity_type',
                                           15: u'city', 16: u'state', 17: u'entity_website', 18: u'entity_subgroup',
                                           19: u'entity_subgroup_url', 20: u'entity_subgroup_resources_url'}
        self.ci_headers_dict = {0: u'contact_email', 1: u'contact_name', 2: u'source_domain',
                                3: u'source_domain_trust_flow', 4: u'source_domain_topic',
                                5: u'source_domain_topic_trust', 6: u'source_url', 7: u'target_url',
                                8: u'target_domain', 9: u'date_collected', 10: u'type', 11: u'context', 12: u'site',
                                13: u'asset_url', 14: u'content_group', 15: u'series', 16: u'asset', 17: u'audiences',
                                18: u'campaign', 19: u'email_variant', 20: u'email_notes', 21: u'contact_action'}
        self.edu_ci_headers_dict = {0: u'contact_email', 1: u'contact_name', 2: u'source_domain', 3: u'source_url',
                                    4: u'entity_type', 5: u'entity_name', 6: u'entity_subgroup',
                                    7: u'entity_subgroup_url', 8: u'entity_subgroup_resources_url', 9: u'city',
                                    10: u'state', 11: u'county', 12: u'entity_type', 13: u'site', 14: u'series',
                                    15: u'asset', 16: u'campaign', 17: u'variable', 18: u'abuv_url',
                                    19: u'date_collected'}
        self.headers_dict = {0: u'target_url', 1: u'target_domain', 2: u'asset', 3: u'campaign', 4: u'email_variant',
                             5: u'source_url', 6: u'source_domain', 7: u'contact_name', 8: u'contact_email',
                             9: u'email_notes', 10: u'domain_registrar_country', 11: u'domain_admin_country',
                             12: u'domain_tech_country', 13: u'domain_ip_address_country',
                             14: u'domain_registrar_locale', 15: u'domain_admin_locale', 16: u'domain_tech_locale',
                             17: u'domain_ip_geo_locale', 18: u'domain_tld_type', 19: u'email_registrar_country',
                             20: u'email_admin_country', 21: u'email_tech_country', 22: u'email_ip_address_country',
                             23: u'email_registrar_locale', 24: u'email_admin_locale', 25: u'email_tech_locale',
                             26: u'email_ip_geo_locale', 27: u'email_tld_type', 28: u'domain_geo_based_action',
                             29: u'domain_geo_based_cache', 30: u'email_geo_based_action', 31: u'email_geo_based_cache',
                             32: u'kickbox_email_action', 33: u'email_result', 34: u'email_source',
                             35: u'contact_action'}
        self.full_data_dict = {}
        self.deduplicated_contacts_list = []
        self.unique_email_contacts_list = []
        self.blacklisted_contacts_list = blacklisted_contacts_list
        self.geo_cut_contacts_list = geo_cut_contacts_list
        self.email_cut_contacts_list = email_cut_contacts_list
        self.invalid_contacts_list = invalid_contacts_list
        self.high_value_contacts_list = high_value_contacts_list
        self.iw_contacts_list = iw_contacts_list
        self.cutfile_contacts_list = []
        self.duplicated_contacts_list = []

    def customize_outfile_names(self):
        """
        Updates the outfile names based on user input.

        Returns:
            None
        """

        custom_key = raw_input("Please enter the custom name for the outfiles for this mailer.\n")
        self.ci_filename = custom_key + '_' + self.ci_filename
        self.edu_ci_filename = custom_key + '_' + self.edu_ci_filename
        self.prelim_filename = custom_key + '_' + self.prelim_filename
        self.cutfile_filename = custom_key + '_' + self.cutfile_filename
        self.high_value_filename = custom_key + '_' + self.high_value_contacts_list
        return

    def build_empty_full_data_dict(self):
        """
        Replaces any values stored in full_data_dict with a brand new empty dict only containing keys

        Returns:
            None
        """

        self.full_data_dict = {u'target_url': [], u'target_domain': [], u'asset': [], u'campaign': [],
                               u'source_url': [], u'source_domain': [], u'contact_name': [], u'contact_email': [],
                               u'email_notes': [], u'domain_registrar_country': [], u'domain_admin_country': [],
                               u'domain_tech_country': [], u'domain_ip_address_country': [],
                               u'domain_registrar_locale': [], u'domain_admin_locale': [], u'domain_tech_locale': [],
                               u'domain_ip_geo_locale': [], u'domain_tld_type': [], u'email_registrar_country': [],
                               u'email_admin_country': [], u'email_tech_country': [], u'email_ip_address_country': [],
                               u'email_registrar_locale': [], u'email_admin_locale': [], u'email_tech_locale': [],
                               u'email_ip_geo_locale': [], u'email_tld_type': [], u'domain_geo_based_action': [],
                               u'domain_geo_based_cache': [], u'email_geo_based_action': [],
                               u'email_geo_based_cache': [], u'kickbox_email_action': [], u'email_result': [],
                               u'email_source': [], u'contact_action': [], u'email_domain': [], u'email_subdomain': [],
                               u'source_subdomain': [], u'email_variant': [], u'audiences': [],
                               u'source_domain_trust_flow': [], u'source_domain_topic': [],
                               u'source_domain_topic_trust': [], u'date_collected': [], u'type': [], u'context': [],
                               u'site': [], u'asset_url': [], u'content_group': [], u'series': [], u'entity_name': [],
                               u'entity_type': [], u'city': [], u'state': [], u'entity_website': [],
                               u'entity_subgroup': [], u'entity_subgroup_url': [], u'entity_subgroup_resources_url': [],
                               u'subject': [], u'bucket': [], u'var1': [], u'var2': [], u'var3': [], u'var4': [],
                               u'var5': [], u'var6': [], u'var7': [], u'var8': []}
        return

    def contacts_list_to_data_dict(self, contacts_list):
        """
        Adds the attributes for every contact in contacts_list to the full_data_dict for output to csv

        Args:
            contacts_list (list) : a list of Contact objects from which to create data dict

        Returns:
            None
        """

        self.build_empty_full_data_dict()
        for contact in contacts_list:
            self.full_data_dict[u'target_url'].append(contact.target_entry.url)
            self.full_data_dict[u'target_domain'].append(contact.target_entry.domain)
            self.full_data_dict[u'asset'].append(contact.asset_entry.asset)
            self.full_data_dict[u'campaign'].append(contact.asset_entry.campaign)
            self.full_data_dict[u'email_variant'].append(contact.asset_entry.email_variant)
            self.full_data_dict[u'source_url'].append(contact.source_entry.url)
            self.full_data_dict[u'source_domain'].append(contact.source_entry.domain)
            self.full_data_dict[u'contact_name'].append(contact.contact_name)
            self.full_data_dict[u'contact_email'].append(contact.email_entry.email)
            self.full_data_dict[u'domain_registrar_country'].append(contact.source_entry.whois_entry.registrar_country)
            self.full_data_dict[u'domain_admin_country'].append(contact.source_entry.whois_entry.admin_country)
            self.full_data_dict[u'domain_tech_country'].append(contact.source_entry.whois_entry.tech_country)
            self.full_data_dict[u'domain_ip_address_country'].append(contact.source_entry.ip_geo_entry.country_name)
            self.full_data_dict[u'domain_registrar_locale'].append(contact.source_entry.whois_entry.registrar_locale)
            self.full_data_dict[u'domain_admin_locale'].append(contact.source_entry.whois_entry.admin_locale)
            self.full_data_dict[u'domain_tech_locale'].append(contact.source_entry.whois_entry.tech_locale)
            self.full_data_dict[u'domain_ip_geo_locale'].append(contact.source_entry.ip_geo_entry.locale)
            self.full_data_dict[u'domain_tld_type'].append(contact.source_entry.tld_entry.tld_type)
            self.full_data_dict[u'email_registrar_country'].append(contact.email_entry.whois_entry.registrar_country)
            self.full_data_dict[u'email_admin_country'].append(contact.email_entry.whois_entry.admin_country)
            self.full_data_dict[u'email_tech_country'].append(contact.email_entry.whois_entry.tech_country)
            self.full_data_dict[u'email_ip_address_country'].append(contact.email_entry.ip_geo_entry.country_name)
            self.full_data_dict[u'email_registrar_locale'].append(contact.email_entry.whois_entry.registrar_locale)
            self.full_data_dict[u'email_admin_locale'].append(contact.email_entry.whois_entry.admin_locale)
            self.full_data_dict[u'email_tech_locale'].append(contact.email_entry.whois_entry.tech_locale)
            self.full_data_dict[u'email_ip_geo_locale'].append(contact.email_entry.ip_geo_entry.locale)
            self.full_data_dict[u'email_tld_type'].append(contact.email_entry.tld_entry.tld_type)
            self.full_data_dict[u'domain_geo_based_action'].append(contact.source_entry.action)
            self.full_data_dict[u'domain_geo_based_cache'].append(contact.source_entry.cache)
            self.full_data_dict[u'email_geo_based_action'].append(contact.email_entry.action)
            self.full_data_dict[u'email_geo_based_cache'].append(contact.email_entry.cache)
            self.full_data_dict[u'kickbox_email_action'].append(contact.email_entry.kickbox_action)
            self.full_data_dict[u'email_result'].append(contact.email_entry.result)
            self.full_data_dict[u'email_source'].append(contact.email_entry.source)
            self.full_data_dict[u'email_notes'].append(contact.asset_entry.email_notes)
            self.full_data_dict[u'contact_action'].append(contact.contact_action)
            self.full_data_dict[u'email_domain'].append(contact.email_entry.domain)
            self.full_data_dict[u'email_subdomain'].append(contact.email_entry.subdomain)
            self.full_data_dict[u'source_subdomain'].append(contact.source_entry.subdomain)
            self.full_data_dict[u'source_domain_trust_flow'].append(contact.source_entry.trust_flow)
            self.full_data_dict[u'source_domain_topic'].append(contact.source_entry.topic)
            self.full_data_dict[u'source_domain_topic_trust'].append(contact.source_entry.topic_trust_flow)
            self.full_data_dict[u'audiences'].append(contact.asset_entry.audiences)
            self.full_data_dict[u'date_collected'].append(contact.date_collected)
            self.full_data_dict[u'type'].append(contact.contact_type)
            self.full_data_dict[u'site'].append(contact.asset_entry.site)
            self.full_data_dict[u'asset_url'].append(contact.asset_entry.asset_url)
            self.full_data_dict[u'content_group'].append(contact.asset_entry.content_group)
            self.full_data_dict[u'series'].append(contact.asset_entry.series)
            self.full_data_dict[u'context'].append(contact.asset_entry.context)
            self.full_data_dict[u'entity_name'].append(contact.source_entry.entity_name)
            self.full_data_dict[u'entity_type'].append(contact.source_entry.entity_type)
            self.full_data_dict[u'city'].append(contact.source_entry.city)
            self.full_data_dict[u'state'].append(contact.source_entry.state)
            self.full_data_dict[u'entity_website'].append(contact.source_entry.entity_website)
            self.full_data_dict[u'entity_subgroup'].append(contact.source_entry.entity_subgroup)
            self.full_data_dict[u'entity_subgroup_url'].append(contact.source_entry.entity_subgroup_url)
            self.full_data_dict[u'entity_subgroup_resources_url'].append(
                contact.source_entry.entity_subgroup_resources_url)
            for var_num in [u'var' + str(i + 1) for i in range(8)]:
                self.full_data_dict[var_num].append(getattr(contact.send_vars_entry, var_num))
            self.full_data_dict[u'subject'].append(contact.send_vars_entry.subject)
            self.full_data_dict[u'bucket'].append(contact.send_vars_entry.bucket)

        return

    def create_deduplicated_contacts_lists(self):
        """
        Eliminates duplicate contacts from contacts list based on multiple attribute keys and adds uniques to lists

        Returns:
            None
        """

        contact_string_rep_set = set()
        unique_email_set = set()

        for contact in self.contacts_list:
            contact_string_rep = contact.create_string_of_object_attrs()
            if contact_string_rep not in contact_string_rep_set:
                contact_string_rep_set.add(contact_string_rep)
                self.deduplicated_contacts_list.append(contact)
            else:
                contact.contact_action = u'Cut - Duplicate Valid Contact'
                self.duplicated_contacts_list.append(contact)
            if contact.email_entry.email not in unique_email_set:
                unique_email_set.add(contact.email_entry.email)
                self.unique_email_contacts_list.append(contact)
        return

    def build_cutfile(self):
        """
        Iterates over all cut lines and adds to cutfile list

        Returns:
            None
        """

        cut_lists = [self.blacklisted_contacts_list, self.geo_cut_contacts_list, self.email_cut_contacts_list,
                     self.duplicated_contacts_list, self.invalid_contacts_list, self.iw_contacts_list]
        for cut_list in cut_lists:
            for contact in cut_list:
                self.cutfile_contacts_list.append(contact)
        return

    def export_contacts(self, outfile_name=None):
        """
        Exports contacts_list data to the output CSV file

        Return:
            None
        """

        if not outfile_name:
            self.customize_outfile_names()
        else:
            outfile_name = outfile_name.replace('.csv', '')  # this is hacky, but it keeps outfile names clean
            self.ci_filename = outfile_name + '_' + self.ci_filename
            self.edu_ci_filename = outfile_name + '_' + self.edu_ci_filename
            self.prelim_filename = outfile_name + '_' + self.prelim_filename
            self.cutfile_filename = outfile_name + '_' + self.cutfile_filename
            self.high_value_filename = outfile_name + '_' + self.high_value_filename
        print_message("Now writing contacts to output...")
        self.create_deduplicated_contacts_lists()
        self.contacts_list_to_data_dict(self.deduplicated_contacts_list)
        csv_exporter = CSVExporter(self.ci_filename, data_dict=self.full_data_dict, headers_dict=self.ci_headers_dict)
        csv_exporter.write_csv_from_dict()
        csv_exporter = CSVExporter(self.edu_ci_filename, data_dict=self.full_data_dict,
                                   headers_dict=self.edu_ci_headers_dict)
        csv_exporter.write_csv_from_dict()
        self.contacts_list_to_data_dict(self.unique_email_contacts_list)
        csv_exporter = CSVExporter(self.prelim_filename, data_dict=self.full_data_dict,
                                   headers_dict=self.prelim_mailer_headers_dict)
        csv_exporter.write_csv_from_dict()
        if self.high_value_contacts_list:  # not removing this block in case it's still being used in a subclass
            self.contacts_list_to_data_dict(self.high_value_contacts_list)
            csv_exporter = CSVExporter(self.high_value_filename, data_dict=self.full_data_dict,
                                       headers_dict=self.prelim_mailer_headers_dict)
            csv_exporter.write_csv_from_dict()
        self.build_cutfile()
        self.contacts_list_to_data_dict(self.cutfile_contacts_list)
        csv_exporter = CSVExporter(self.cutfile_filename, data_dict=self.full_data_dict, headers_dict=self.headers_dict)
        csv_exporter.write_csv_from_dict()
        print_message("Writing to output complete!")
        return

    def export_ci_only(self, outfile_name=None):
        """
        export contact inventory and cutfile data

        Returns:
            None
        """

        if not outfile_name:
            self.customize_outfile_names()
        else:
            outfile_name = outfile_name.replace('.csv', '')  # this is hacky, but it keeps outfile names clean
            self.ci_filename = outfile_name + '_' + self.ci_filename
            self.edu_ci_filename = outfile_name + '_' + self.edu_ci_filename
            self.cutfile_filename = outfile_name + '_' + self.cutfile_filename
        print_message("Now writing contacts and cutfile to output...")
        self.create_deduplicated_contacts_lists()
        self.contacts_list_to_data_dict(self.deduplicated_contacts_list)
        csv_exporter = CSVExporter(self.ci_filename, data_dict=self.full_data_dict, headers_dict=self.ci_headers_dict)
        csv_exporter.write_csv_from_dict()
        self.build_cutfile()
        self.contacts_list_to_data_dict(self.cutfile_contacts_list)
        csv_exporter = CSVExporter(self.cutfile_filename, data_dict=self.full_data_dict, headers_dict=self.headers_dict)
        csv_exporter.write_csv_from_dict()
        print_message("Writing to output complete!")
        return


class CompOutputWriter(OutputWriter):
    def __init__(self, contacts_list, blacklist_contacts_list, geo_cut_contacts_list, email_cut_contacts_list,
                 vertical_blacklist_contacts_list, response_cut_contacts_list, already_linking_contacts_list,
                 recently_sent_contacts_list, extra_contacts_list, invalid_contacts_list, high_value_contacts_list,
                 iw_contacts_list):
        super(CompOutputWriter, self).__init__(contacts_list, blacklist_contacts_list, geo_cut_contacts_list,
                                               email_cut_contacts_list, invalid_contacts_list, high_value_contacts_list,
                                               iw_contacts_list)
        self.vertical_blacklist_contacts_list = vertical_blacklist_contacts_list
        self.response_cut_contacts_list = response_cut_contacts_list
        self.already_linking_contacts_list = already_linking_contacts_list
        self.recently_sent_contacts_list = recently_sent_contacts_list
        self.extra_contacts_list = extra_contacts_list
        self.high_value_contacts_list = high_value_contacts_list
        self.mailer_filename = u'comp_mailer.csv'
        self.mailer_headers_dict = {0: u'contact_email', 1: u'email_subdomain', 2: u'source_subdomain',
                                    3: u'contact_name', 4: u'source_url', 5: u'source_domain', 6: u'target_url',
                                    7: u'target_domain', 8: u'asset', 9: u'campaign', 10: u'email_variant',
                                    11: u'email_notes', 12: u'audiences', 13: u'subject', 14: u'bucket', 15: u'var1',
                                    16: u'var2', 17: u'var3', 18: u'var4', 19: u'var5', 20: u'var6', 21: u'var7',
                                    22: u'var8', 23: u'previously_contacted'}

    def customize_outfile_names(self):
        """
        Updates the outfile names based on user input.

        Returns:
            None
        """

        custom_key = raw_input("Please enter the custom name for the outfiles for this mailer.\n")
        self.mailer_filename = custom_key + '_' + self.mailer_filename
        self.cutfile_filename = custom_key + '_' + self.cutfile_filename
        self.high_value_filename = custom_key + '_' + self.high_value_filename
        return

    def build_cutfile(self):
        """
        Iterates over all cut lines and adds to cutfile list

        Returns:
            None
        """

        cut_lists = [self.blacklisted_contacts_list, self.geo_cut_contacts_list, self.email_cut_contacts_list,
                     self.vertical_blacklist_contacts_list, self.response_cut_contacts_list,
                     self.already_linking_contacts_list, self.recently_sent_contacts_list, self.extra_contacts_list,
                     self.invalid_contacts_list, self.iw_contacts_list]
        for cut_list in cut_lists:
            for contact in cut_list:
                self.cutfile_contacts_list.append(contact)
        return

    def build_empty_full_data_dict(self):
        """
        Replaces any values stored in full_data_dict with a brand new empty dict only containing keys

        Returns:
            None
        """

        self.full_data_dict = {u'target_url': [], u'target_domain': [], u'asset': [], u'campaign': [],
                               u'source_url': [], u'source_domain': [], u'contact_name': [], u'contact_email': [],
                               u'email_notes': [], u'domain_registrar_country': [], u'domain_admin_country': [],
                               u'domain_tech_country': [], u'domain_ip_address_country': [],
                               u'domain_registrar_locale': [], u'domain_admin_locale': [], u'domain_tech_locale': [],
                               u'domain_ip_geo_locale': [], u'domain_tld_type': [], u'email_registrar_country': [],
                               u'email_admin_country': [], u'email_tech_country': [], u'email_ip_address_country': [],
                               u'email_registrar_locale': [], u'email_admin_locale': [], u'email_tech_locale': [],
                               u'email_ip_geo_locale': [], u'email_tld_type': [], u'domain_geo_based_action': [],
                               u'domain_geo_based_cache': [], u'email_geo_based_action': [],
                               u'email_geo_based_cache': [], u'kickbox_email_action': [], u'email_result': [],
                               u'email_source': [], u'contact_action': [], u'email_domain': [], u'email_subdomain': [],
                               u'source_subdomain': [], u'email_variant': [], u'audiences': [],
                               u'source_domain_trust_flow': [], u'source_domain_topic': [],
                               u'source_domain_topic_trust': [], u'date_collected': [], u'type': [], u'context': [],
                               u'site': [], u'asset_url': [], u'content_group': [], u'series': [],
                               u'previously_contacted': []}

    def contacts_list_to_data_dict(self, contacts_list):
        """
        Adds the attributes for every contact in contacts_list to the full_data_dict for output to csv

        Args:
            contacts_list (list) : a list of Contact objects from which to create data dict

        Returns:
            None
        """

        self.build_empty_full_data_dict()
        for contact in contacts_list:
            self.full_data_dict[u'target_url'].append(contact.target_entry.url)
            self.full_data_dict[u'target_domain'].append(contact.target_entry.domain)
            self.full_data_dict[u'asset'].append(contact.asset_entry.asset)
            self.full_data_dict[u'campaign'].append(contact.asset_entry.campaign)
            self.full_data_dict[u'email_variant'].append(contact.asset_entry.email_variant)
            self.full_data_dict[u'source_url'].append(contact.source_entry.url)
            self.full_data_dict[u'source_domain'].append(contact.source_entry.domain)
            self.full_data_dict[u'contact_name'].append(contact.contact_name)
            self.full_data_dict[u'contact_email'].append(contact.email_entry.email)
            self.full_data_dict[u'domain_registrar_country'].append(contact.source_entry.whois_entry.registrar_country)
            self.full_data_dict[u'domain_admin_country'].append(contact.source_entry.whois_entry.admin_country)
            self.full_data_dict[u'domain_tech_country'].append(contact.source_entry.whois_entry.tech_country)
            self.full_data_dict[u'domain_ip_address_country'].append(contact.source_entry.ip_geo_entry.country_name)
            self.full_data_dict[u'domain_registrar_locale'].append(contact.source_entry.whois_entry.registrar_locale)
            self.full_data_dict[u'domain_admin_locale'].append(contact.source_entry.whois_entry.admin_locale)
            self.full_data_dict[u'domain_tech_locale'].append(contact.source_entry.whois_entry.tech_locale)
            self.full_data_dict[u'domain_ip_geo_locale'].append(contact.source_entry.ip_geo_entry.locale)
            self.full_data_dict[u'domain_tld_type'].append(contact.source_entry.tld_entry.tld_type)
            self.full_data_dict[u'email_registrar_country'].append(contact.email_entry.whois_entry.registrar_country)
            self.full_data_dict[u'email_admin_country'].append(contact.email_entry.whois_entry.admin_country)
            self.full_data_dict[u'email_tech_country'].append(contact.email_entry.whois_entry.tech_country)
            self.full_data_dict[u'email_ip_address_country'].append(contact.email_entry.ip_geo_entry.country_name)
            self.full_data_dict[u'email_registrar_locale'].append(contact.email_entry.whois_entry.registrar_locale)
            self.full_data_dict[u'email_admin_locale'].append(contact.email_entry.whois_entry.admin_locale)
            self.full_data_dict[u'email_tech_locale'].append(contact.email_entry.whois_entry.tech_locale)
            self.full_data_dict[u'email_ip_geo_locale'].append(contact.email_entry.ip_geo_entry.locale)
            self.full_data_dict[u'email_tld_type'].append(contact.email_entry.tld_entry.tld_type)
            self.full_data_dict[u'domain_geo_based_action'].append(contact.source_entry.action)
            self.full_data_dict[u'domain_geo_based_cache'].append(contact.source_entry.cache)
            self.full_data_dict[u'email_geo_based_action'].append(contact.email_entry.action)
            self.full_data_dict[u'email_geo_based_cache'].append(contact.email_entry.cache)
            self.full_data_dict[u'kickbox_email_action'].append(contact.email_entry.kickbox_action)
            self.full_data_dict[u'email_result'].append(contact.email_entry.result)
            self.full_data_dict[u'email_source'].append(contact.email_entry.source)
            self.full_data_dict[u'email_notes'].append(contact.asset_entry.email_notes)
            self.full_data_dict[u'contact_action'].append(contact.contact_action)
            self.full_data_dict[u'email_domain'].append(contact.email_entry.domain)
            self.full_data_dict[u'email_subdomain'].append(contact.email_entry.subdomain)
            self.full_data_dict[u'source_subdomain'].append(contact.source_entry.subdomain)
            self.full_data_dict[u'source_domain_trust_flow'].append(contact.source_entry.trust_flow)
            self.full_data_dict[u'source_domain_topic'].append(contact.source_entry.topic)
            self.full_data_dict[u'source_domain_topic_trust'].append(contact.source_entry.topic_trust_flow)
            self.full_data_dict[u'audiences'].append(contact.asset_entry.audiences)
            self.full_data_dict[u'date_collected'].append(contact.date_collected)
            self.full_data_dict[u'type'].append(contact.contact_type)
            self.full_data_dict[u'site'].append(contact.asset_entry.site)
            self.full_data_dict[u'asset_url'].append(contact.asset_entry.asset_url)
            self.full_data_dict[u'content_group'].append(contact.asset_entry.content_group)
            self.full_data_dict[u'series'].append(contact.asset_entry.series)
            self.full_data_dict[u'context'].append(contact.asset_entry.context)
            self.full_data_dict[u'previously_contacted'].append(contact.previously_contacted)
        return

    def export_contacts(self, outfile_name=None):
        """
        Exports contacts_list data to the output CSV file

        Returns:
            None
        """

        if not outfile_name:
            self.customize_outfile_names()
        else:
            self.mailer_filename = outfile_name + '_' + self.mailer_filename
            self.cutfile_filename = outfile_name + '_' + self.cutfile_filename
        print_message("Now writing contacts to output...")
        self.create_deduplicated_contacts_lists()
        self.contacts_list_to_data_dict(self.unique_email_contacts_list)
        csv_exporter = CSVExporter(self.mailer_filename, data_dict=self.full_data_dict,
                                   headers_dict=self.mailer_headers_dict)
        csv_exporter.write_csv_from_dict()
        self.contacts_list_to_data_dict(self.high_value_contacts_list)
        csv_exporter = CSVExporter(self.high_value_filename, data_dict=self.full_data_dict,
                                   headers_dict=self.mailer_headers_dict)
        csv_exporter.write_csv_from_dict()
        self.build_cutfile()
        self.contacts_list_to_data_dict(self.cutfile_contacts_list)
        csv_exporter = CSVExporter(self.cutfile_filename, data_dict=self.full_data_dict, headers_dict=self.headers_dict)
        csv_exporter.write_csv_from_dict()
        print_message("Writing to output complete!")
        return


class CompPublisher(CompOutputWriter):
    def __init__(self, contacts_list, blacklist_contacts_list, geo_cut_contacts_list, email_cut_contacts_list,
                 vertical_blacklist_contacts_list, response_cut_contacts_list, already_linking_contacts_list,
                 recently_sent_contacts_list, extra_contacts_list, invalid_contacts, high_value_contacts_list,
                 iw_contacts_list):
        super(CompPublisher, self).__init__(contacts_list, blacklist_contacts_list, geo_cut_contacts_list,
                                            email_cut_contacts_list, vertical_blacklist_contacts_list,
                                            response_cut_contacts_list, already_linking_contacts_list,
                                            recently_sent_contacts_list, extra_contacts_list, invalid_contacts,
                                            high_value_contacts_list, iw_contacts_list)
        self.inventory_cxn = None

    def connect_inventory_cxn(self):
        """
        Sets up a connection to the contact inventory database

        Returns:
            None
        """

        self.inventory_cxn = DBConnection('root', 'vagrant', '127.0.0.1', 'contact_inventory')
        self.inventory_cxn.connect()
        return

    def close_inventory_cxn(self):
        """
        Closes the inventory connection

        Returns:
            None
        """

        self.inventory_cxn.close()
        return

    def update_inventory_recent_sends(self):
        """
        Adds all contacts that will be added to the send-ready CSV to the recent_sends table in the DB

        Returns:
            None
        """

        self.connect_inventory_cxn()
        today_str = datetime.date.today().strftime('%Y-%m-%d')
        statement = "INSERT INTO hist_sends (email, site, send_date) VALUES (%s, %s, %s)"
        value_list = [tuple([contact.email_entry.email, contact.asset_entry.site, today_str]) for
                      contact in self.unique_email_contacts_list]
        if value_list:
            self.inventory_cxn.execute_update_many(statement, value_list)
        else:
            print("Unable to push recent sends to Database. Please be sure to update manually.")
        self.close_inventory_cxn()
        return

    def export_contacts(self, outfile_name=None):
        """
        Exports contacts_list data to the output CSV file

        Returns:
            None
        """

        if not outfile_name:
            self.customize_outfile_names()
        else:
            self.mailer_filename = outfile_name + '_' + self.mailer_filename
            self.cutfile_filename = outfile_name + '_' + self.cutfile_filename
            self.high_value_filename = outfile_name + '_' + self.high_value_filename
        self.create_deduplicated_contacts_lists()
        print_message("Now updating hist_sends table in database.")
        self.update_inventory_recent_sends()
        print_message("Now writing contacts to output...")
        self.contacts_list_to_data_dict(self.unique_email_contacts_list)
        csv_exporter = CSVExporter(self.mailer_filename, data_dict=self.full_data_dict,
                                   headers_dict=self.mailer_headers_dict)
        csv_exporter.write_csv_from_dict()
        self.contacts_list_to_data_dict(self.high_value_contacts_list)
        csv_exporter = CSVExporter(self.high_value_filename, data_dict=self.full_data_dict,
                                   headers_dict=self.mailer_headers_dict)
        csv_exporter.write_csv_from_dict()
        self.build_cutfile()
        self.contacts_list_to_data_dict(self.cutfile_contacts_list)
        csv_exporter = CSVExporter(self.cutfile_filename, data_dict=self.full_data_dict, headers_dict=self.headers_dict)
        csv_exporter.write_csv_from_dict()
        print_message("Writing to output complete!")
        return


class FollowUpWriter(OutputWriter):
    """


    """

    def __init__(self, contacts_list, responder_cut_contacts_list, blacklisted_contacts_list, already_linking,
                 geo_cut_contacts_list, email_cut_contacts_list, invalid_contacts_list, high_value_contacts_list,
                 iw_contacts_list):
        self.mailer_filename = u'followup_mailer.csv'
        self.cutfile_filename = u'cutfile.csv'
        self.high_value_filename = u'high_value_targets.csv'
        self.mailer_headers_dict = {0: u'contact_email', 1: u'email_subdomain', 2: u'source_subdomain',
                                    3: u'contact_name', 4: u'source_url', 5: u'source_domain', 6: u'target_url',
                                    7: u'target_domain', 8: u'asset', 9: u'campaign', 10: u'email_variant',
                                    11: u'email_notes', 12: u'audiences', 13: u'subject', 14: u'bucket', 15: u'var1',
                                    16: u'var2', 17: u'var3', 18: u'var4', 19: u'var5', 20: u'var6', 21: u'var7',
                                    22: u'var8'}
        self.headers_dict = {0: u'target_url', 1: u'target_domain', 2: u'asset', 3: u'campaign', 4: u'email_variant',
                             5: u'source_url', 6: u'source_domain', 7: u'contact_name', 8: u'contact_email',
                             9: u'email_notes', 10: u'domain_registrar_country', 11: u'domain_admin_country',
                             12: u'domain_tech_country', 13: u'domain_ip_address_country',
                             14: u'domain_registrar_locale', 15: u'domain_admin_locale', 16: u'domain_tech_locale',
                             17: u'domain_ip_geo_locale', 18: u'domain_tld_type', 19: u'email_registrar_country',
                             20: u'email_admin_country', 21: u'email_tech_country', 22: u'email_ip_address_country',
                             23: u'email_registrar_locale', 24: u'email_admin_locale', 25: u'email_tech_locale',
                             26: u'email_ip_geo_locale', 27: u'email_tld_type', 28: u'domain_geo_based_action',
                             29: u'domain_geo_based_cache', 30: u'email_geo_based_action', 31: u'email_geo_based_cache',
                             32: u'kickbox_email_action', 33: u'email_result', 34: u'email_source',
                             35: u'contact_action'}
        self.full_data_dict = {}
        self.deduplicated_contacts_list = []
        self.unique_email_contacts_list = []
        self.contacts_list = contacts_list
        self.responder_cut_contacts_list = responder_cut_contacts_list
        self.blacklisted_contacts_list = blacklisted_contacts_list
        self.invalid_contacts_list = invalid_contacts_list
        self.already_linking = already_linking
        self.geo_cut_contacts_list = geo_cut_contacts_list
        self.high_value_contacts_list = high_value_contacts_list
        self.email_cut_contacts_list = email_cut_contacts_list
        self.iw_contacts_list = iw_contacts_list
        self.cutfile_contacts_list = []
        self.duplicated_contacts_list = []

    def customize_outfile_names(self):
        """
        Updates the outfile names based on user input.

        Returns:
            None
        """

        custom_key = raw_input("Please enter the custom name for the outfiles for this mailer.\n")
        self.mailer_filename = custom_key + '_' + self.mailer_filename
        self.cutfile_filename = custom_key + '_' + self.cutfile_filename
        self.high_value_filename = custom_key + '_' + self.high_value_filename
        return

    def build_cutfile(self):
        """
        Iterates over all cut lines and adds to cutfile list

        Returns:
            None
        """

        cut_lists = [self.responder_cut_contacts_list, self.blacklisted_contacts_list, self.already_linking,
                     self.geo_cut_contacts_list, self.email_cut_contacts_list, self.duplicated_contacts_list,
                     self.invalid_contacts_list, self.iw_contacts_list]
        for cut_list in cut_lists:
            for contact in cut_list:
                self.cutfile_contacts_list.append(contact)
        return

    def export_contacts(self, outfile_name=None):
        """
        Exports contacts_list data to the output CSV file

        Returns:
            None
        """

        if not outfile_name:
            self.customize_outfile_names()
        else:
            self.mailer_filename = outfile_name + '_' + self.mailer_filename
            self.cutfile_filename = outfile_name + '_' + self.cutfile_filename
            self.high_value_filename = outfile_name + '_' + self.high_value_filename
        print_message("Now writing contacts to output...")
        self.create_deduplicated_contacts_lists()
        self.contacts_list_to_data_dict(self.unique_email_contacts_list)
        csv_exporter = CSVExporter(self.mailer_filename, data_dict=self.full_data_dict,
                                   headers_dict=self.mailer_headers_dict)
        csv_exporter.write_csv_from_dict()
        self.contacts_list_to_data_dict(self.high_value_contacts_list)
        csv_exporter = CSVExporter(self.high_value_filename, data_dict=self.full_data_dict,
                                   headers_dict=self.mailer_headers_dict)
        csv_exporter.write_csv_from_dict()
        self.build_cutfile()
        self.contacts_list_to_data_dict(self.cutfile_contacts_list)
        csv_exporter = CSVExporter(self.cutfile_filename, data_dict=self.full_data_dict, headers_dict=self.headers_dict)
        csv_exporter.write_csv_from_dict()
        print_message("Writing to output complete!")
        return


class FollowUpPublisher(FollowUpWriter):
    def __init__(self, contacts_list, responder_cut_contacts_list, blacklisted_contacts_list, already_linking,
                 geo_cut_contacts_list, email_cut_contacts_list, invalid_contacts_list, high_value_contacts_list,
                 iw_contacts_list):
        super(FollowUpPublisher, self).__init__(contacts_list, responder_cut_contacts_list, blacklisted_contacts_list,
                                                already_linking, geo_cut_contacts_list, email_cut_contacts_list,
                                                invalid_contacts_list, high_value_contacts_list, iw_contacts_list)
        self.inventory_cxn = None

    def connect_inventory_cxn(self):
        """
        Sets up a connection to the contact inventory database

        Returns:
            None
        """

        self.inventory_cxn = DBConnection('root', 'vagrant', '127.0.0.1', 'contact_inventory')
        self.inventory_cxn.connect()
        return

    def close_inventory_cxn(self):
        """
        Closes the inventory connection

        Returns:
            None
        """

        self.inventory_cxn.close()
        return

    def update_inventory_recent_sends(self):
        """
        Adds all contacts that will be added to the send-ready CSV to the recent_sends table in the DB

        Returns:
            None
        """

        self.connect_inventory_cxn()
        today_str = datetime.date.today().strftime('%Y-%m-%d')
        statement = "INSERT INTO hist_sends (email, site, send_date) VALUES (%s, %s, %s)"
        value_list = [tuple([contact.email_entry.email, contact.asset_entry.site, today_str]) for
                      contact in self.unique_email_contacts_list]
        if value_list:
            self.inventory_cxn.execute_update_many(statement, value_list)
        else:
            print("Unable to push recent sends to Database. Please be sure to update manually.")
        self.close_inventory_cxn()
        return

    def export_contacts(self, outfile_name=None):
        """
        Exports contacts_list data to the output CSV file

        Returns:
            None
        """

        if not outfile_name:
            self.customize_outfile_names()
        else:
            self.mailer_filename = outfile_name + '_' + self.mailer_filename
            self.cutfile_filename = outfile_name + '_' + self.cutfile_filename
            self.high_value_filename = outfile_name + '_' + self.high_value_filename
        self.create_deduplicated_contacts_lists()
        print_message("Now updating hist_sends table in database.")
        self.update_inventory_recent_sends()
        print_message("Now writing contacts to output...")
        self.contacts_list_to_data_dict(self.unique_email_contacts_list)
        csv_exporter = CSVExporter(self.mailer_filename, data_dict=self.full_data_dict,
                                   headers_dict=self.mailer_headers_dict)
        csv_exporter.write_csv_from_dict()
        self.contacts_list_to_data_dict(self.high_value_contacts_list)
        csv_exporter = CSVExporter(self.high_value_filename, data_dict=self.full_data_dict,
                                   headers_dict=self.mailer_headers_dict)
        csv_exporter.write_csv_from_dict()
        self.build_cutfile()
        self.contacts_list_to_data_dict(self.cutfile_contacts_list)
        csv_exporter = CSVExporter(self.cutfile_filename, data_dict=self.full_data_dict, headers_dict=self.headers_dict)
        csv_exporter.write_csv_from_dict()
        print_message("Writing to output complete!")
        return


class RecentSendsScreener(object):
    """
    A class used to check contacts against recent sends items in the inventory database

    Attributes:
        unscreened_contacts (list) : a list of contacts that has to be screened
        screened_contacts (list) : a list of contacts that has been screened
        recently_sent_contacts (list) : a list of contacts that has been cut due to screen rules
        recently_sent_emails (dict) : a dict of the form {site : set([emails,...])} containing recent sends
        recently_sent_verticals_emails (dict) : a dict of form {vertical : set([emails,...])} containing recent sends
        vertical_dict (dict) : a dict containing a mapping of sites to verticals
        inventory_cxn (DBConnection) : a DBConnection to inventory
    """

    def __init__(self, contacts):
        """
        Initializes the object

        Args:
            contacts (list) : a list of contacts
        """

        self.unscreened_contacts = contacts
        self.screened_contacts = []
        self.recently_sent_contacts = []
        self.recently_sent_emails = {u'ACO': set([]),
                                     u'ASO': set([]),
                                     u'CSO': set([]),
                                     u'FS': set([]),
                                     u'GG': set([]),
                                     u'LHTB': set([]),
                                     u'LP': set([]),
                                     u'TT': set([]),
                                     u'MG': set([]),
                                     u'LGS': set([])}
        self.recently_sent_verticals_emails = {u'EDU': set([]),
                                               u'FIN': set([]),
                                               u'SOL': set([])}
        self.vertical_dict = {u'ACO': u'EDU',
                              u'ASO': u'EDU',
                              u'CSO': u'EDU',
                              u'FS': u'EDU',
                              u'GG': u'EDU',
                              u'LHTB': u'EDU',
                              u'LP': u'EDU',
                              u'TT': u'EDU',
                              u'MG': u'FIN',
                              u'LGS': u'SOL'}
        self.inventory_cxn = None

    def connect_inventory_cxn(self):
        """
        Sets up a connection to the contact inventory database

        Returns:
            None
        """

        self.inventory_cxn = DBConnection('root', 'vagrant', '127.0.0.1', 'contact_inventory')
        self.inventory_cxn.connect()
        return

    def close_inventory_cxn(self):
        """
        Closes the inventory connection

        Returns:
            None
        """

        self.inventory_cxn.close()
        return

    def screen_recently_sent_contacts(self):
        """
        Checks contacts against recent sends and recent vertical sends

        Returns:
            A list of screened contacts and a list of contacts cut due to recent send rules
        """

        self.retrieve_recently_sent_contacts()
        for contact in self.unscreened_contacts:
            email = contact.email_entry.email
            site = contact.asset_entry.site
            vertical = self.vertical_dict[site]
            recent = False
            if email in self.recently_sent_emails[site]:
                contact.contact_action = u'Recent Send <= 30 Days'
                recent = True
            if email in self.recently_sent_verticals_emails[vertical]:
                contact.contact_action = u'Vertical Send <= 7 Days'
                recent = True
            if recent:
                self.recently_sent_contacts.append(contact)
            else:
                self.screened_contacts.append(contact)
        return self.screened_contacts, self.recently_sent_contacts

    def retrieve_recently_sent_contacts(self):
        """
        Removes email records from recent sends older than 30 days and retrieves all other emails

        Returns:
            None
        """

        self.connect_inventory_cxn()
        self.inventory_cxn.execute_update("DELETE FROM hist_sends "
                                          "WHERE datediff(send_date, curdate()) <= -60")
        sites = tuple(set([contact.asset_entry.site for contact in self.unscreened_contacts]))
        s_placeholder = ",".join(["%s" for s in sites])
        statement = "SELECT email, site, send_date FROM hist_sends WHERE site in ({})".format(s_placeholder)
        self.inventory_cxn.execute_select(statement, sites)
        last_month_sent = self.inventory_cxn.cursor_to_list_of_rows()
        print_message("Retrieved {} emails from the recent sends cache (site-specific) for these sites: {}".format(
            len(last_month_sent), ', '.join(list(sites))))
        for row in last_month_sent:
            email, site, date = row
            email = set_string_to_unicode(email)
            site = set_string_to_unicode(site)
            self.recently_sent_emails[site].add(email)
        verts = set([self.vertical_dict[site] for site in sites])
        sites_in_verts = tuple(set([site for site, vertical in self.vertical_dict.iteritems() if vertical in verts]))
        s_placeholder = ",".join(["%s" for s in sites_in_verts])
        statement = "SELECT email, site, send_date FROM hist_sends WHERE site in ({}) AND datediff(send_date, curdate()) >= -14".format(
            s_placeholder)
        self.inventory_cxn.execute_select(statement, sites_in_verts)
        five_days_verts = self.inventory_cxn.cursor_to_list_of_rows()
        print_message("Retrieved {} emails from the recent sends cache (vertical-specific) for these sites: {}".format(
            len(five_days_verts), ', '.join(list(sites_in_verts))))
        for row in five_days_verts:
            email, site, date = row
            email = set_string_to_unicode(email)
            site = set_string_to_unicode(site)
            vert = self.vertical_dict[site]
            self.recently_sent_verticals_emails[vert].add(email)
        self.close_inventory_cxn()
        return


class ReferringDomainsScreener(object):
    """
    A class to check for referring domains already linking to a set of asset URLs.

    Attributes:
        unscreened_contacts (list) : a list of contacts objects
        screened_contacts (list) : a list of contacts objects that has been screened against referring domains
        already_linking_contacts (list) : a list of already-linking contacts
        urls_to_check (set) : a set of URLs to check in Majestic API
        linking_domains (dict) : a dict of the form {asset_url : [linking_domains]}

    """

    def __init__(self, unscreened_contacts):
        """
        Initializes a ReferringDomainsScreener object

        Args:
            unscreened_contacts (list) : a list of contacts objects
        """

        self.unscreened_contacts = unscreened_contacts
        self.screened_contacts = []
        self.already_linking_contacts = []
        self.urls_to_check = set([])
        self.linking_domains = {}
        self.emails_cut = set([])  # remove this to keep contacts linking to other assets

    def set_urls_to_check(self):
        """
        Iterates over all unscreened contacts and adds all asset urls that need to be checked

        Returns:
            None
        """

        for contact in self.unscreened_contacts:
            self.urls_to_check.add(contact.asset_entry.asset_url)
        return

    def get_linking_domains(self):
        """
        Sends Majestic queries for all asset urls that need to be checked and adds results to linking_domains dict

        Returns:
            None
        """

        majestic_connection = MajesticConnection(u'https://api.majestic.com/api_command',
                                                 u'64A68FD403AC17BBBA40960B1B3719E6')
        majestic_connection.build_api_connection()
        for url in self.urls_to_check:
            ref_domains = majestic_connection.get_referring_domains_single_item(item=url)
            ref_domains = [set_string_to_unicode(domain) for domain in ref_domains]
            self.linking_domains[url] = ref_domains
        return

    def screen_already_linking_domains(self):
        """
        Iterates over all contacts and screens them against the linking domains dict for their asset urls

        Returns:
            The list of screened contacts and the list of already linking contacts
        """

        self.set_urls_to_check()
        self.get_linking_domains()
        for contact in self.unscreened_contacts:
            url = contact.asset_entry.asset_url
            ref_domains = self.linking_domains[url]
            source_domain = contact.source_entry.domain
            source_subdomain = contact.source_entry.subdomain
            email_domain = contact.email_entry.domain
            email_subdomain = contact.email_entry.subdomain
            email = contact.email_entry.email  # remove this to keep contacts linking to other assets
            linking = False
            if source_domain in ref_domains:
                contact.contact_action = u'Source Domain Already Linking'
                linking = True
            if source_subdomain in ref_domains:
                contact.contact_action = u'Source Subdomain Already Linking'
                linking = True
            if email_domain in ref_domains:
                contact.contact_action = u'Email Domain Already Linking'
                linking = True
            if email_subdomain in ref_domains:
                contact.contact_action = u'Email Subdomain Already Linking'
                linking = True
            if email in self.emails_cut:
                contact.contact_action = u'Email Contact Already Linking'
                linking = True
            if linking:
                self.already_linking_contacts.append(contact)
                self.emails_cut.add(email)  # remove this to keep contacts linking to other assets
            else:
                self.screened_contacts.append(contact)
        return self.screened_contacts, self.already_linking_contacts


class ResponseScreener(object):
    def __init__(self, unscreened_contacts):
        self.unscreened_contacts = unscreened_contacts
        self.email_contact_dict = {contact.email_entry.email: contact for contact in self.unscreened_contacts}
        self.whitelist_domains = whitelisted_email_domains
        self.imap_cxn = None
        self.inventory_cxn = DBConnection('root', 'vagrant', '127.0.0.1', 'contact_inventory')
        self.selected_site = None
        self.selected_account_creds = None
        self.selected_folder = None
        self.email_acct_creds = {}
        self.responders_set = set([])
        self.responding_domains_set = set([])
        self.more_responders = True
        self.keep_contacts = []
        self.cut_contacts = []

    def retrieve_email_acct_creds(self):
        self.inventory_cxn.connect()
        self.inventory_cxn.set_cursor()
        self.inventory_cxn.cursor.execute('call spGetActiveEmailCredentials')
        data_rows = self.inventory_cxn.cursor_to_list_of_rows()
        for row in data_rows:
            site = row[0]
            creds_dict = {'email': row[1], 'password': row[2], 'imap_host': row[3], 'imap_port': row[4]}
            if site in self.email_acct_creds:
                self.email_acct_creds[site].append(creds_dict)
            else:
                self.email_acct_creds[site] = [creds_dict]
        self.inventory_cxn.close()

    def prompt_account(self):
        """Prompt user to make selection from list of Abuv websites.

        Calls self.prompt_account() because self.account and self.password are the imperative data points. Self.site
        is just a logical way to subdivide possible choices.
        """
        print_message("Please select this campaign's site.")
        self.selected_site = selection_from_list(self.email_acct_creds.keys(), list_name=u'sites', cols_disabled=True)
        print_message("Please select the this campaign's email account.")
        emails_dict = OrderedDict({})
        for record in self.email_acct_creds[self.selected_site]:
            emails_dict[record['email']] = record
        selected_email = selection_from_list(emails_dict, list_name=self.selected_site, indent=2, sorted=False,
                                             cols_disabled=True)
        self.selected_account_creds = emails_dict[selected_email]

    def connect_imap(self):
        self.imap_cxn = IMAPConnection(self.selected_account_creds['email'], self.selected_account_creds['password'],
                                       self.selected_account_creds['imap_host'],
                                       self.selected_account_creds['imap_port'])
        self.imap_cxn.connect()

    def update_db_count(self):
        self.inventory_cxn.connect()
        statement = """
            INSERT INTO email_acct_use_counter
                (email_acct, site, last_date)
            VALUES
                (%s, %s, %s)
            ON DUPLICATE KEY UPDATE count=count+1, last_date=curdate()
            """
        values = tuple(
            [self.selected_account_creds['email'], self.selected_site, datetime.date.today().strftime('%Y-%m-%d')])
        self.inventory_cxn.execute_update(statement, values)
        self.inventory_cxn.close()

    def prompt_folder(self):
        """Prompt user to make selection from top-level folders in self.account.

        Adds selection to self.folders before returning because we need to export responses in this folder also.
        """
        parent_folders = self.imap_cxn.get_folders(parent_folders_only=True)
        self.selected_folder = selection_from_list(parent_folders, list_name=self.selected_account_creds['email'],
                                                   indent=3)

    def get_responders(self):
        folders = self.imap_cxn.get_folders(top_level_folder=self.selected_folder)
        for folder in folders:
            folder_responders = self.imap_cxn.get_emails_set_from_folder(folder)
            self.responders_set = self.responders_set.union(folder_responders)

    def build_responding_domains_set(self):
        """Iterate over responders_set generating complete list of domains to be used in responding domain screen."""
        for responder in self.responders_set:
            self.extract_email_domains(responder)
            if responder in self.email_contact_dict:
                contact_source_domain = self.email_contact_dict[responder].source_entry.domain
                if contact_source_domain not in self.whitelist_domains:
                    self.responding_domains_set.add(contact_source_domain)
                contact_source_subdomain = self.email_contact_dict[responder].source_entry.subdomain
                if contact_source_subdomain not in self.whitelist_domains:
                    self.responding_domains_set.add(contact_source_subdomain)

    def extract_email_domains(self, email_addr):
        """
        Extract the domain and subdomain from an email address

        Args:
            email_addr (string) : an email address to have the domain and subdomain determined for
        """

        email_domain_extract = tldextract.extract(email_addr)
        email_domain = u'.'.join([email_domain_extract.domain, email_domain_extract.suffix])
        if email_domain not in self.whitelist_domains:
            self.responding_domains_set.add(email_domain)
        if email_domain_extract.subdomain:
            email_subdomain = u'.'.join([email_domain_extract.subdomain, email_domain_extract.domain,
                                         email_domain_extract.suffix])
            if email_subdomain not in self.whitelist_domains:
                self.responding_domains_set.add(email_subdomain)

    def apply_rules(self):
        for contact in self.unscreened_contacts:
            rejected = False
            if contact.email_entry.email in self.responders_set:
                contact.contact_action = u'Responder Email'
                rejected = True
            if contact.email_entry.domain in self.responding_domains_set:
                contact.contact_action = u'Responder Email Domain'
                rejected = True
            if contact.email_entry.subdomain in self.responding_domains_set:
                contact.contact_action = u'Responder Email Subdomain'
                rejected = True
            if contact.source_entry.domain in self.responding_domains_set:
                contact.contact_action = u'Responder Source Domain'
                rejected = True
            if contact.source_entry.subdomain in self.responding_domains_set:
                contact.contact_action = u'Responder Source Subdomain'
                rejected = True
            if rejected:
                self.cut_contacts.append(contact)
            else:
                self.keep_contacts.append(contact)

    def reset_selections(self):
        self.selected_site = None
        self.selected_account_creds = None
        self.selected_folder = None
        if self.imap_cxn:
            self.imap_cxn.disconnect()
            self.imap_cxn = None

    def prompt_more_responders(self):
        while True:
            print_message("Would you like to export more responders?")
            selection = raw_input("1: Yes\n"
                                  "2: No\n")
            int_selection = int(selection)
            if int_selection == 1:
                self.more_responders = True
                return
            elif int_selection == 2:
                self.more_responders = False
                return
            else:
                print_message("Invalid selection. Please enter a valid number.")

    def screen_responders(self):
        self.retrieve_email_acct_creds()
        while self.more_responders:
            self.prompt_account()
            self.update_db_count()
            self.connect_imap()
            self.prompt_folder()
            self.get_responders()
            self.prompt_more_responders()
            if self.more_responders:
                self.reset_selections()
        self.build_responding_domains_set()
        print_message("Responders: {}".format(len(self.responders_set)))
        print_message("Responding domains: {}".format(len(self.responding_domains_set)))
        self.apply_rules()
        return self.keep_contacts, self.cut_contacts


class SendReadyPublisher(object):
    """
    A class that processes contacts and writes them to CSV output, pushes relevant info to database

    Attributes:

    """

    def __init__(self, accepted_contacts, blacklist_contacts, linking_contacts, recent_contacts, invalid_contacts,
                 additional_contacts, high_value_contacts, iw_cut_contacts):
        """
        Initializes the object

        Args:
            accepted_contacts (list) : a list of contacts that are accepted for the send-ready mailer
            blacklist_contacts (list) : a list of contacts that are blacklisted
            linking_contacts (list) : a list of contacts that are already-linking
            recent_contacts (list) : a list of contacts that received recent sends
            invalid_contacts (list) : a list of contacts that are not valid by data cleaning business rules
            additional_contacts (list) : a list of contacts that exceed the send-limit per domain
            high_value_contacts (list) : a list of high-value contacts that require manual treatment
        """

        self.accepted_contacts = accepted_contacts
        self.blacklist_contacts = blacklist_contacts
        self.linking_contacts = linking_contacts
        self.recent_contacts = recent_contacts
        self.additional_contacts = additional_contacts
        self.invalid_contacts = invalid_contacts
        self.high_value_contacts = high_value_contacts
        self.iw_cut_contacts = iw_cut_contacts
        self.cutfile_contacts_list = []
        self.send_ready_mailer_headers_dict = {0: u'contact_email', 1: u'email_subdomain', 2: u'source_subdomain',
                                               3: u'contact_name', 4: u'source_url', 5: u'source_domain',
                                               6: u'target_url', 7: u'target_domain', 8: u'asset', 9: u'campaign',
                                               10: u'email_variant', 11: u'email_notes', 12: u'audiences',
                                               13: u'subject', 14: u'bucket', 15: u'var1', 16: u'var2', 17: u'var3',
                                               18: u'var4', 19: u'var5', 20: u'var6', 21: u'var7', 22: u'var8'}
        self.cutfile_headers_dict = {0: u'contact_email', 1: u'email_subdomain', 2: u'source_subdomain',
                                     3: u'contact_name', 4: u'source_url', 5: u'source_domain', 6: u'target_url',
                                     7: u'target_domain', 8: u'asset', 9: u'campaign', 10: u'email_variant',
                                     11: u'email_notes', 12: u'audiences', 13: u'contact_action'}
        self.full_data_dict = {}
        self.send_ready_filename = None
        self.cutfile_filename = None
        self.high_value_contacts_filename = None
        self.inventory_cxn = None

    def connect_inventory_cxn(self):
        """
        Sets up a connection to the contact inventory database

        Returns:
            None
        """

        self.inventory_cxn = DBConnection('root', 'vagrant', '127.0.0.1', 'contact_inventory')
        self.inventory_cxn.connect()
        return

    def close_inventory_cxn(self):
        """
        Closes the inventory connection

        Returns:
            None
        """

        self.inventory_cxn.close()
        return

    def update_inventory_recent_sends(self):
        """
        Adds all contacts that will be added to the send-ready CSV to the recent_sends table in the DB

        Returns:
            None
        """

        self.connect_inventory_cxn()
        today_str = datetime.date.today().strftime('%Y-%m-%d')
        statement = "INSERT INTO hist_sends (email, site, send_date) VALUES (%s, %s, %s)"
        value_list = [tuple([contact.email_entry.email, contact.asset_entry.site, today_str]) for
                      contact in self.accepted_contacts]
        if value_list:
            self.inventory_cxn.execute_update_many(statement, value_list)
        else:
            print("Unable to push recent sends to Database. Please be sure to update manually.")
        self.close_inventory_cxn()
        return

    def customize_outfile_names(self):
        """
        Updates the outfile names based on user input.

        Returns:
            None
        """

        custom_key = raw_input("Please enter the custom name for the outfiles for this mailer.\n")
        if custom_key[:-4] != '.csv':
            custom_key += '.csv'
        self.send_ready_filename = custom_key
        self.cutfile_filename = custom_key[:-4] + "_cutfile" + custom_key[-4:]
        self.high_value_contacts_filename = custom_key[:-4] + "high_value_contacts" + custom_key[-4:]
        return

    def build_empty_full_data_dict(self):
        """
        Replaces any values stored in full_data_dict with a brand new empty dict only containing keys

        Returns:
            None
        """

        self.full_data_dict = {u'target_url': [], u'target_domain': [], u'asset': [], u'campaign': [],
                               u'source_url': [], u'source_domain': [], u'contact_name': [], u'contact_email': [],
                               u'email_notes': [], u'domain_registrar_country': [], u'domain_admin_country': [],
                               u'domain_tech_country': [], u'domain_ip_address_country': [],
                               u'domain_registrar_locale': [], u'domain_admin_locale': [], u'domain_tech_locale': [],
                               u'domain_ip_geo_locale': [], u'domain_tld_type': [], u'email_registrar_country': [],
                               u'email_admin_country': [], u'email_tech_country': [], u'email_ip_address_country': [],
                               u'email_registrar_locale': [], u'email_admin_locale': [], u'email_tech_locale': [],
                               u'email_ip_geo_locale': [], u'email_tld_type': [], u'domain_geo_based_action': [],
                               u'domain_geo_based_cache': [], u'email_geo_based_action': [],
                               u'email_geo_based_cache': [], u'kickbox_email_action': [], u'email_result': [],
                               u'email_source': [], u'contact_action': [], u'email_domain': [], u'email_subdomain': [],
                               u'source_subdomain': [], u'email_variant': [], u'audiences': [],
                               u'source_domain_trust_flow': [], u'source_domain_topic': [],
                               u'source_domain_topic_trust': [], u'date_collected': [], u'type': [], u'context': [],
                               u'site': [], u'asset_url': [], u'content_group': [], u'series': [], u'entity_name': [],
                               u'entity_type': [], u'city': [], u'state': [], u'entity_website': [],
                               u'entity_subgroup': [], u'entity_subgroup_url': [], u'entity_subgroup_resources_url': [],
                               u'subject': [], u'bucket': [], u'var1': [], u'var2': [], u'var3': [], u'var4': [],
                               u'var5': [], u'var6': [], u'var7': [], u'var8': []}
        return

    def contacts_list_to_data_dict(self, contacts_list):
        """
        Adds the attributes for every contact in contacts_list to the full_data_dict for output to csv

        Args:
            contacts_list (list) : a list of Contact objects from which to create data dict

        Returns:
            None
        """

        self.build_empty_full_data_dict()
        for contact in contacts_list:
            self.full_data_dict[u'target_url'].append(contact.target_entry.url)
            self.full_data_dict[u'target_domain'].append(contact.target_entry.domain)
            self.full_data_dict[u'asset'].append(contact.asset_entry.asset)
            self.full_data_dict[u'campaign'].append(contact.asset_entry.campaign)
            self.full_data_dict[u'email_variant'].append(contact.asset_entry.email_variant)
            self.full_data_dict[u'source_url'].append(contact.source_entry.url)
            self.full_data_dict[u'source_domain'].append(contact.source_entry.domain)
            self.full_data_dict[u'contact_name'].append(contact.contact_name)
            self.full_data_dict[u'contact_email'].append(contact.email_entry.email)
            self.full_data_dict[u'domain_registrar_country'].append(contact.source_entry.whois_entry.registrar_country)
            self.full_data_dict[u'domain_admin_country'].append(contact.source_entry.whois_entry.admin_country)
            self.full_data_dict[u'domain_tech_country'].append(contact.source_entry.whois_entry.tech_country)
            self.full_data_dict[u'domain_ip_address_country'].append(contact.source_entry.ip_geo_entry.country_name)
            self.full_data_dict[u'domain_registrar_locale'].append(contact.source_entry.whois_entry.registrar_locale)
            self.full_data_dict[u'domain_admin_locale'].append(contact.source_entry.whois_entry.admin_locale)
            self.full_data_dict[u'domain_tech_locale'].append(contact.source_entry.whois_entry.tech_locale)
            self.full_data_dict[u'domain_ip_geo_locale'].append(contact.source_entry.ip_geo_entry.locale)
            self.full_data_dict[u'domain_tld_type'].append(contact.source_entry.tld_entry.tld_type)
            self.full_data_dict[u'email_registrar_country'].append(contact.email_entry.whois_entry.registrar_country)
            self.full_data_dict[u'email_admin_country'].append(contact.email_entry.whois_entry.admin_country)
            self.full_data_dict[u'email_tech_country'].append(contact.email_entry.whois_entry.tech_country)
            self.full_data_dict[u'email_ip_address_country'].append(contact.email_entry.ip_geo_entry.country_name)
            self.full_data_dict[u'email_registrar_locale'].append(contact.email_entry.whois_entry.registrar_locale)
            self.full_data_dict[u'email_admin_locale'].append(contact.email_entry.whois_entry.admin_locale)
            self.full_data_dict[u'email_tech_locale'].append(contact.email_entry.whois_entry.tech_locale)
            self.full_data_dict[u'email_ip_geo_locale'].append(contact.email_entry.ip_geo_entry.locale)
            self.full_data_dict[u'email_tld_type'].append(contact.email_entry.tld_entry.tld_type)
            self.full_data_dict[u'domain_geo_based_action'].append(contact.source_entry.action)
            self.full_data_dict[u'domain_geo_based_cache'].append(contact.source_entry.cache)
            self.full_data_dict[u'email_geo_based_action'].append(contact.email_entry.action)
            self.full_data_dict[u'email_geo_based_cache'].append(contact.email_entry.cache)
            self.full_data_dict[u'kickbox_email_action'].append(contact.email_entry.kickbox_action)
            self.full_data_dict[u'email_result'].append(contact.email_entry.result)
            self.full_data_dict[u'email_source'].append(contact.email_entry.source)
            self.full_data_dict[u'email_notes'].append(contact.asset_entry.email_notes)
            self.full_data_dict[u'contact_action'].append(contact.contact_action)
            self.full_data_dict[u'email_domain'].append(contact.email_entry.domain)
            self.full_data_dict[u'email_subdomain'].append(contact.email_entry.subdomain)
            self.full_data_dict[u'source_subdomain'].append(contact.source_entry.subdomain)
            self.full_data_dict[u'source_domain_trust_flow'].append(contact.source_entry.trust_flow)
            self.full_data_dict[u'source_domain_topic'].append(contact.source_entry.topic)
            self.full_data_dict[u'source_domain_topic_trust'].append(contact.source_entry.topic_trust_flow)
            self.full_data_dict[u'audiences'].append(contact.asset_entry.audiences)
            self.full_data_dict[u'date_collected'].append(contact.date_collected)
            self.full_data_dict[u'type'].append(contact.contact_type)
            self.full_data_dict[u'site'].append(contact.asset_entry.site)
            self.full_data_dict[u'asset_url'].append(contact.asset_entry.asset_url)
            self.full_data_dict[u'content_group'].append(contact.asset_entry.content_group)
            self.full_data_dict[u'series'].append(contact.asset_entry.series)
            self.full_data_dict[u'context'].append(contact.asset_entry.context)
            self.full_data_dict[u'entity_name'].append(contact.source_entry.entity_name)
            self.full_data_dict[u'entity_type'].append(contact.source_entry.entity_type)
            self.full_data_dict[u'city'].append(contact.source_entry.city)
            self.full_data_dict[u'state'].append(contact.source_entry.state)
            self.full_data_dict[u'entity_website'].append(contact.source_entry.entity_website)
            self.full_data_dict[u'entity_subgroup'].append(contact.source_entry.entity_subgroup)
            self.full_data_dict[u'entity_subgroup_url'].append(contact.source_entry.entity_subgroup_url)
            self.full_data_dict[u'entity_subgroup_resources_url'].append(
                contact.source_entry.entity_subgroup_resources_url)
            for var_num in [u'var' + str(i + 1) for i in range(8)]:
                self.full_data_dict[var_num].append(getattr(contact.send_vars_entry, var_num))
            self.full_data_dict[u'subject'].append(contact.send_vars_entry.subject)
            self.full_data_dict[u'bucket'].append(contact.send_vars_entry.bucket)
        return

    def build_cutfile(self):
        """
        Iterates over all cut lines and adds to cutfile list

        Returns:
            None
        """

        cut_lists = [self.blacklist_contacts, self.linking_contacts, self.recent_contacts,
                     self.invalid_contacts, self.additional_contacts, self.iw_cut_contacts]
        for cut_list in cut_lists:
            for contact in cut_list:
                self.cutfile_contacts_list.append(contact)
        return

    def export_contacts(self, outfile_name=None):
        """
        Exports contacts_list data to the output CSV file

        Returns:
            None
        """

        if not outfile_name:
            self.customize_outfile_names()
        else:
            self.send_ready_filename = outfile_name
            self.cutfile_filename = outfile_name[:-4] + "_cutfile" + outfile_name[-4:]
            self.high_value_contacts_filename = outfile_name[:-4] + "_high_value_contacts.csv"
        print_message("Now updating recent sends table in database.")
        self.update_inventory_recent_sends()
        print_message("Now writing contacts to output...")
        self.contacts_list_to_data_dict(self.accepted_contacts)
        csv_exporter = CSVExporter(self.send_ready_filename, data_dict=self.full_data_dict,
                                   headers_dict=self.send_ready_mailer_headers_dict)
        csv_exporter.write_csv_from_dict()
        self.contacts_list_to_data_dict(self.high_value_contacts)
        csv_exporter = CSVExporter(self.high_value_contacts_filename, data_dict=self.full_data_dict,
                                   headers_dict=self.send_ready_mailer_headers_dict)
        csv_exporter.write_csv_from_dict()
        self.build_cutfile()
        self.contacts_list_to_data_dict(self.cutfile_contacts_list)
        csv_exporter = CSVExporter(self.cutfile_filename, data_dict=self.full_data_dict,
                                   headers_dict=self.cutfile_headers_dict)
        csv_exporter.write_csv_from_dict()
        print_message("Writing to output complete!")
        return


if __name__ == '__main__':
    pass
